// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/12/2016 10:16:54
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action I_ShowPart. */
public class I_ShowPart extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "part" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String part;
	/**
	 * Location, in the context, of the parameter "part" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String partLoc;

	/**
	 * Constructor. Constructs an instance of I_ShowPart.
	 * 
	 * @param part
	 *            value of the parameter "part", or null in case it should be
	 *            read from the context. If null, <code>partLoc</code> cannot be
	 *            null.
	 * @param partLoc
	 *            in case <code>part</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public I_ShowPart(jbt.model.core.ModelTask guard, java.lang.String part,
			java.lang.String partLoc) {
		super(guard);
		this.part = part;
		this.partLoc = partLoc;
	}

	/**
	 * Returns a bts.actions.execution.I_ShowPart task that is able to run this
	 * task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.I_ShowPart(this, executor, parent,
				this.part, this.partLoc);
	}
}