// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/17/2016 23:43:34
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action AI_GetDir. */
public class AI_GetDir extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "varName" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String varName;
	/**
	 * Location, in the context, of the parameter "varName" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String varNameLoc;

	/**
	 * Constructor. Constructs an instance of AI_GetDir.
	 * 
	 * @param varName
	 *            value of the parameter "varName", or null in case it should be
	 *            read from the context. If null, <code>varNameLoc</code> cannot
	 *            be null.
	 * @param varNameLoc
	 *            in case <code>varName</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public AI_GetDir(jbt.model.core.ModelTask guard, java.lang.String varName,
			java.lang.String varNameLoc) {
		super(guard);
		this.varName = varName;
		this.varNameLoc = varNameLoc;
	}

	/**
	 * Returns a bts.actions.execution.AI_GetDir task that is able to run this
	 * task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.AI_GetDir(this, executor, parent,
				this.varName, this.varNameLoc);
	}
}