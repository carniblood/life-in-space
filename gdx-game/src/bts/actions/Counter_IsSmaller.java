// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/10/2016 19:51:14
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action Counter_IsSmaller. */
public class Counter_IsSmaller extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "counterName" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String counterName;
	/**
	 * Location, in the context, of the parameter "counterName" in case its
	 * value is not specified at construction time. null otherwise.
	 */
	private java.lang.String counterNameLoc;
	/**
	 * Value of the parameter "value" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Integer value;
	/**
	 * Location, in the context, of the parameter "value" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String valueLoc;

	/**
	 * Constructor. Constructs an instance of Counter_IsSmaller.
	 * 
	 * @param counterName
	 *            value of the parameter "counterName", or null in case it
	 *            should be read from the context. If null,
	 *            <code>counterNameLoc</code> cannot be null.
	 * @param counterNameLoc
	 *            in case <code>counterName</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 * @param value
	 *            value of the parameter "value", or null in case it should be
	 *            read from the context. If null, <code>valueLoc</code> cannot
	 *            be null.
	 * @param valueLoc
	 *            in case <code>value</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public Counter_IsSmaller(jbt.model.core.ModelTask guard,
			java.lang.String counterName, java.lang.String counterNameLoc,
			java.lang.Integer value, java.lang.String valueLoc) {
		super(guard);
		this.counterName = counterName;
		this.counterNameLoc = counterNameLoc;
		this.value = value;
		this.valueLoc = valueLoc;
	}

	/**
	 * Returns a bts.actions.execution.Counter_IsSmaller task that is able to
	 * run this task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.Counter_IsSmaller(this, executor,
				parent, this.counterName, this.counterNameLoc, this.value,
				this.valueLoc);
	}
}