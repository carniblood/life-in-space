// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/18/2016 02:18:16
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action AI_Move. */
public class AI_Move extends jbt.model.task.leaf.action.ModelAction {

	/** Constructor. Constructs an instance of AI_Move. */
	public AI_Move(jbt.model.core.ModelTask guard) {
		super(guard);
	}

	/**
	 * Returns a bts.actions.execution.AI_Move task that is able to run this
	 * task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.AI_Move(this, executor, parent);
	}
}