// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/09/2016 13:20:31
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action Stack_IsEmpty. */
public class Stack_IsEmpty extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "stack" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String stack;
	/**
	 * Location, in the context, of the parameter "stack" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String stackLoc;

	/**
	 * Constructor. Constructs an instance of Stack_IsEmpty.
	 * 
	 * @param stack
	 *            value of the parameter "stack", or null in case it should be
	 *            read from the context. If null, <code>stackLoc</code> cannot
	 *            be null.
	 * @param stackLoc
	 *            in case <code>stack</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public Stack_IsEmpty(jbt.model.core.ModelTask guard,
			java.lang.String stack, java.lang.String stackLoc) {
		super(guard);
		this.stack = stack;
		this.stackLoc = stackLoc;
	}

	/**
	 * Returns a bts.actions.execution.Stack_IsEmpty task that is able to run
	 * this task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.Stack_IsEmpty(this, executor, parent,
				this.stack, this.stackLoc);
	}
}