// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/09/2016 13:20:31
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action AI_MoveAway. */
public class AI_MoveAway extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "targetAngle" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Float targetAngle;
	/**
	 * Location, in the context, of the parameter "targetAngle" in case its
	 * value is not specified at construction time. null otherwise.
	 */
	private java.lang.String targetAngleLoc;

	/**
	 * Constructor. Constructs an instance of AI_MoveAway.
	 * 
	 * @param targetAngle
	 *            value of the parameter "targetAngle", or null in case it
	 *            should be read from the context. If null,
	 *            <code>targetAngleLoc</code> cannot be null.
	 * @param targetAngleLoc
	 *            in case <code>targetAngle</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 */
	public AI_MoveAway(jbt.model.core.ModelTask guard,
			java.lang.Float targetAngle, java.lang.String targetAngleLoc) {
		super(guard);
		this.targetAngle = targetAngle;
		this.targetAngleLoc = targetAngleLoc;
	}

	/**
	 * Returns a bts.actions.execution.AI_MoveAway task that is able to run this
	 * task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.AI_MoveAway(this, executor, parent,
				this.targetAngle, this.targetAngleLoc);
	}
}