// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/17/2016 23:27:00
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action Entity_IsCollider. */
public class Entity_IsCollider extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "object" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object object;
	/**
	 * Location, in the context, of the parameter "object" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String objectLoc;

	/**
	 * Constructor. Constructs an instance of Entity_IsCollider.
	 * 
	 * @param object
	 *            value of the parameter "object", or null in case it should be
	 *            read from the context. If null, <code>objectLoc</code> cannot
	 *            be null.
	 * @param objectLoc
	 *            in case <code>object</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public Entity_IsCollider(jbt.model.core.ModelTask guard,
			java.lang.Object object, java.lang.String objectLoc) {
		super(guard);
		this.object = object;
		this.objectLoc = objectLoc;
	}

	/**
	 * Returns a bts.actions.execution.Entity_IsCollider task that is able to
	 * run this task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.Entity_IsCollider(this, executor,
				parent, this.object, this.objectLoc);
	}
}