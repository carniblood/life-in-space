// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/17/2016 23:27:00
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action Float_IsGreater. */
public class Float_IsGreater extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "value1" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Float value1;
	/**
	 * Location, in the context, of the parameter "value1" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String value1Loc;
	/**
	 * Value of the parameter "value2" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Float value2;
	/**
	 * Location, in the context, of the parameter "value2" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String value2Loc;

	/**
	 * Constructor. Constructs an instance of Float_IsGreater.
	 * 
	 * @param value1
	 *            value of the parameter "value1", or null in case it should be
	 *            read from the context. If null, <code>value1Loc</code> cannot
	 *            be null.
	 * @param value1Loc
	 *            in case <code>value1</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 * @param value2
	 *            value of the parameter "value2", or null in case it should be
	 *            read from the context. If null, <code>value2Loc</code> cannot
	 *            be null.
	 * @param value2Loc
	 *            in case <code>value2</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public Float_IsGreater(jbt.model.core.ModelTask guard,
			java.lang.Float value1, java.lang.String value1Loc,
			java.lang.Float value2, java.lang.String value2Loc) {
		super(guard);
		this.value1 = value1;
		this.value1Loc = value1Loc;
		this.value2 = value2;
		this.value2Loc = value2Loc;
	}

	/**
	 * Returns a bts.actions.execution.Float_IsGreater task that is able to run
	 * this task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.Float_IsGreater(this, executor,
				parent, this.value1, this.value1Loc, this.value2,
				this.value2Loc);
	}
}