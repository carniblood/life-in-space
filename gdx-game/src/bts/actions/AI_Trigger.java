// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/10/2016 21:28:39
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action AI_Trigger. */
public class AI_Trigger extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "event" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String event;
	/**
	 * Location, in the context, of the parameter "event" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String eventLoc;
	/**
	 * Value of the parameter "actor" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object actor;
	/**
	 * Location, in the context, of the parameter "actor" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String actorLoc;

	/**
	 * Constructor. Constructs an instance of AI_Trigger.
	 * 
	 * @param event
	 *            value of the parameter "event", or null in case it should be
	 *            read from the context. If null, <code>eventLoc</code> cannot
	 *            be null.
	 * @param eventLoc
	 *            in case <code>event</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 * @param actor
	 *            value of the parameter "actor", or null in case it should be
	 *            read from the context. If null, <code>actorLoc</code> cannot
	 *            be null.
	 * @param actorLoc
	 *            in case <code>actor</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public AI_Trigger(jbt.model.core.ModelTask guard, java.lang.String event,
			java.lang.String eventLoc, java.lang.Object actor,
			java.lang.String actorLoc) {
		super(guard);
		this.event = event;
		this.eventLoc = eventLoc;
		this.actor = actor;
		this.actorLoc = actorLoc;
	}

	/**
	 * Returns a bts.actions.execution.AI_Trigger task that is able to run this
	 * task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.AI_Trigger(this, executor, parent,
				this.event, this.eventLoc, this.actor, this.actorLoc);
	}
}