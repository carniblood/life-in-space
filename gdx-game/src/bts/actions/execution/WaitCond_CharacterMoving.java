// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/10/2016 21:34:45
// ******************************************************* 
package bts.actions.execution;

import com.chernobyl.lifeinspace.Character;

/** ExecutionAction class created from MMPM action WaitCond_CharacterMoving. */
public class WaitCond_CharacterMoving extends
		jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "character" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object character;
	/**
	 * Location, in the context, of the parameter "character" in case its value
	 * is not specified at construction time. null otherwise.
	 */
	private java.lang.String characterLoc;

	private float lastAngle;
	
	/**
	 * Constructor. Constructs an instance of WaitCond_CharacterMoving that is
	 * able to run a bts.actions.WaitCond_CharacterMoving.
	 * 
	 * @param character
	 *            value of the parameter "character", or null in case it should
	 *            be read from the context. If null,
	 *            <code>characterLoc<code> cannot be null.
	 * @param characterLoc
	 *            in case <code>character</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 */
	public WaitCond_CharacterMoving(
			bts.actions.WaitCond_CharacterMoving modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent,
			java.lang.Object character, java.lang.String characterLoc) {
		super(modelTask, executor, parent);

		this.character = character;
		this.characterLoc = characterLoc;
	}

	/**
	 * Returns the value of the parameter "character", or null in case it has
	 * not been specified or it cannot be found in the context.
	 */
	public java.lang.Object getCharacter() {
		if (this.character != null) {
			return this.character;
		} else {
			return (java.lang.Object) this.getContext().getVariable(
					this.characterLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
		
		Character actor = (Character)getCharacter();
		
		lastAngle = actor.GetAngle();
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		Character actor = (Character)getCharacter();		
		if (actor == null)
			return jbt.execution.core.ExecutionTask.Status.FAILURE;		
		else if (actor.GetAngle() != lastAngle)
			return jbt.execution.core.ExecutionTask.Status.SUCCESS;
		
		lastAngle = actor.GetAngle();
		
		return jbt.execution.core.ExecutionTask.Status.RUNNING;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}