// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/28/2016 10:46:55
// ******************************************************* 
package bts.actions.execution;

import com.chernobyl.common.utils.JBTHelpers;

/** ExecutionAction class created from MMPM action Timer_Wait. */
public class Timer_Wait extends jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "timeInSec" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Float timeInSec;
	/**
	 * Location, in the context, of the parameter "timeInSec" in case its value
	 * is not specified at construction time. null otherwise.
	 */
	private java.lang.String timeInSecLoc;
	
	private float startTime;

	/**
	 * Constructor. Constructs an instance of Timer_Wait that is able to run a
	 * bts.actions.Timer_Wait.
	 * 
	 * @param timeInSec
	 *            value of the parameter "timeInSec", or null in case it should
	 *            be read from the context. If null,
	 *            <code>timeInSecLoc<code> cannot be null.
	 * @param timeInSecLoc
	 *            in case <code>timeInSec</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 */
	public Timer_Wait(bts.actions.Timer_Wait modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.Float timeInSec,
			java.lang.String timeInSecLoc) {
		super(modelTask, executor, parent);

		this.timeInSec = timeInSec;
		this.timeInSecLoc = timeInSecLoc;
	}

	/**
	 * Returns the value of the parameter "timeInSec", or null in case it has
	 * not been specified or it cannot be found in the context.
	 */
	public java.lang.Float getTimeInSec() {
		if (this.timeInSec != null) {
			return this.timeInSec;
		} else {
			return (java.lang.Float) this.getContext().getVariable(
					this.timeInSecLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
		
		startTime = JBTHelpers.GetCurTime();
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		if (JBTHelpers.GetCurTime() - startTime >= getTimeInSec())
			return jbt.execution.core.ExecutionTask.Status.SUCCESS;
		else
			return jbt.execution.core.ExecutionTask.Status.RUNNING;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}