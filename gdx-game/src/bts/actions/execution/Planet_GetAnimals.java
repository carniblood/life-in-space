// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/09/2016 13:20:31
// ******************************************************* 
package bts.actions.execution;

import jbt.execution.core.IContext;

import com.chernobyl.common.utils.JBTHelpers;
import com.chernobyl.lifeinspace.Entity;
import com.chernobyl.lifeinspace.Planet;

/** ExecutionAction class created from MMPM action Planet_GetAnimals. */
public class Planet_GetAnimals extends
		jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "stackName" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String stackName;
	/**
	 * Location, in the context, of the parameter "stackName" in case its value
	 * is not specified at construction time. null otherwise.
	 */
	private java.lang.String stackNameLoc;

	/**
	 * Constructor. Constructs an instance of Planet_GetAnimals that is able to
	 * run a bts.actions.Planet_GetAnimals.
	 * 
	 * @param stackName
	 *            value of the parameter "stackName", or null in case it should
	 *            be read from the context. If null,
	 *            <code>stackNameLoc<code> cannot be null.
	 * @param stackNameLoc
	 *            in case <code>stackName</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 */
	public Planet_GetAnimals(bts.actions.Planet_GetAnimals modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent,
			java.lang.String stackName, java.lang.String stackNameLoc) {
		super(modelTask, executor, parent);

		this.stackName = stackName;
		this.stackNameLoc = stackNameLoc;
	}

	/**
	 * Returns the value of the parameter "stackName", or null in case it has
	 * not been specified or it cannot be found in the context.
	 */
	public java.lang.String getStackName() {
		if (this.stackName != null) {
			return this.stackName;
		} else {
			return (java.lang.String) this.getContext().getVariable(
					this.stackNameLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		IContext context = getContext();
		final Entity agent = (Entity)context.getVariable("agent");
		
		// empty stack
		while (JBTHelpers.PopFromStack(getStackName()) != null) {}

		agent.GetPlanet().ApplyToEntities(new Planet.EntityFunction()
			{
				@Override
				public void apply(Entity entity)
				{	
					if (entity == agent)
						return;
					
					if (entity.GetInteractionType().equals("Animal"))
					{
						JBTHelpers.PushToStack(getStackName(), entity);
					}
				}
			});
		
		return jbt.execution.core.ExecutionTask.Status.SUCCESS;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}