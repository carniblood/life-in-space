// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/10/2016 23:49:04
// ******************************************************* 
package bts.actions.execution;

/** ExecutionAction class created from MMPM action Integer_IsSmaller. */
public class Integer_IsSmaller extends
		jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "value1" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Integer value1;
	/**
	 * Location, in the context, of the parameter "value1" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String value1Loc;
	/**
	 * Value of the parameter "value2" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Integer value2;
	/**
	 * Location, in the context, of the parameter "value2" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String value2Loc;

	/**
	 * Constructor. Constructs an instance of Integer_IsSmaller that is able to
	 * run a bts.actions.Integer_IsSmaller.
	 * 
	 * @param value1
	 *            value of the parameter "value1", or null in case it should be
	 *            read from the context. If null,
	 *            <code>value1Loc<code> cannot be null.
	 * @param value1Loc
	 *            in case <code>value1</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 * @param value2
	 *            value of the parameter "value2", or null in case it should be
	 *            read from the context. If null,
	 *            <code>value2Loc<code> cannot be null.
	 * @param value2Loc
	 *            in case <code>value2</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public Integer_IsSmaller(bts.actions.Integer_IsSmaller modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.Integer value1,
			java.lang.String value1Loc, java.lang.Integer value2,
			java.lang.String value2Loc) {
		super(modelTask, executor, parent);

		this.value1 = value1;
		this.value1Loc = value1Loc;
		this.value2 = value2;
		this.value2Loc = value2Loc;
	}

	/**
	 * Returns the value of the parameter "value1", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Integer getValue1() {
		if (this.value1 != null) {
			return this.value1;
		} else {
			return (java.lang.Integer) this.getContext().getVariable(
					this.value1Loc);
		}
	}

	/**
	 * Returns the value of the parameter "value2", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Integer getValue2() {
		if (this.value2 != null) {
			return this.value2;
		} else {
			return (java.lang.Integer) this.getContext().getVariable(
					this.value2Loc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		if (getValue1() < getValue2())
			return jbt.execution.core.ExecutionTask.Status.SUCCESS;
		else
			return jbt.execution.core.ExecutionTask.Status.FAILURE;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}