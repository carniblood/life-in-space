// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/12/2016 10:16:53
// ******************************************************* 
package bts.actions.execution;

import jbt.execution.core.IContext;

import com.chernobyl.lifeinspace.Character;

/** ExecutionAction class created from MMPM action I_IsType. */
public class I_IsType extends jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "type" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String type;
	/**
	 * Location, in the context, of the parameter "type" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String typeLoc;

	/**
	 * Constructor. Constructs an instance of I_IsType that is able to run a
	 * bts.actions.I_IsType.
	 * 
	 * @param type
	 *            value of the parameter "type", or null in case it should be
	 *            read from the context. If null,
	 *            <code>typeLoc<code> cannot be null.
	 * @param typeLoc
	 *            in case <code>type</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public I_IsType(bts.actions.I_IsType modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.String type,
			java.lang.String typeLoc) {
		super(modelTask, executor, parent);

		this.type = type;
		this.typeLoc = typeLoc;
	}

	/**
	 * Returns the value of the parameter "type", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.String getType() {
		if (this.type != null) {
			return this.type;
		} else {
			return (java.lang.String) this.getContext().getVariable(
					this.typeLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		IContext context = getContext();
		
		Character actor = (Character)context.getVariable("actor");
		
		if (actor.GetInteractionType().equals(getType()))
			return jbt.execution.core.ExecutionTask.Status.SUCCESS;
		else
			return jbt.execution.core.ExecutionTask.Status.FAILURE;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}