// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/09/2016 13:20:31
// ******************************************************* 
package bts.actions.execution;

import jbt.execution.core.IContext;

import com.chernobyl.lifeinspace.Entity;
import com.chernobyl.lifeinspace.Planet;

/** ExecutionAction class created from MMPM action Entity_GetDistance. */
public class Entity_GetDistance extends
		jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "object" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object object;
	/**
	 * Location, in the context, of the parameter "object" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String objectLoc;
	/**
	 * Value of the parameter "varName" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String varName;
	/**
	 * Location, in the context, of the parameter "varName" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String varNameLoc;

	/**
	 * Constructor. Constructs an instance of Entity_GetDistance that is able to
	 * run a bts.actions.Entity_GetDistance.
	 * 
	 * @param object
	 *            value of the parameter "object", or null in case it should be
	 *            read from the context. If null,
	 *            <code>objectLoc<code> cannot be null.
	 * @param objectLoc
	 *            in case <code>object</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 * @param varName
	 *            value of the parameter "varName", or null in case it should be
	 *            read from the context. If null,
	 *            <code>varNameLoc<code> cannot be null.
	 * @param varNameLoc
	 *            in case <code>varName</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public Entity_GetDistance(bts.actions.Entity_GetDistance modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.Object object,
			java.lang.String objectLoc, java.lang.String varName,
			java.lang.String varNameLoc) {
		super(modelTask, executor, parent);

		this.object = object;
		this.objectLoc = objectLoc;
		this.varName = varName;
		this.varNameLoc = varNameLoc;
	}

	/**
	 * Returns the value of the parameter "object", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Object getObject() {
		if (this.object != null) {
			return this.object;
		} else {
			return (java.lang.Object) this.getContext().getVariable(
					this.objectLoc);
		}
	}

	/**
	 * Returns the value of the parameter "varName", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.String getVarName() {
		if (this.varName != null) {
			return this.varName;
		} else {
			return (java.lang.String) this.getContext().getVariable(
					this.varNameLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		IContext context = getContext();
		
		Entity agent = (Entity)context.getVariable("agent");			
		Entity entity = (Entity)getObject();	
		
		String varName = getVarName();
		if (varName == null || varName.isEmpty())
			return jbt.execution.core.ExecutionTask.Status.FAILURE;
		
		Planet planet = agent.GetPlanet();
		float distance = planet.Distance(agent, entity);
		context.setVariable(getVarName(), distance);
		
		return jbt.execution.core.ExecutionTask.Status.SUCCESS;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}