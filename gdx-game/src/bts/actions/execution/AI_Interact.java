// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/09/2016 13:20:31
// ******************************************************* 
package bts.actions.execution;

import com.chernobyl.common.inputs.TouchState;
import com.chernobyl.lifeinspace.Character;
import com.chernobyl.lifeinspace.Entity;

import jbt.execution.core.IContext;

/** ExecutionAction class created from MMPM action AI_Interact. */
public class AI_Interact extends jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "target" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object target;
	/**
	 * Location, in the context, of the parameter "target" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String targetLoc;

	private boolean failed;
	
	/**
	 * Constructor. Constructs an instance of AI_Interact that is able to run a
	 * bts.actions.AI_Interact.
	 * 
	 * @param target
	 *            value of the parameter "target", or null in case it should be
	 *            read from the context. If null,
	 *            <code>targetLoc<code> cannot be null.
	 * @param targetLoc
	 *            in case <code>target</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public AI_Interact(bts.actions.AI_Interact modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.Object target,
			java.lang.String targetLoc) {
		super(modelTask, executor, parent);

		this.target = target;
		this.targetLoc = targetLoc;
	}

	/**
	 * Returns the value of the parameter "target", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Object getTarget() {
		if (this.target != null) {
			return this.target;
		} else {
			return (java.lang.Object) this.getContext().getVariable(
					this.targetLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);

		IContext context = getContext();
		Character agent = (Character)context.getVariable("agent");
		
		Entity target = (Entity)getTarget();
		if (target != null)
		{
			failed = !agent.Interact(target, TouchState.PRESS);
		}
		else
		{
			failed = true;
		}
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		IContext context = getContext();
		Character agent = (Character)context.getVariable("agent");
		
		if (failed)
		{
			return jbt.execution.core.ExecutionTask.Status.FAILURE;
		}
		else if (agent.IsInteracting())
		{
			return jbt.execution.core.ExecutionTask.Status.RUNNING;
		}
		else
		{
			return jbt.execution.core.ExecutionTask.Status.SUCCESS;
		}
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}