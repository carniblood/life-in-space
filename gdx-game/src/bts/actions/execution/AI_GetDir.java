// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/17/2016 23:43:34
// ******************************************************* 
package bts.actions.execution;

import jbt.execution.core.IContext;

import com.chernobyl.lifeinspace.Character;

/** ExecutionAction class created from MMPM action AI_GetDir. */
public class AI_GetDir extends jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "varName" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String varName;
	/**
	 * Location, in the context, of the parameter "varName" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String varNameLoc;

	/**
	 * Constructor. Constructs an instance of AI_GetDir that is able to run a
	 * bts.actions.AI_GetDir.
	 * 
	 * @param varName
	 *            value of the parameter "varName", or null in case it should be
	 *            read from the context. If null,
	 *            <code>varNameLoc<code> cannot be null.
	 * @param varNameLoc
	 *            in case <code>varName</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public AI_GetDir(bts.actions.AI_GetDir modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.String varName,
			java.lang.String varNameLoc) {
		super(modelTask, executor, parent);

		this.varName = varName;
		this.varNameLoc = varNameLoc;
	}

	/**
	 * Returns the value of the parameter "varName", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.String getVarName() {
		if (this.varName != null) {
			return this.varName;
		} else {
			return (java.lang.String) this.getContext().getVariable(
					this.varNameLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		String varName = getVarName();
		if (varName == null || varName.isEmpty())
			return jbt.execution.core.ExecutionTask.Status.FAILURE;

		IContext context = getContext();
		
		Character agent = (Character)context.getVariable("agent");
		
		int direction = agent.GetDir();
		context.setVariable(getVarName(), direction);
		
		return jbt.execution.core.ExecutionTask.Status.SUCCESS;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}