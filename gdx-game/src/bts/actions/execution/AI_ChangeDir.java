// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/09/2016 13:20:31
// ******************************************************* 
package bts.actions.execution;

import jbt.execution.core.IContext;

import com.chernobyl.lifeinspace.Character;

/** ExecutionAction class created from MMPM action AI_ChangeDir. */
public class AI_ChangeDir extends
		jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "newDir" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Integer newDir;
	/**
	 * Location, in the context, of the parameter "newDir" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String newDirLoc;

	/**
	 * Constructor. Constructs an instance of AI_ChangeDir that is able to run a
	 * bts.actions.AI_ChangeDir.
	 * 
	 * @param newDir
	 *            value of the parameter "newDir", or null in case it should be
	 *            read from the context. If null,
	 *            <code>newDirLoc<code> cannot be null.
	 * @param newDirLoc
	 *            in case <code>newDir</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public AI_ChangeDir(bts.actions.AI_ChangeDir modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.Integer newDir,
			java.lang.String newDirLoc) {
		super(modelTask, executor, parent);

		this.newDir = newDir;
		this.newDirLoc = newDirLoc;
	}

	/**
	 * Returns the value of the parameter "newDir", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Integer getNewDir() {
		if (this.newDir != null) {
			return this.newDir;
		} else {
			return (java.lang.Integer) this.getContext().getVariable(
					this.newDirLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		IContext context = getContext();
		Character agent = (Character)context.getVariable("agent");
		
		agent.SetDir(getNewDir());
		
		return jbt.execution.core.ExecutionTask.Status.SUCCESS;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}