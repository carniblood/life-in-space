// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/17/2016 23:27:00
// ******************************************************* 
package bts.actions.execution;

import jbt.execution.core.IContext;

import com.chernobyl.lifeinspace.Entity;

/** ExecutionAction class created from MMPM action Entity_GetPrev. */
public class Entity_GetPrev extends
		jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "object" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object object;
	/**
	 * Location, in the context, of the parameter "object" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String objectLoc;
	/**
	 * Value of the parameter "prev" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object prev;
	/**
	 * Location, in the context, of the parameter "prev" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String prevLoc;

	/**
	 * Constructor. Constructs an instance of Entity_GetPrev that is able to run
	 * a bts.actions.Entity_GetPrev.
	 * 
	 * @param object
	 *            value of the parameter "object", or null in case it should be
	 *            read from the context. If null,
	 *            <code>objectLoc<code> cannot be null.
	 * @param objectLoc
	 *            in case <code>object</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 * @param prev
	 *            value of the parameter "prev", or null in case it should be
	 *            read from the context. If null,
	 *            <code>prevLoc<code> cannot be null.
	 * @param prevLoc
	 *            in case <code>prev</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public Entity_GetPrev(bts.actions.Entity_GetPrev modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.Object object,
			java.lang.String objectLoc, java.lang.Object prev,
			java.lang.String prevLoc) {
		super(modelTask, executor, parent);

		this.object = object;
		this.objectLoc = objectLoc;
		this.prev = prev;
		this.prevLoc = prevLoc;
	}

	/**
	 * Returns the value of the parameter "object", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Object getObject() {
		if (this.object != null) {
			return this.object;
		} else {
			return (java.lang.Object) this.getContext().getVariable(
					this.objectLoc);
		}
	}

	/**
	 * Returns the value of the parameter "prev", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Object getPrev() {
		if (this.prev != null) {
			return this.prev;
		} else {
			return (java.lang.Object) this.getContext().getVariable(
					this.prevLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		IContext context = getContext();

		Entity entity = (Entity)getObject();
		Entity nextEntity = entity.GetPrevEntity();
		
		if (nextEntity == null)
		{
			return jbt.execution.core.ExecutionTask.Status.FAILURE;			
		}
		else
		{
			context.setVariable(this.prevLoc, nextEntity);
			return jbt.execution.core.ExecutionTask.Status.SUCCESS;	
		}
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}