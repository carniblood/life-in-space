// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/10/2016 22:00:12
// ******************************************************* 
package bts.actions.execution;

import jbt.execution.core.IContext;

import com.chernobyl.lifeinspace.Entity;
import com.chernobyl.lifeinspace.Planet;

/** ExecutionAction class created from MMPM action WaitCond_EntityDistanceMax. */
public class WaitCond_EntityDistanceMax extends
		jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "entity" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object entity;
	/**
	 * Location, in the context, of the parameter "entity" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String entityLoc;
	/**
	 * Value of the parameter "distance" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Float distance;
	/**
	 * Location, in the context, of the parameter "distance" in case its value
	 * is not specified at construction time. null otherwise.
	 */
	private java.lang.String distanceLoc;
	
	private float lastAngle = 1000.f;

	/**
	 * Constructor. Constructs an instance of WaitCond_EntityDistanceMax that is
	 * able to run a bts.actions.WaitCond_EntityDistanceMax.
	 * 
	 * @param entity
	 *            value of the parameter "entity", or null in case it should be
	 *            read from the context. If null,
	 *            <code>entityLoc<code> cannot be null.
	 * @param entityLoc
	 *            in case <code>entity</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 * @param distance
	 *            value of the parameter "distance", or null in case it should
	 *            be read from the context. If null,
	 *            <code>distanceLoc<code> cannot be null.
	 * @param distanceLoc
	 *            in case <code>distance</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 */
	public WaitCond_EntityDistanceMax(
			bts.actions.WaitCond_EntityDistanceMax modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.Object entity,
			java.lang.String entityLoc, java.lang.Float distance,
			java.lang.String distanceLoc) {
		super(modelTask, executor, parent);

		this.entity = entity;
		this.entityLoc = entityLoc;
		this.distance = distance;
		this.distanceLoc = distanceLoc;
	}

	/**
	 * Returns the value of the parameter "entity", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Object getEntity() {
		if (this.entity != null) {
			return this.entity;
		} else {
			return (java.lang.Object) this.getContext().getVariable(
					this.entityLoc);
		}
	}

	/**
	 * Returns the value of the parameter "distance", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Float getDistance() {
		if (this.distance != null) {
			return this.distance;
		} else {
			return (java.lang.Float) this.getContext().getVariable(
					this.distanceLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		IContext context = getContext();
		
		Entity agent = (Entity)context.getVariable("agent");		
		Entity entity = (Entity)getEntity();	

		if (entity.GetAngle() == lastAngle)
		{
			return jbt.execution.core.ExecutionTask.Status.RUNNING;			
		}
		
		lastAngle = entity.GetAngle();
		
		Planet planet = agent.GetPlanet();
		float distance = planet.Distance(agent, entity);
		
		if (Math.abs(distance) < getDistance())
			return jbt.execution.core.ExecutionTask.Status.SUCCESS;
		else
			return jbt.execution.core.ExecutionTask.Status.RUNNING;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}