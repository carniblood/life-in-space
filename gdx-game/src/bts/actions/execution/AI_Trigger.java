// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/10/2016 21:28:39
// ******************************************************* 
package bts.actions.execution;

import jbt.execution.core.IContext;

import com.chernobyl.common.utils.JBTHelpers;
import com.chernobyl.common.utils.JBTHelpers.Interaction;

import com.chernobyl.lifeinspace.Character;
import com.chernobyl.lifeinspace.Entity;

/** ExecutionAction class created from MMPM action AI_Trigger. */
public class AI_Trigger extends jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "event" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String event;
	/**
	 * Location, in the context, of the parameter "event" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String eventLoc;
	/**
	 * Value of the parameter "actor" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object actor;
	/**
	 * Location, in the context, of the parameter "actor" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String actorLoc;
	
	Interaction interaction;

	/**
	 * Constructor. Constructs an instance of AI_Trigger that is able to run a
	 * bts.actions.AI_Trigger.
	 * 
	 * @param event
	 *            value of the parameter "event", or null in case it should be
	 *            read from the context. If null,
	 *            <code>eventLoc<code> cannot be null.
	 * @param eventLoc
	 *            in case <code>event</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 * @param actor
	 *            value of the parameter "actor", or null in case it should be
	 *            read from the context. If null,
	 *            <code>actorLoc<code> cannot be null.
	 * @param actorLoc
	 *            in case <code>actor</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public AI_Trigger(bts.actions.AI_Trigger modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.String event,
			java.lang.String eventLoc, java.lang.Object actor,
			java.lang.String actorLoc) {
		super(modelTask, executor, parent);

		this.event = event;
		this.eventLoc = eventLoc;
		this.actor = actor;
		this.actorLoc = actorLoc;
	}

	/**
	 * Returns the value of the parameter "event", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.String getEvent() {
		if (this.event != null) {
			return this.event;
		} else {
			return (java.lang.String) this.getContext().getVariable(
					this.eventLoc);
		}
	}

	/**
	 * Returns the value of the parameter "actor", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Object getActor() {
		if (this.actor != null) {
			return this.actor;
		} else {
			return (java.lang.Object) this.getContext().getVariable(
					this.actorLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);

		IContext context = getContext();		
		Entity entity = (Entity)context.getVariable("agent");
		Character player = (Character)getActor();
		
		interaction = JBTHelpers.GetInteraction(getEvent(), entity, player);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		if (interaction == null)
			return jbt.execution.core.ExecutionTask.Status.FAILURE;
		else if (interaction.run())
			return jbt.execution.core.ExecutionTask.Status.RUNNING;
		else
			return jbt.execution.core.ExecutionTask.Status.SUCCESS;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}