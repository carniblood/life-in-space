// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/10/2016 19:51:14
// ******************************************************* 
package bts.actions.execution;

import com.chernobyl.common.utils.JBTHelpers;

/** ExecutionAction class created from MMPM action Counter_Incr. */
public class Counter_Incr extends
		jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "counterName" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String counterName;
	/**
	 * Location, in the context, of the parameter "counterName" in case its
	 * value is not specified at construction time. null otherwise.
	 */
	private java.lang.String counterNameLoc;

	/**
	 * Constructor. Constructs an instance of Counter_Incr that is able to run a
	 * bts.actions.Counter_Incr.
	 * 
	 * @param counterName
	 *            value of the parameter "counterName", or null in case it
	 *            should be read from the context. If null,
	 *            <code>counterNameLoc<code> cannot be null.
	 * @param counterNameLoc
	 *            in case <code>counterName</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 */
	public Counter_Incr(bts.actions.Counter_Incr modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent,
			java.lang.String counterName, java.lang.String counterNameLoc) {
		super(modelTask, executor, parent);

		this.counterName = counterName;
		this.counterNameLoc = counterNameLoc;
	}

	/**
	 * Returns the value of the parameter "counterName", or null in case it has
	 * not been specified or it cannot be found in the context.
	 */
	public java.lang.String getCounterName() {
		if (this.counterName != null) {
			return this.counterName;
		} else {
			return (java.lang.String) this.getContext().getVariable(
					this.counterNameLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		JBTHelpers.IncrCounter(getCounterName());
		return jbt.execution.core.ExecutionTask.Status.SUCCESS;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}