// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/09/2016 13:20:31
// ******************************************************* 
package bts.actions.execution;

import jbt.execution.core.IContext;

import com.chernobyl.lifeinspace.Entity;
import com.chernobyl.lifeinspace.Planet;

/** ExecutionAction class created from MMPM action Planet_GetPlayer. */
public class Planet_GetPlayer extends
		jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "player" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object player;
	/**
	 * Location, in the context, of the parameter "player" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String playerLoc;

	/**
	 * Constructor. Constructs an instance of Planet_GetPlayer that is able to
	 * run a bts.actions.Planet_GetPlayer.
	 * 
	 * @param player
	 *            value of the parameter "player", or null in case it should be
	 *            read from the context. If null,
	 *            <code>playerLoc<code> cannot be null.
	 * @param playerLoc
	 *            in case <code>player</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public Planet_GetPlayer(bts.actions.Planet_GetPlayer modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.Object player,
			java.lang.String playerLoc) {
		super(modelTask, executor, parent);

		this.player = player;
		this.playerLoc = playerLoc;
	}

	/**
	 * Returns the value of the parameter "player", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Object getPlayer() {
		if (this.player != null) {
			return this.player;
		} else {
			return (java.lang.Object) this.getContext().getVariable(
					this.playerLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		IContext context = getContext();
		final Entity agent = (Entity)context.getVariable("agent");
		
		// empty variable
		getContext().setVariable(playerLoc, null);

		agent.GetPlanet().ApplyToEntities(new Planet.EntityFunction()
			{
				@Override
				public void apply(Entity entity)
				{	
					if (entity == agent)
						return;
					
					if (entity.GetInteractionType().equals("Player"))
					{
						getContext().setVariable(playerLoc, entity);
					}
				}
			});
		
		return jbt.execution.core.ExecutionTask.Status.SUCCESS;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}