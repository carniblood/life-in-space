// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/17/2016 03:02:48
// ******************************************************* 
package bts.actions.execution;

import jbt.execution.core.IContext;

/** ExecutionAction class created from MMPM action I_ForbidCancel. */
public class I_ForbidCancel extends
		jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "forbid" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Boolean forbid;
	/**
	 * Location, in the context, of the parameter "forbid" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String forbidLoc;

	/**
	 * Constructor. Constructs an instance of I_ForbidCancel that is able to run
	 * a bts.actions.I_ForbidCancel.
	 * 
	 * @param forbid
	 *            value of the parameter "forbid", or null in case it should be
	 *            read from the context. If null,
	 *            <code>forbidLoc<code> cannot be null.
	 * @param forbidLoc
	 *            in case <code>forbid</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public I_ForbidCancel(bts.actions.I_ForbidCancel modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.Boolean forbid,
			java.lang.String forbidLoc) {
		super(modelTask, executor, parent);

		this.forbid = forbid;
		this.forbidLoc = forbidLoc;
	}

	/**
	 * Returns the value of the parameter "forbid", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Boolean getForbid() {
		if (this.forbid != null) {
			return this.forbid;
		} else {
			return (java.lang.Boolean) this.getContext().getVariable(
					this.forbidLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		IContext context = getContext();
		
		context.setVariable("forbidCancel", getForbid());
		
		return jbt.execution.core.ExecutionTask.Status.SUCCESS;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}