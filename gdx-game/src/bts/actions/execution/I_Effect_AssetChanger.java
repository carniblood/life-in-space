// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/09/2016 13:20:31
// ******************************************************* 
package bts.actions.execution;

import jbt.execution.core.IContext;

import com.chernobyl.common.render.Billboard;
import com.chernobyl.lifeinspace.Entity;

/** ExecutionAction class created from MMPM action I_Effect_AssetChanger. */
public class I_Effect_AssetChanger extends
		jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "asset" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String asset;
	/**
	 * Location, in the context, of the parameter "asset" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String assetLoc;
	/**
	 * Value of the parameter "duration" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Float duration;
	/**
	 * Location, in the context, of the parameter "duration" in case its value
	 * is not specified at construction time. null otherwise.
	 */
	private java.lang.String durationLoc;

	/**
	 * Constructor. Constructs an instance of I_Effect_AssetChanger that is able
	 * to run a bts.actions.I_Effect_AssetChanger.
	 * 
	 * @param asset
	 *            value of the parameter "asset", or null in case it should be
	 *            read from the context. If null,
	 *            <code>assetLoc<code> cannot be null.
	 * @param assetLoc
	 *            in case <code>asset</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 * @param duration
	 *            value of the parameter "duration", or null in case it should
	 *            be read from the context. If null,
	 *            <code>durationLoc<code> cannot be null.
	 * @param durationLoc
	 *            in case <code>duration</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 */
	public I_Effect_AssetChanger(bts.actions.I_Effect_AssetChanger modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.String asset,
			java.lang.String assetLoc, java.lang.Float duration,
			java.lang.String durationLoc) {
		super(modelTask, executor, parent);

		this.asset = asset;
		this.assetLoc = assetLoc;
		this.duration = duration;
		this.durationLoc = durationLoc;
	}

	/**
	 * Returns the value of the parameter "asset", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.String getAsset() {
		if (this.asset != null) {
			return this.asset;
		} else {
			return (java.lang.String) this.getContext().getVariable(
					this.assetLoc);
		}
	}

	/**
	 * Returns the value of the parameter "duration", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Float getDuration() {
		if (this.duration != null) {
			return this.duration;
		} else {
			return (java.lang.Float) this.getContext().getVariable(
					this.durationLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		IContext context = getContext();
		
		Entity target = (Entity)context.getVariable("target");
		target.AddEffect(Billboard.AssetChangerEffect(getAsset(), 0.001f, getDuration()));
		
		return jbt.execution.core.ExecutionTask.Status.SUCCESS;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}