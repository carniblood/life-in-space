// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/17/2016 15:21:23
// ******************************************************* 
package bts.actions.execution;

import jbt.execution.core.IContext;

import com.chernobyl.lifeinspace.Character;
import com.chernobyl.lifeinspace.Entity;
import com.chernobyl.lifeinspace.Monologues;

/** ExecutionAction class created from MMPM action I_Say. */
public class I_Say extends jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "textId" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Integer textId;
	/**
	 * Location, in the context, of the parameter "textId" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String textIdLoc;

	/**
	 * Constructor. Constructs an instance of I_Say that is able to run a
	 * bts.actions.I_Say.
	 * 
	 * @param textId
	 *            value of the parameter "textId", or null in case it should be
	 *            read from the context. If null,
	 *            <code>textIdLoc<code> cannot be null.
	 * @param textIdLoc
	 *            in case <code>textId</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public I_Say(bts.actions.I_Say modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.Integer textId,
			java.lang.String textIdLoc) {
		super(modelTask, executor, parent);

		this.textId = textId;
		this.textIdLoc = textIdLoc;
	}

	/**
	 * Returns the value of the parameter "textId", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Integer getTextId() {
		if (this.textId != null) {
			return this.textId;
		} else {
			return (java.lang.Integer) this.getContext().getVariable(
					this.textIdLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		IContext context = getContext();

		Entity target = (Entity)context.getVariable("target");		
		Character actor = (Character)context.getVariable("actor");

		if (target.GetPlanet() != actor.GetPlanet())
			return jbt.execution.core.ExecutionTask.Status.FAILURE;
			
		actor.Say(Monologues.GetTextFromId(getTextId()));	
		
		return jbt.execution.core.ExecutionTask.Status.SUCCESS;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}