// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/09/2016 13:20:31
// ******************************************************* 
package bts.actions.execution;

import jbt.execution.core.IContext;

import com.chernobyl.lifeinspace.Character;
import com.chernobyl.lifeinspace.Planet;

/** ExecutionAction class created from MMPM action AI_MoveDiff. */
public class AI_MoveDiff extends jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "distance" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Float distance;
	/**
	 * Location, in the context, of the parameter "distance" in case its value
	 * is not specified at construction time. null otherwise.
	 */
	private java.lang.String distanceLoc;
	
	private float startAngle;

	/**
	 * Constructor. Constructs an instance of AI_MoveDiff that is able to run a
	 * bts.actions.AI_MoveDiff.
	 * 
	 * @param distance
	 *            value of the parameter "distance", or null in case it should
	 *            be read from the context. If null,
	 *            <code>distanceLoc<code> cannot be null.
	 * @param distanceLoc
	 *            in case <code>distance</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 */
	public AI_MoveDiff(bts.actions.AI_MoveDiff modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.Float distance,
			java.lang.String distanceLoc) {
		super(modelTask, executor, parent);

		this.distance = distance;
		this.distanceLoc = distanceLoc;
	}

	/**
	 * Returns the value of the parameter "distance", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Float getDistance() {
		if (this.distance != null) {
			return this.distance;
		} else {
			return (java.lang.Float) this.getContext().getVariable(
					this.distanceLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
		
		IContext context = getContext();
		Character agent = (Character)context.getVariable("agent");
		
		startAngle = agent.GetAngle();
		
		agent.MoveDiff(getDistance());
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		IContext context = getContext();
		Character agent = (Character)context.getVariable("agent");

		if (agent.IsMoving())
		{
			return jbt.execution.core.ExecutionTask.Status.RUNNING;			
		}
		
		Planet planet = agent.GetPlanet();
		float dist = Math.abs(planet.Distance(agent, startAngle));
		
		if (dist <= getDistance() + 0.01f)
			return jbt.execution.core.ExecutionTask.Status.SUCCESS;
		else
			return jbt.execution.core.ExecutionTask.Status.FAILURE;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}