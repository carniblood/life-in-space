// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/09/2016 13:20:31
// ******************************************************* 
package bts.actions.execution;

import jbt.execution.core.IContext;

import com.chernobyl.lifeinspace.Entity;
import com.chernobyl.lifeinspace.Planet;

/** ExecutionAction class created from MMPM action Entity_GetDistanceBetween. */
public class Entity_GetDistanceBetween extends
		jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "object1" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object object1;
	/**
	 * Location, in the context, of the parameter "object1" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String object1Loc;
	/**
	 * Value of the parameter "object2" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object object2;
	/**
	 * Location, in the context, of the parameter "object2" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String object2Loc;
	/**
	 * Value of the parameter "varName" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String varName;
	/**
	 * Location, in the context, of the parameter "varName" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String varNameLoc;

	/**
	 * Constructor. Constructs an instance of Entity_GetDistanceBetween that is
	 * able to run a bts.actions.Entity_GetDistanceBetween.
	 * 
	 * @param object1
	 *            value of the parameter "object1", or null in case it should be
	 *            read from the context. If null,
	 *            <code>object1Loc<code> cannot be null.
	 * @param object1Loc
	 *            in case <code>object1</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 * @param object2
	 *            value of the parameter "object2", or null in case it should be
	 *            read from the context. If null,
	 *            <code>object2Loc<code> cannot be null.
	 * @param object2Loc
	 *            in case <code>object2</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 * @param varName
	 *            value of the parameter "varName", or null in case it should be
	 *            read from the context. If null,
	 *            <code>varNameLoc<code> cannot be null.
	 * @param varNameLoc
	 *            in case <code>varName</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public Entity_GetDistanceBetween(
			bts.actions.Entity_GetDistanceBetween modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.Object object1,
			java.lang.String object1Loc, java.lang.Object object2,
			java.lang.String object2Loc, java.lang.String varName,
			java.lang.String varNameLoc) {
		super(modelTask, executor, parent);

		this.object1 = object1;
		this.object1Loc = object1Loc;
		this.object2 = object2;
		this.object2Loc = object2Loc;
		this.varName = varName;
		this.varNameLoc = varNameLoc;
	}

	/**
	 * Returns the value of the parameter "object1", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Object getObject1() {
		if (this.object1 != null) {
			return this.object1;
		} else {
			return (java.lang.Object) this.getContext().getVariable(
					this.object1Loc);
		}
	}

	/**
	 * Returns the value of the parameter "object2", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Object getObject2() {
		if (this.object2 != null) {
			return this.object2;
		} else {
			return (java.lang.Object) this.getContext().getVariable(
					this.object2Loc);
		}
	}

	/**
	 * Returns the value of the parameter "varName", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.String getVarName() {
		if (this.varName != null) {
			return this.varName;
		} else {
			return (java.lang.String) this.getContext().getVariable(
					this.varNameLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		IContext context = getContext();
		
		Entity agent = (Entity)context.getVariable("agent");		
		Entity entity1 = (Entity)getObject1();
		Entity entity2 = (Entity)getObject2();
		
		String varName = getVarName();
		if (varName == null || varName.isEmpty())
			return jbt.execution.core.ExecutionTask.Status.FAILURE;
		
		Planet planet = agent.GetPlanet();
		float distance = planet.Distance(entity1, entity2);
		context.setVariable(getVarName(), distance);
		
		return jbt.execution.core.ExecutionTask.Status.SUCCESS;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}