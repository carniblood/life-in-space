// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                MUST BE CAREFULLY COMPLETED              
//                                                         
//           ABSTRACT METHODS MUST BE IMPLEMENTED          
//                                                         
// Generated on 01/17/2016 03:02:48
// ******************************************************* 
package bts.actions.execution;

import jbt.execution.core.IContext;

import com.chernobyl.lifeinspace.Character;

/** ExecutionAction class created from MMPM action I_BlockMovement. */
public class I_BlockMovement extends
		jbt.execution.task.leaf.action.ExecutionAction {
	/**
	 * Value of the parameter "block" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Boolean block;
	/**
	 * Location, in the context, of the parameter "block" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String blockLoc;

	/**
	 * Constructor. Constructs an instance of I_BlockMovement that is able to
	 * run a bts.actions.I_BlockMovement.
	 * 
	 * @param block
	 *            value of the parameter "block", or null in case it should be
	 *            read from the context. If null,
	 *            <code>blockLoc<code> cannot be null.
	 * @param blockLoc
	 *            in case <code>block</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public I_BlockMovement(bts.actions.I_BlockMovement modelTask,
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent, java.lang.Boolean block,
			java.lang.String blockLoc) {
		super(modelTask, executor, parent);

		this.block = block;
		this.blockLoc = blockLoc;
	}

	/**
	 * Returns the value of the parameter "block", or null in case it has not
	 * been specified or it cannot be found in the context.
	 */
	public java.lang.Boolean getBlock() {
		if (this.block != null) {
			return this.block;
		} else {
			return (java.lang.Boolean) this.getContext().getVariable(
					this.blockLoc);
		}
	}

	protected void internalSpawn() {
		/*
		 * Do not remove this first line unless you know what it does and you
		 * need not do it.
		 */
		this.getExecutor().requestInsertionIntoList(
				jbt.execution.core.BTExecutor.BTExecutorList.TICKABLE, this);
	}

	protected jbt.execution.core.ExecutionTask.Status internalTick() {
		IContext context = getContext();
		
		Character actor = (Character)context.getVariable("actor");
		actor.BlockMovement((boolean)getBlock());
		
		return jbt.execution.core.ExecutionTask.Status.SUCCESS;
	}

	protected void internalTerminate() {
	}

	protected void restoreState(jbt.execution.core.ITaskState state) {
	}

	protected jbt.execution.core.ITaskState storeState() {
		return null;
	}

	protected jbt.execution.core.ITaskState storeTerminationState() {
		return null;
	}
}