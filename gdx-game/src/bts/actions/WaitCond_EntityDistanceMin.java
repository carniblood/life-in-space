// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/10/2016 22:00:12
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action WaitCond_EntityDistanceMin. */
public class WaitCond_EntityDistanceMin extends
		jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "entity" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object entity;
	/**
	 * Location, in the context, of the parameter "entity" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String entityLoc;
	/**
	 * Value of the parameter "distance" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Float distance;
	/**
	 * Location, in the context, of the parameter "distance" in case its value
	 * is not specified at construction time. null otherwise.
	 */
	private java.lang.String distanceLoc;

	/**
	 * Constructor. Constructs an instance of WaitCond_EntityDistanceMin.
	 * 
	 * @param entity
	 *            value of the parameter "entity", or null in case it should be
	 *            read from the context. If null, <code>entityLoc</code> cannot
	 *            be null.
	 * @param entityLoc
	 *            in case <code>entity</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 * @param distance
	 *            value of the parameter "distance", or null in case it should
	 *            be read from the context. If null, <code>distanceLoc</code>
	 *            cannot be null.
	 * @param distanceLoc
	 *            in case <code>distance</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 */
	public WaitCond_EntityDistanceMin(jbt.model.core.ModelTask guard,
			java.lang.Object entity, java.lang.String entityLoc,
			java.lang.Float distance, java.lang.String distanceLoc) {
		super(guard);
		this.entity = entity;
		this.entityLoc = entityLoc;
		this.distance = distance;
		this.distanceLoc = distanceLoc;
	}

	/**
	 * Returns a bts.actions.execution.WaitCond_EntityDistanceMin task that is
	 * able to run this task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.WaitCond_EntityDistanceMin(this,
				executor, parent, this.entity, this.entityLoc, this.distance,
				this.distanceLoc);
	}
}