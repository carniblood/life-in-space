// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/09/2016 13:20:31
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action I_Effect_AssetChanger. */
public class I_Effect_AssetChanger extends
		jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "asset" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String asset;
	/**
	 * Location, in the context, of the parameter "asset" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String assetLoc;
	/**
	 * Value of the parameter "duration" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Float duration;
	/**
	 * Location, in the context, of the parameter "duration" in case its value
	 * is not specified at construction time. null otherwise.
	 */
	private java.lang.String durationLoc;

	/**
	 * Constructor. Constructs an instance of I_Effect_AssetChanger.
	 * 
	 * @param asset
	 *            value of the parameter "asset", or null in case it should be
	 *            read from the context. If null, <code>assetLoc</code> cannot
	 *            be null.
	 * @param assetLoc
	 *            in case <code>asset</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 * @param duration
	 *            value of the parameter "duration", or null in case it should
	 *            be read from the context. If null, <code>durationLoc</code>
	 *            cannot be null.
	 * @param durationLoc
	 *            in case <code>duration</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 */
	public I_Effect_AssetChanger(jbt.model.core.ModelTask guard,
			java.lang.String asset, java.lang.String assetLoc,
			java.lang.Float duration, java.lang.String durationLoc) {
		super(guard);
		this.asset = asset;
		this.assetLoc = assetLoc;
		this.duration = duration;
		this.durationLoc = durationLoc;
	}

	/**
	 * Returns a bts.actions.execution.I_Effect_AssetChanger task that is able
	 * to run this task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.I_Effect_AssetChanger(this, executor,
				parent, this.asset, this.assetLoc, this.duration,
				this.durationLoc);
	}
}