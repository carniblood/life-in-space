// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/18/2016 11:30:20
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action Planet_GetShelters. */
public class Planet_GetShelters extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "stackName" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String stackName;
	/**
	 * Location, in the context, of the parameter "stackName" in case its value
	 * is not specified at construction time. null otherwise.
	 */
	private java.lang.String stackNameLoc;

	/**
	 * Constructor. Constructs an instance of Planet_GetShelters.
	 * 
	 * @param stackName
	 *            value of the parameter "stackName", or null in case it should
	 *            be read from the context. If null, <code>stackNameLoc</code>
	 *            cannot be null.
	 * @param stackNameLoc
	 *            in case <code>stackName</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 */
	public Planet_GetShelters(jbt.model.core.ModelTask guard,
			java.lang.String stackName, java.lang.String stackNameLoc) {
		super(guard);
		this.stackName = stackName;
		this.stackNameLoc = stackNameLoc;
	}

	/**
	 * Returns a bts.actions.execution.Planet_GetShelters task that is able to
	 * run this task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.Planet_GetShelters(this, executor,
				parent, this.stackName, this.stackNameLoc);
	}
}