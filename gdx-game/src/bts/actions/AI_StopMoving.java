// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/18/2016 02:18:16
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action AI_StopMoving. */
public class AI_StopMoving extends jbt.model.task.leaf.action.ModelAction {

	/** Constructor. Constructs an instance of AI_StopMoving. */
	public AI_StopMoving(jbt.model.core.ModelTask guard) {
		super(guard);
	}

	/**
	 * Returns a bts.actions.execution.AI_StopMoving task that is able to run
	 * this task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.AI_StopMoving(this, executor, parent);
	}
}