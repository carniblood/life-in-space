// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/09/2016 13:20:31
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action Stack_Push. */
public class Stack_Push extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "stackTo" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String stackTo;
	/**
	 * Location, in the context, of the parameter "stackTo" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String stackToLoc;
	/**
	 * Value of the parameter "object" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object object;
	/**
	 * Location, in the context, of the parameter "object" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String objectLoc;

	/**
	 * Constructor. Constructs an instance of Stack_Push.
	 * 
	 * @param stackTo
	 *            value of the parameter "stackTo", or null in case it should be
	 *            read from the context. If null, <code>stackToLoc</code> cannot
	 *            be null.
	 * @param stackToLoc
	 *            in case <code>stackTo</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 * @param object
	 *            value of the parameter "object", or null in case it should be
	 *            read from the context. If null, <code>objectLoc</code> cannot
	 *            be null.
	 * @param objectLoc
	 *            in case <code>object</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public Stack_Push(jbt.model.core.ModelTask guard, java.lang.String stackTo,
			java.lang.String stackToLoc, java.lang.Object object,
			java.lang.String objectLoc) {
		super(guard);
		this.stackTo = stackTo;
		this.stackToLoc = stackToLoc;
		this.object = object;
		this.objectLoc = objectLoc;
	}

	/**
	 * Returns a bts.actions.execution.Stack_Push task that is able to run this
	 * task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.Stack_Push(this, executor, parent,
				this.stackTo, this.stackToLoc, this.object, this.objectLoc);
	}
}