// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/09/2016 13:20:31
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action AI_ChangeDir. */
public class AI_ChangeDir extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "newDir" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Integer newDir;
	/**
	 * Location, in the context, of the parameter "newDir" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String newDirLoc;

	/**
	 * Constructor. Constructs an instance of AI_ChangeDir.
	 * 
	 * @param newDir
	 *            value of the parameter "newDir", or null in case it should be
	 *            read from the context. If null, <code>newDirLoc</code> cannot
	 *            be null.
	 * @param newDirLoc
	 *            in case <code>newDir</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public AI_ChangeDir(jbt.model.core.ModelTask guard,
			java.lang.Integer newDir, java.lang.String newDirLoc) {
		super(guard);
		this.newDir = newDir;
		this.newDirLoc = newDirLoc;
	}

	/**
	 * Returns a bts.actions.execution.AI_ChangeDir task that is able to run
	 * this task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.AI_ChangeDir(this, executor, parent,
				this.newDir, this.newDirLoc);
	}
}