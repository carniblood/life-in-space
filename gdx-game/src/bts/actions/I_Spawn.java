// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/12/2016 10:16:53
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action I_Spawn. */
public class I_Spawn extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "type" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String type;
	/**
	 * Location, in the context, of the parameter "type" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String typeLoc;

	/**
	 * Constructor. Constructs an instance of I_Spawn.
	 * 
	 * @param type
	 *            value of the parameter "type", or null in case it should be
	 *            read from the context. If null, <code>typeLoc</code> cannot be
	 *            null.
	 * @param typeLoc
	 *            in case <code>type</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public I_Spawn(jbt.model.core.ModelTask guard, java.lang.String type,
			java.lang.String typeLoc) {
		super(guard);
		this.type = type;
		this.typeLoc = typeLoc;
	}

	/**
	 * Returns a bts.actions.execution.I_Spawn task that is able to run this
	 * task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.I_Spawn(this, executor, parent,
				this.type, this.typeLoc);
	}
}