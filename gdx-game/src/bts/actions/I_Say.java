// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/17/2016 15:21:23
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action I_Say. */
public class I_Say extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "textId" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Integer textId;
	/**
	 * Location, in the context, of the parameter "textId" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String textIdLoc;

	/**
	 * Constructor. Constructs an instance of I_Say.
	 * 
	 * @param textId
	 *            value of the parameter "textId", or null in case it should be
	 *            read from the context. If null, <code>textIdLoc</code> cannot
	 *            be null.
	 * @param textIdLoc
	 *            in case <code>textId</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public I_Say(jbt.model.core.ModelTask guard, java.lang.Integer textId,
			java.lang.String textIdLoc) {
		super(guard);
		this.textId = textId;
		this.textIdLoc = textIdLoc;
	}

	/**
	 * Returns a bts.actions.execution.I_Say task that is able to run this task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.I_Say(this, executor, parent,
				this.textId, this.textIdLoc);
	}
}