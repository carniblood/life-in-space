// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/17/2016 03:02:47
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action I_BlockMovement. */
public class I_BlockMovement extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "block" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Boolean block;
	/**
	 * Location, in the context, of the parameter "block" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String blockLoc;

	/**
	 * Constructor. Constructs an instance of I_BlockMovement.
	 * 
	 * @param block
	 *            value of the parameter "block", or null in case it should be
	 *            read from the context. If null, <code>blockLoc</code> cannot
	 *            be null.
	 * @param blockLoc
	 *            in case <code>block</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public I_BlockMovement(jbt.model.core.ModelTask guard,
			java.lang.Boolean block, java.lang.String blockLoc) {
		super(guard);
		this.block = block;
		this.blockLoc = blockLoc;
	}

	/**
	 * Returns a bts.actions.execution.I_BlockMovement task that is able to run
	 * this task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.I_BlockMovement(this, executor,
				parent, this.block, this.blockLoc);
	}
}