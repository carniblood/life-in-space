// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/17/2016 23:27:00
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action Entity_GetNext. */
public class Entity_GetNext extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "object" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object object;
	/**
	 * Location, in the context, of the parameter "object" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String objectLoc;
	/**
	 * Value of the parameter "next" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object next;
	/**
	 * Location, in the context, of the parameter "next" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String nextLoc;

	/**
	 * Constructor. Constructs an instance of Entity_GetNext.
	 * 
	 * @param object
	 *            value of the parameter "object", or null in case it should be
	 *            read from the context. If null, <code>objectLoc</code> cannot
	 *            be null.
	 * @param objectLoc
	 *            in case <code>object</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 * @param next
	 *            value of the parameter "next", or null in case it should be
	 *            read from the context. If null, <code>nextLoc</code> cannot be
	 *            null.
	 * @param nextLoc
	 *            in case <code>next</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public Entity_GetNext(jbt.model.core.ModelTask guard,
			java.lang.Object object, java.lang.String objectLoc,
			java.lang.Object next, java.lang.String nextLoc) {
		super(guard);
		this.object = object;
		this.objectLoc = objectLoc;
		this.next = next;
		this.nextLoc = nextLoc;
	}

	/**
	 * Returns a bts.actions.execution.Entity_GetNext task that is able to run
	 * this task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.Entity_GetNext(this, executor, parent,
				this.object, this.objectLoc, this.next, this.nextLoc);
	}
}