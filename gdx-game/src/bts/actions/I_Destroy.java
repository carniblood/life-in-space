// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/12/2016 10:16:53
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action I_Destroy. */
public class I_Destroy extends jbt.model.task.leaf.action.ModelAction {

	/** Constructor. Constructs an instance of I_Destroy. */
	public I_Destroy(jbt.model.core.ModelTask guard) {
		super(guard);
	}

	/**
	 * Returns a bts.actions.execution.I_Destroy task that is able to run this
	 * task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.I_Destroy(this, executor, parent);
	}
}