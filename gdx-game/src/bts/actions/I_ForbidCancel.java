// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/17/2016 03:02:48
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action I_ForbidCancel. */
public class I_ForbidCancel extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "forbid" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Boolean forbid;
	/**
	 * Location, in the context, of the parameter "forbid" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String forbidLoc;

	/**
	 * Constructor. Constructs an instance of I_ForbidCancel.
	 * 
	 * @param forbid
	 *            value of the parameter "forbid", or null in case it should be
	 *            read from the context. If null, <code>forbidLoc</code> cannot
	 *            be null.
	 * @param forbidLoc
	 *            in case <code>forbid</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public I_ForbidCancel(jbt.model.core.ModelTask guard,
			java.lang.Boolean forbid, java.lang.String forbidLoc) {
		super(guard);
		this.forbid = forbid;
		this.forbidLoc = forbidLoc;
	}

	/**
	 * Returns a bts.actions.execution.I_ForbidCancel task that is able to run
	 * this task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.I_ForbidCancel(this, executor, parent,
				this.forbid, this.forbidLoc);
	}
}