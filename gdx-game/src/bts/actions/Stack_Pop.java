// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/09/2016 13:20:31
// ******************************************************* 
package bts.actions;

/** ModelAction class created from MMPM action Stack_Pop. */
public class Stack_Pop extends jbt.model.task.leaf.action.ModelAction {
	/**
	 * Value of the parameter "stackFrom" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.String stackFrom;
	/**
	 * Location, in the context, of the parameter "stackFrom" in case its value
	 * is not specified at construction time. null otherwise.
	 */
	private java.lang.String stackFromLoc;
	/**
	 * Value of the parameter "object" in case its value is specified at
	 * construction time. null otherwise.
	 */
	private java.lang.Object object;
	/**
	 * Location, in the context, of the parameter "object" in case its value is
	 * not specified at construction time. null otherwise.
	 */
	private java.lang.String objectLoc;

	/**
	 * Constructor. Constructs an instance of Stack_Pop.
	 * 
	 * @param stackFrom
	 *            value of the parameter "stackFrom", or null in case it should
	 *            be read from the context. If null, <code>stackFromLoc</code>
	 *            cannot be null.
	 * @param stackFromLoc
	 *            in case <code>stackFrom</code> is null, this variable
	 *            represents the place in the context where the parameter's
	 *            value will be retrieved from.
	 * @param object
	 *            value of the parameter "object", or null in case it should be
	 *            read from the context. If null, <code>objectLoc</code> cannot
	 *            be null.
	 * @param objectLoc
	 *            in case <code>object</code> is null, this variable represents
	 *            the place in the context where the parameter's value will be
	 *            retrieved from.
	 */
	public Stack_Pop(jbt.model.core.ModelTask guard,
			java.lang.String stackFrom, java.lang.String stackFromLoc,
			java.lang.Object object, java.lang.String objectLoc) {
		super(guard);
		this.stackFrom = stackFrom;
		this.stackFromLoc = stackFromLoc;
		this.object = object;
		this.objectLoc = objectLoc;
	}

	/**
	 * Returns a bts.actions.execution.Stack_Pop task that is able to run this
	 * task.
	 */
	public jbt.execution.core.ExecutionTask createExecutor(
			jbt.execution.core.BTExecutor executor,
			jbt.execution.core.ExecutionTask parent) {
		return new bts.actions.execution.Stack_Pop(this, executor, parent,
				this.stackFrom, this.stackFromLoc, this.object, this.objectLoc);
	}
}