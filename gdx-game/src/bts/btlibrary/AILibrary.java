// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/28/2016 10:49:42
// ******************************************************* 
package bts.btlibrary;

/**
 * BT library that includes the trees read from the following files:
 * <ul>
 * <li>./config/ai/test.xbt</li>
 * <li>./config/ai/animal.xbt</li>
 * <li>./config/ai/triggers/introPlanet/intro_monologue.xbt</li>
 * <li>./config/ai/triggers/introPlanet/planet_zone_tuto1.xbt</li>
 * <li>./config/ai/triggers/satelliteTuto1/discover_rocks.xbt</li>
 * <li>./config/ai/triggers/satelliteTuto1/planet_zone_tuto2.xbt</li>
 * <li>./config/ai/triggers/satelliteTuto1/planet_zone_first_back.xbt</li>
 * <li>./config/ai/triggers/satelliteTuto2/tuto2_monologue.xbt</li>
 * <li>./config/ai/triggers/satelliteTuto2/discover_vegetation_tuto.xbt</li>
 * <li>./config/ai/triggers/satelliteTuto2/planet_zone_big_planet.xbt</li>
 * <li>./config/ai/triggers/satelliteTuto2/planet_zone_tuto1_back.xbt</li>
 * <li>./config/ai/triggers/bigPlanet/big_planet_monologue.xbt</li>
 * <li>./config/ai/triggers/bushPlanet/bushZone1.xbt</li>
 * <li>./config/ai/triggers/bushPlanet/bushZone2.xbt</li>
 * <li>./config/ai/triggers/animalPlanet/animalsZone1.xbt</li>
 * <li>./config/ai/triggers/animalPlanet/animalsZone2.xbt</li>
 * <li>./config/ai/triggers/vegetation_cycle.xbt</li>
 * <li>./config/ai/triggers/seed_cycle.xbt</li>
 * <li>./config/ai/triggers/discover_vegetation.xbt</li>
 * <li>./config/ai/triggers/again_vegetation.xbt</li>
 * <li>./config/ai/triggers/planet_zone.xbt</li>
 * </ul>
 */
public class AILibrary implements jbt.execution.core.IBTLibrary {
	/** Tree generated from file ./config/ai/test.xbt. */
	private static jbt.model.core.ModelTask Test;
	/** Tree generated from file ./config/ai/animal.xbt. */
	private static jbt.model.core.ModelTask Animal;
	/**
	 * Tree generated from file
	 * ./config/ai/triggers/introPlanet/intro_monologue.xbt.
	 */
	private static jbt.model.core.ModelTask IntroMonologue;
	/**
	 * Tree generated from file
	 * ./config/ai/triggers/introPlanet/planet_zone_tuto1.xbt.
	 */
	private static jbt.model.core.ModelTask PlanetZoneTuto1;
	/**
	 * Tree generated from file
	 * ./config/ai/triggers/satelliteTuto1/discover_rocks.xbt.
	 */
	private static jbt.model.core.ModelTask DiscoverRocks;
	/**
	 * Tree generated from file
	 * ./config/ai/triggers/satelliteTuto1/planet_zone_tuto2.xbt.
	 */
	private static jbt.model.core.ModelTask PlanetZoneTuto2;
	/**
	 * Tree generated from file
	 * ./config/ai/triggers/satelliteTuto1/planet_zone_first_back.xbt.
	 */
	private static jbt.model.core.ModelTask PlanetZoneFirstBack;
	/**
	 * Tree generated from file
	 * ./config/ai/triggers/satelliteTuto2/tuto2_monologue.xbt.
	 */
	private static jbt.model.core.ModelTask Tuto2Monologue;
	/**
	 * Tree generated from file
	 * ./config/ai/triggers/satelliteTuto2/discover_vegetation_tuto.xbt.
	 */
	private static jbt.model.core.ModelTask DiscoverVegetationTuto;
	/**
	 * Tree generated from file
	 * ./config/ai/triggers/satelliteTuto2/planet_zone_big_planet.xbt.
	 */
	private static jbt.model.core.ModelTask PlanetZoneBigPlanet;
	/**
	 * Tree generated from file
	 * ./config/ai/triggers/satelliteTuto2/planet_zone_tuto1_back.xbt.
	 */
	private static jbt.model.core.ModelTask PlanetZoneTuto1Back;
	/**
	 * Tree generated from file
	 * ./config/ai/triggers/bigPlanet/big_planet_monologue.xbt.
	 */
	private static jbt.model.core.ModelTask BigPlanetMonologue;
	/** Tree generated from file ./config/ai/triggers/bushPlanet/bushZone1.xbt. */
	private static jbt.model.core.ModelTask BushZone1;
	/** Tree generated from file ./config/ai/triggers/bushPlanet/bushZone2.xbt. */
	private static jbt.model.core.ModelTask BushZone2;
	/**
	 * Tree generated from file
	 * ./config/ai/triggers/animalPlanet/animalsZone1.xbt.
	 */
	private static jbt.model.core.ModelTask AnimalsZone1;
	/**
	 * Tree generated from file
	 * ./config/ai/triggers/animalPlanet/animalsZone2.xbt.
	 */
	private static jbt.model.core.ModelTask AnimalsZone2;
	/** Tree generated from file ./config/ai/triggers/vegetation_cycle.xbt. */
	private static jbt.model.core.ModelTask VegetationCycle;
	/** Tree generated from file ./config/ai/triggers/seed_cycle.xbt. */
	private static jbt.model.core.ModelTask SeedCycle;
	/** Tree generated from file ./config/ai/triggers/discover_vegetation.xbt. */
	private static jbt.model.core.ModelTask DiscoverVegetation;
	/** Tree generated from file ./config/ai/triggers/again_vegetation.xbt. */
	private static jbt.model.core.ModelTask AgainVegetation;
	/** Tree generated from file ./config/ai/triggers/planet_zone.xbt. */
	private static jbt.model.core.ModelTask PlanetZone;

	/* Static initialization of all the trees. */
	static {
		Test = new jbt.model.task.composite.ModelSequence(
				null,
				new bts.actions.Planet_GetPlayer(null, null, "player"),
				new jbt.model.task.decorator.ModelRepeat(
						null,
						new jbt.model.task.composite.ModelSequence(
								null,
								new bts.actions.Planet_GetVegetations(null,
										"vegetations", null),
								new bts.actions.Stack_Shuffle(null,
										"vegetations", null),
								new jbt.model.task.decorator.ModelUntilFail(
										null,
										new jbt.model.task.composite.ModelSequence(
												null,
												new bts.actions.Stack_Pop(null,
														"vegetations", null,
														null, "vegetation"),
												new bts.actions.Entity_GetDirectionBetween(
														null, null, "agent",
														null, "vegetation",
														"vegDist", null),
												new bts.actions.Entity_GetPosition(
														null, null,
														"vegetation", "vegPos",
														null),
												new bts.actions.AI_MoveTo(null,
														null, "vegPos"),
												new jbt.model.task.leaf.ModelWait(
														null, 100),
												new jbt.model.task.composite.ModelSelector(
														null,
														new jbt.model.task.composite.ModelSequence(
																null,
																new bts.actions.Integer_IsGreater(
																		null,
																		null,
																		"vegDist",
																		(int) 0,
																		null),
																new bts.actions.AI_ChangeDir(
																		null,
																		(int) -1,
																		null),
																new jbt.model.task.leaf.ModelWait(
																		null,
																		100),
																new bts.actions.AI_ChangeDir(
																		null,
																		(int) 1,
																		null)),
														new jbt.model.task.composite.ModelSequence(
																null,
																new bts.actions.Integer_IsSmaller(
																		null,
																		null,
																		"vegDist",
																		(int) 0,
																		null),
																new bts.actions.AI_ChangeDir(
																		null,
																		(int) 1,
																		null),
																new jbt.model.task.leaf.ModelWait(
																		null,
																		100),
																new bts.actions.AI_ChangeDir(
																		null,
																		(int) -1,
																		null))),
												new jbt.model.task.leaf.ModelWait(
														null, 100))))));

		Animal = new jbt.model.task.decorator.ModelRepeat(
				null,
				new jbt.model.task.composite.ModelParallel(
						null,
						jbt.model.task.composite.ModelParallel.ParallelPolicy.SEQUENCE_POLICY,
						new jbt.model.task.decorator.ModelRepeat(
								null,
								new jbt.model.task.composite.ModelSequence(
										null,
										new bts.actions.AI_Move(null),
										new jbt.model.task.decorator.ModelUntilFail(
												null,
												new jbt.model.task.decorator.ModelInverter(
														null,
														new bts.actions.IsNull(
																null, null,
																"curTarget"))))),
						new jbt.model.task.decorator.ModelInverter(
								null,
								new jbt.model.task.decorator.ModelUntilFail(
										null,
										new jbt.model.task.composite.ModelSequence(
												null,
												new bts.actions.AI_GetDir(null,
														"curDir", null),
												new jbt.model.task.composite.ModelSelector(
														null,
														new jbt.model.task.composite.ModelSequence(
																null,
																new bts.actions.Integer_IsGreater(
																		null,
																		null,
																		"curDir",
																		(int) 0,
																		null),
																new bts.actions.SetVariable(
																		null,
																		"next",
																		null,
																		null,
																		"agent"),
																new jbt.model.task.decorator.ModelUntilFail(
																		null,
																		new jbt.model.task.composite.ModelSequence(
																				null,
																				new bts.actions.Entity_GetNext(
																						null,
																						null,
																						"next",
																						null,
																						"next"),
																				new bts.actions.Entity_IsHolded(
																						null,
																						null,
																						"next")))),
														new jbt.model.task.composite.ModelSequence(
																null,
																new bts.actions.SetVariable(
																		null,
																		"next",
																		null,
																		null,
																		"agent"),
																new jbt.model.task.decorator.ModelUntilFail(
																		null,
																		new jbt.model.task.composite.ModelSequence(
																				null,
																				new bts.actions.Entity_GetPrev(
																						null,
																						null,
																						"next",
																						null,
																						"next"),
																				new bts.actions.Entity_IsHolded(
																						null,
																						null,
																						"next"))))),
												new jbt.model.task.composite.ModelSelector(
														null,
														new bts.actions.IsNull(
																null, null,
																"next"),
														new jbt.model.task.composite.ModelSequence(
																null,
																new bts.actions.Entity_IsType(
																		null,
																		null,
																		"next",
																		"Seed",
																		null),
																new bts.actions.SetVariable(
																		null,
																		"curTarget",
																		null,
																		null,
																		"next"),
																new bts.actions.AI_StopMoving(
																		null),
																new bts.actions.AI_Interact(
																		null,
																		null,
																		"next"),
																new bts.actions.SetVariable(
																		null,
																		"next",
																		null,
																		null,
																		"nullObject"),
																new bts.actions.SetVariable(
																		null,
																		"curTarget",
																		null,
																		null,
																		"nullObject")),
														new jbt.model.task.composite.ModelSequence(
																null,
																new bts.actions.Entity_IsType(
																		null,
																		null,
																		"next",
																		"Shelter",
																		null),
																new bts.actions.SetVariable(
																		null,
																		"curTarget",
																		null,
																		null,
																		"next"),
																new bts.actions.AI_StopMoving(
																		null),
																new bts.actions.AI_Interact(
																		null,
																		null,
																		"next"),
																new bts.actions.SetVariable(
																		null,
																		"next",
																		null,
																		null,
																		"nullObject"),
																new bts.actions.SetVariable(
																		null,
																		"curTarget",
																		null,
																		null,
																		"nullObject")),
														new jbt.model.task.composite.ModelSequence(
																null,
																new bts.actions.Entity_IsCollider(
																		null,
																		null,
																		"next"),
																new bts.actions.Entity_GetDistance(
																		null,
																		null,
																		"next",
																		"nextDist",
																		null),
																new bts.actions.Float_IsSmaller(
																		null,
																		null,
																		"nextDist",
																		(float) 0.3f,
																		null),
																new bts.actions.Float_IsGreater(
																		null,
																		null,
																		"nextDist",
																		(float) -0.3f,
																		null),
																new bts.actions.AI_StopMoving(
																		null),
																new jbt.model.task.composite.ModelSelector(
																		null,
																		new jbt.model.task.composite.ModelSequence(
																				null,
																				new bts.actions.Integer_IsGreater(
																						null,
																						null,
																						"curDir",
																						(int) 0,
																						null),
																				new bts.actions.AI_ChangeDir(
																						null,
																						(int) -1,
																						null)),
																		new bts.actions.AI_ChangeDir(
																				null,
																				(int) 1,
																				null))),
														new bts.actions.SetVariable(
																null, "next",
																null, null,
																"nullObject")),
												new bts.actions.IsNull(null,
														null, "next"))))));

		IntroMonologue = new jbt.model.task.composite.ModelSequence(
				null,
				new bts.actions.Planet_GetPlayer(null, null, "player"),
				new jbt.model.task.composite.ModelParallel(
						null,
						jbt.model.task.composite.ModelParallel.ParallelPolicy.SEQUENCE_POLICY,
						new jbt.model.task.composite.ModelSequence(
								null,
								new bts.actions.WaitCond_EntityDistanceMin(
										null, null, "player", (float) 1.f, null)),
						new bts.actions.AI_Trigger(null, "Intro", null, null,
								"player")),
				new jbt.model.task.composite.ModelParallel(
						null,
						jbt.model.task.composite.ModelParallel.ParallelPolicy.SEQUENCE_POLICY,
						new jbt.model.task.composite.ModelSequence(
								null,
								new bts.actions.WaitCond_EntityDistanceMin(
										null, null, "player", (float) 2.f, null)),
						new bts.actions.AI_Trigger(null, "Intro2", null, null,
								"player")),
				new jbt.model.task.composite.ModelParallel(
						null,
						jbt.model.task.composite.ModelParallel.ParallelPolicy.SEQUENCE_POLICY,
						new jbt.model.task.composite.ModelSequence(
								null,
								new bts.actions.WaitCond_EntityDistanceMin(
										null, null, "player", (float) 3.f, null)),
						new bts.actions.AI_Trigger(null, "Intro3", null, null,
								"player")), new bts.actions.AI_Trigger(null,
						"Intro4", null, null, "player"));

		PlanetZoneTuto1 = new jbt.model.task.composite.ModelSequence(
				null,
				new bts.actions.Planet_GetPlayer(null, null, "player"),
				new jbt.model.task.decorator.ModelRepeat(
						null,
						new jbt.model.task.composite.ModelSequence(
								null,
								new bts.actions.WaitCond_EntityDistanceMax(
										null, null, "player", (float) 0.2, null),
								new bts.actions.AI_Trigger(null,
										"Tuto1SwitchOn", null, null, "player"),
								new bts.actions.WaitCond_EntityDistanceMin(
										null, null, "player", (float) 0.5, null),
								new bts.actions.AI_Trigger(null,
										"Tuto1SwitchOff", null, null, "player"))));

		DiscoverRocks = new jbt.model.task.composite.ModelSequence(
				null,
				new bts.actions.Planet_GetPlayer(null, null, "player"),
				new bts.actions.WaitCond_EntityDistanceMax(null, null,
						"player", (float) 0.1, null),
				new bts.actions.Counter_IncrAndGet(null, "RockTutoCounter",
						null, "RockTutoCount", null),
				new jbt.model.task.composite.ModelSelector(null,
						new jbt.model.task.composite.ModelSequence(null,
								new bts.actions.Integer_AreEqual(null, null,
										"RockTutoCount", (int) 1, null),
								new bts.actions.AI_Trigger(null,
										"FoundRockTuto", null, null, "player"))));

		PlanetZoneTuto2 = new jbt.model.task.composite.ModelSequence(
				null,
				new bts.actions.Planet_GetPlayer(null, null, "player"),
				new jbt.model.task.decorator.ModelRepeat(
						null,
						new jbt.model.task.composite.ModelSequence(
								null,
								new bts.actions.WaitCond_EntityDistanceMax(
										null, null, "player", (float) 0.2, null),
								new bts.actions.AI_Trigger(null,
										"Tuto2SwitchOn", null, null, "player"),
								new bts.actions.WaitCond_EntityDistanceMin(
										null, null, "player", (float) 0.5, null),
								new bts.actions.AI_Trigger(null,
										"Tuto2SwitchOff", null, null, "player"))));

		PlanetZoneFirstBack = new jbt.model.task.composite.ModelSequence(
				null,
				new bts.actions.Planet_GetPlayer(null, null, "player"),
				new jbt.model.task.decorator.ModelRepeat(
						null,
						new jbt.model.task.composite.ModelSequence(
								null,
								new bts.actions.WaitCond_EntityDistanceMax(
										null, null, "player", (float) 0.2, null),
								new bts.actions.AI_Trigger(null,
										"FirstSwitchBackOn", null, null,
										"player"),
								new bts.actions.WaitCond_EntityDistanceMin(
										null, null, "player", (float) 0.5, null),
								new bts.actions.AI_Trigger(null,
										"FirstSwitchBackOff", null, null,
										"player"))));

		Tuto2Monologue = new jbt.model.task.composite.ModelSequence(null,
				new bts.actions.Planet_GetPlayer(null, null, "player"),
				new bts.actions.AI_Trigger(null, "IntroTuto2", null, null,
						"player"));

		DiscoverVegetationTuto = new jbt.model.task.composite.ModelSequence(
				null, new bts.actions.Planet_GetPlayer(null, null, "player"),
				new bts.actions.WaitCond_EntityDistanceMax(null, null,
						"player", (float) 0.6, null),
				new bts.actions.AI_Trigger(null, "FoundVegetationTuto", null,
						null, "player"));

		PlanetZoneBigPlanet = new jbt.model.task.composite.ModelSequence(
				null,
				new bts.actions.Planet_GetPlayer(null, null, "player"),
				new jbt.model.task.decorator.ModelRepeat(
						null,
						new jbt.model.task.composite.ModelSequence(
								null,
								new bts.actions.WaitCond_EntityDistanceMax(
										null, null, "player", (float) 0.2, null),
								new bts.actions.AI_Trigger(null,
										"BigPlanetSwitchOn", null, null,
										"player"),
								new bts.actions.WaitCond_EntityDistanceMin(
										null, null, "player", (float) 0.5, null),
								new bts.actions.AI_Trigger(null,
										"BigPlanetSwitchOff", null, null,
										"player"))));

		PlanetZoneTuto1Back = new jbt.model.task.composite.ModelSequence(
				null,
				new bts.actions.Planet_GetPlayer(null, null, "player"),
				new jbt.model.task.decorator.ModelRepeat(
						null,
						new jbt.model.task.composite.ModelSequence(
								null,
								new bts.actions.WaitCond_EntityDistanceMax(
										null, null, "player", (float) 0.2, null),
								new bts.actions.AI_Trigger(null,
										"Tuto1SwitchBackOn", null, null,
										"player"),
								new bts.actions.WaitCond_EntityDistanceMin(
										null, null, "player", (float) 0.5, null),
								new bts.actions.AI_Trigger(null,
										"Tuto1SwitchBackOff", null, null,
										"player"))));

		BigPlanetMonologue = new jbt.model.task.composite.ModelSequence(null,
				new bts.actions.Planet_GetPlayer(null, null, "player"),
				new bts.actions.AI_Trigger(null, "IntroBigPlanet", null, null,
						"player"));

		BushZone1 = new jbt.model.task.composite.ModelSequence(
				null,
				new bts.actions.Planet_GetPlayer(null, null, "player"),
				new jbt.model.task.decorator.ModelRepeat(
						null,
						new jbt.model.task.composite.ModelSequence(
								null,
								new bts.actions.WaitCond_EntityDistanceMax(
										null, null, "player", (float) 0.2, null),
								new bts.actions.AI_Trigger(null, "Bush1", null,
										null, "player"),
								new bts.actions.WaitCond_EntityDistanceMin(
										null, null, "player", (float) 0.5, null))));

		BushZone2 = new jbt.model.task.composite.ModelSequence(
				null,
				new bts.actions.Planet_GetPlayer(null, null, "player"),
				new jbt.model.task.decorator.ModelRepeat(
						null,
						new jbt.model.task.composite.ModelSequence(
								null,
								new bts.actions.WaitCond_EntityDistanceMax(
										null, null, "player", (float) 0.2, null),
								new bts.actions.AI_Trigger(null, "Bush2", null,
										null, "player"),
								new bts.actions.WaitCond_EntityDistanceMin(
										null, null, "player", (float) 0.5, null))));

		AnimalsZone1 = new jbt.model.task.composite.ModelSequence(
				null,
				new bts.actions.Planet_GetPlayer(null, null, "player"),
				new jbt.model.task.decorator.ModelRepeat(
						null,
						new jbt.model.task.composite.ModelSequence(
								null,
								new bts.actions.WaitCond_EntityDistanceMax(
										null, null, "player", (float) 0.2, null),
								new bts.actions.AI_Trigger(null, "Animals1",
										null, null, "player"),
								new bts.actions.WaitCond_EntityDistanceMin(
										null, null, "player", (float) 0.5, null))));

		AnimalsZone2 = new jbt.model.task.composite.ModelSequence(
				null,
				new bts.actions.Planet_GetPlayer(null, null, "player"),
				new jbt.model.task.decorator.ModelRepeat(
						null,
						new jbt.model.task.composite.ModelSequence(
								null,
								new bts.actions.WaitCond_EntityDistanceMax(
										null, null, "player", (float) 0.2, null),
								new bts.actions.AI_Trigger(null, "Animals2",
										null, null, "player"),
								new bts.actions.WaitCond_EntityDistanceMin(
										null, null, "player", (float) 0.5, null))));

		VegetationCycle = new jbt.model.task.decorator.ModelRepeat(null,
				new jbt.model.task.composite.ModelSequence(null,
						new bts.actions.Planet_GetPlayer(null, null, "player"),
						new bts.actions.Timer_Wait(null, (float) 10, null),
						new bts.actions.AI_Trigger(null, "SpawnSeeds", null,
								null, "player"), new bts.actions.Timer_Wait(
								null, (float) 5, null),
						new bts.actions.AI_Trigger(null, "FallSeeds", null,
								null, "player")));

		SeedCycle = new jbt.model.task.composite.ModelSequence(null,
				new jbt.model.task.decorator.ModelUntilFail(null,
						new jbt.model.task.composite.ModelSequence(null,
								new bts.actions.Timer_Wait(null, (float) 5,
										null),
								new jbt.model.task.decorator.ModelInverter(
										null, new bts.actions.AI_Trigger(null,
												"SpawnVegetation", null, null,
												"player")))));

		DiscoverVegetation = new jbt.model.task.composite.ModelSequence(null,
				new bts.actions.Planet_GetPlayer(null, null, "player"),
				new bts.actions.WaitCond_EntityDistanceMax(null, null,
						"player", (float) 0.1, null),
				new bts.actions.Counter_IncrAndGet(null, "vegetationCounter",
						null, "vegetationCount", null),
				new jbt.model.task.composite.ModelSelector(null,
						new jbt.model.task.composite.ModelSequence(null,
								new bts.actions.Integer_AreEqual(null, null,
										"vegetationCount", (int) 1, null),
								new bts.actions.AI_Trigger(null, "FoundTree1",
										null, null, "player")),
						new jbt.model.task.composite.ModelSequence(null,
								new bts.actions.Integer_AreEqual(null, null,
										"vegetationCount", (int) 2, null),
								new bts.actions.AI_Trigger(null, "FoundTree2",
										null, null, "player")),
						new jbt.model.task.composite.ModelSequence(null,
								new bts.actions.Integer_AreEqual(null, null,
										"vegetationCount", (int) 4, null),
								new bts.actions.AI_Trigger(null, "FoundTree3",
										null, null, "player"))));

		AgainVegetation = new jbt.model.task.composite.ModelSequence(null,
				new bts.actions.Planet_GetPlayer(null, null, "player"),
				new bts.actions.WaitCond_EntityDistanceMax(null, null,
						"player", (float) 0.1, null),
				new bts.actions.Counter_IncrAndGet(null, "againCounter", null,
						"againCount", null), new bts.actions.Integer_AreEqual(
						null, null, "againCount", (int) 2, null),
				new bts.actions.AI_Trigger(null, "AgainTree", null, null,
						"player"));

		PlanetZone = new jbt.model.task.decorator.ModelRepeat(null,
				new jbt.model.task.composite.ModelSequence(null,
						new bts.actions.Planet_GetPlayer(null, null, "player"),
						new bts.actions.WaitCond_EntityDistanceMax(null, null,
								"player", (float) 0.2, null),
						new bts.actions.AI_Trigger(null, "PlanetSwitchOn",
								null, null, "player"),
						new bts.actions.WaitCond_EntityDistanceMin(null, null,
								"player", (float) 0.3, null),
						new bts.actions.AI_Trigger(null, "PlanetSwitchOff",
								null, null, "player")));

	}

	/**
	 * Returns a behaviour tree by its name, or null in case it cannot be found.
	 * It must be noted that the trees that are retrieved belong to the class,
	 * not to the instance (that is, the trees are static members of the class),
	 * so they are shared among all the instances of this class.
	 */
	public jbt.model.core.ModelTask getBT(java.lang.String name) {
		if (name.equals("Test")) {
			return Test;
		}
		if (name.equals("Animal")) {
			return Animal;
		}
		if (name.equals("IntroMonologue")) {
			return IntroMonologue;
		}
		if (name.equals("PlanetZoneTuto1")) {
			return PlanetZoneTuto1;
		}
		if (name.equals("DiscoverRocks")) {
			return DiscoverRocks;
		}
		if (name.equals("PlanetZoneTuto2")) {
			return PlanetZoneTuto2;
		}
		if (name.equals("PlanetZoneFirstBack")) {
			return PlanetZoneFirstBack;
		}
		if (name.equals("Tuto2Monologue")) {
			return Tuto2Monologue;
		}
		if (name.equals("DiscoverVegetationTuto")) {
			return DiscoverVegetationTuto;
		}
		if (name.equals("PlanetZoneBigPlanet")) {
			return PlanetZoneBigPlanet;
		}
		if (name.equals("PlanetZoneTuto1Back")) {
			return PlanetZoneTuto1Back;
		}
		if (name.equals("BigPlanetMonologue")) {
			return BigPlanetMonologue;
		}
		if (name.equals("BushZone1")) {
			return BushZone1;
		}
		if (name.equals("BushZone2")) {
			return BushZone2;
		}
		if (name.equals("AnimalsZone1")) {
			return AnimalsZone1;
		}
		if (name.equals("AnimalsZone2")) {
			return AnimalsZone2;
		}
		if (name.equals("VegetationCycle")) {
			return VegetationCycle;
		}
		if (name.equals("SeedCycle")) {
			return SeedCycle;
		}
		if (name.equals("DiscoverVegetation")) {
			return DiscoverVegetation;
		}
		if (name.equals("AgainVegetation")) {
			return AgainVegetation;
		}
		if (name.equals("PlanetZone")) {
			return PlanetZone;
		}
		return null;
	}

	/**
	 * Returns an Iterator that is able to iterate through all the elements in
	 * the library. It must be noted that the iterator does not support the
	 * "remove()" operation. It must be noted that the trees that are retrieved
	 * belong to the class, not to the instance (that is, the trees are static
	 * members of the class), so they are shared among all the instances of this
	 * class.
	 */
	public java.util.Iterator<jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>> iterator() {
		return new BTLibraryIterator();
	}

	private class BTLibraryIterator
			implements
			java.util.Iterator<jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>> {
		static final long numTrees = 21;
		long currentTree = 0;

		public boolean hasNext() {
			return this.currentTree < numTrees;
		}

		public jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask> next() {
			this.currentTree++;

			if ((this.currentTree - 1) == 0) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Test", Test);
			}

			if ((this.currentTree - 1) == 1) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Animal", Animal);
			}

			if ((this.currentTree - 1) == 2) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"IntroMonologue", IntroMonologue);
			}

			if ((this.currentTree - 1) == 3) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"PlanetZoneTuto1", PlanetZoneTuto1);
			}

			if ((this.currentTree - 1) == 4) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"DiscoverRocks", DiscoverRocks);
			}

			if ((this.currentTree - 1) == 5) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"PlanetZoneTuto2", PlanetZoneTuto2);
			}

			if ((this.currentTree - 1) == 6) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"PlanetZoneFirstBack", PlanetZoneFirstBack);
			}

			if ((this.currentTree - 1) == 7) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Tuto2Monologue", Tuto2Monologue);
			}

			if ((this.currentTree - 1) == 8) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"DiscoverVegetationTuto", DiscoverVegetationTuto);
			}

			if ((this.currentTree - 1) == 9) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"PlanetZoneBigPlanet", PlanetZoneBigPlanet);
			}

			if ((this.currentTree - 1) == 10) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"PlanetZoneTuto1Back", PlanetZoneTuto1Back);
			}

			if ((this.currentTree - 1) == 11) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"BigPlanetMonologue", BigPlanetMonologue);
			}

			if ((this.currentTree - 1) == 12) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"BushZone1", BushZone1);
			}

			if ((this.currentTree - 1) == 13) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"BushZone2", BushZone2);
			}

			if ((this.currentTree - 1) == 14) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"AnimalsZone1", AnimalsZone1);
			}

			if ((this.currentTree - 1) == 15) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"AnimalsZone2", AnimalsZone2);
			}

			if ((this.currentTree - 1) == 16) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"VegetationCycle", VegetationCycle);
			}

			if ((this.currentTree - 1) == 17) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"SeedCycle", SeedCycle);
			}

			if ((this.currentTree - 1) == 18) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"DiscoverVegetation", DiscoverVegetation);
			}

			if ((this.currentTree - 1) == 19) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"AgainVegetation", AgainVegetation);
			}

			if ((this.currentTree - 1) == 20) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"PlanetZone", PlanetZone);
			}

			throw new java.util.NoSuchElementException();
		}

		public void remove() {
			throw new java.lang.UnsupportedOperationException();
		}
	}
}
