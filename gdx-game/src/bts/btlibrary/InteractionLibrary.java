// ******************************************************* 
//                   MACHINE GENERATED CODE                
//                       DO NOT MODIFY                     
//                                                         
// Generated on 01/28/2016 10:49:42
// ******************************************************* 
package bts.btlibrary;

/**
 * BT library that includes the trees read from the following files:
 * <ul>
 * <li>./config/interaction/object.xbt</li>
 * <li>./config/interaction/vegetation.xbt</li>
 * <li>./config/interaction/vegetation_tuto.xbt</li>
 * <li>./config/interaction/seed.xbt</li>
 * <li>./config/interaction/rock.xbt</li>
 * <li>./config/interaction/rock_tuto.xbt</li>
 * <li>./config/interaction/planet_switch.xbt</li>
 * <li>./config/interaction/animal.xbt</li>
 * <li>./config/interaction/human.xbt</li>
 * <li>./config/interaction/shelter.xbt</li>
 * <li>./config/interaction/events/spawn_seeds.xbt</li>
 * <li>./config/interaction/events/fall_seeds.xbt</li>
 * <li>./config/interaction/events/spawn_vegetation.xbt</li>
 * <li>./config/interaction/events/planet_switch_on.xbt</li>
 * <li>./config/interaction/events/planet_switch_off.xbt</li>
 * <li>./config/interaction/events/found_tree1.xbt</li>
 * <li>./config/interaction/events/found_tree2.xbt</li>
 * <li>./config/interaction/events/found_tree3.xbt</li>
 * <li>./config/interaction/events/again_tree.xbt</li>
 * <li>./config/interaction/events/introPlanet/intro.xbt</li>
 * <li>./config/interaction/events/introPlanet/intro2.xbt</li>
 * <li>./config/interaction/events/introPlanet/intro3.xbt</li>
 * <li>./config/interaction/events/introPlanet/intro4.xbt</li>
 * <li>./config/interaction/events/introPlanet/tuto1_switch_on.xbt</li>
 * <li>./config/interaction/events/introPlanet/tuto1_switch_off.xbt</li>
 * <li>./config/interaction/events/satelliteTuto1/found_rock_tuto.xbt</li>
 * <li>./config/interaction/events/satelliteTuto1/tuto2_switch_on.xbt</li>
 * <li>./config/interaction/events/satelliteTuto1/tuto2_switch_off.xbt</li>
 * <li>./config/interaction/events/satelliteTuto1/first_switch_back_on.xbt</li>
 * <li>./config/interaction/events/satelliteTuto1/first_switch_back_off.xbt</li>
 * <li>./config/interaction/events/satelliteTuto2/intro_tuto2.xbt</li>
 * <li>./config/interaction/events/satelliteTuto2/found_vegetation_tuto.xbt</li>
 * <li>./config/interaction/events/satelliteTuto2/big_planet_switch_on.xbt</li>
 * <li>./config/interaction/events/satelliteTuto2/big_planet_switch_off.xbt</li>
 * <li>./config/interaction/events/satelliteTuto2/tuto1_switch_back_on.xbt</li>
 * <li>./config/interaction/events/satelliteTuto2/tuto1_switch_back_off.xbt</li>
 * <li>./config/interaction/events/bigPlanet/intro_big_planet.xbt</li>
 * <li>./config/interaction/events/bushPlanet/bush1.xbt</li>
 * <li>./config/interaction/events/bushPlanet/bush2.xbt</li>
 * <li>./config/interaction/events/animalPlanet/animals1.xbt</li>
 * <li>./config/interaction/events/animalPlanet/animals2.xbt</li>
 * </ul>
 */
public class InteractionLibrary implements jbt.execution.core.IBTLibrary {
	/** Tree generated from file ./config/interaction/object.xbt. */
	private static jbt.model.core.ModelTask Object;
	/** Tree generated from file ./config/interaction/vegetation.xbt. */
	private static jbt.model.core.ModelTask Vegetation;
	/** Tree generated from file ./config/interaction/vegetation_tuto.xbt. */
	private static jbt.model.core.ModelTask VegetationTuto;
	/** Tree generated from file ./config/interaction/seed.xbt. */
	private static jbt.model.core.ModelTask Seed;
	/** Tree generated from file ./config/interaction/rock.xbt. */
	private static jbt.model.core.ModelTask Rock;
	/** Tree generated from file ./config/interaction/rock_tuto.xbt. */
	private static jbt.model.core.ModelTask RockTuto;
	/** Tree generated from file ./config/interaction/planet_switch.xbt. */
	private static jbt.model.core.ModelTask PlanetSwitch;
	/** Tree generated from file ./config/interaction/animal.xbt. */
	private static jbt.model.core.ModelTask Animal;
	/** Tree generated from file ./config/interaction/human.xbt. */
	private static jbt.model.core.ModelTask Human;
	/** Tree generated from file ./config/interaction/shelter.xbt. */
	private static jbt.model.core.ModelTask Shelter;
	/** Tree generated from file ./config/interaction/events/spawn_seeds.xbt. */
	private static jbt.model.core.ModelTask SpawnSeeds;
	/** Tree generated from file ./config/interaction/events/fall_seeds.xbt. */
	private static jbt.model.core.ModelTask FallSeeds;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/spawn_vegetation.xbt.
	 */
	private static jbt.model.core.ModelTask SpawnVegetation;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/planet_switch_on.xbt.
	 */
	private static jbt.model.core.ModelTask PlanetSwitchOn;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/planet_switch_off.xbt.
	 */
	private static jbt.model.core.ModelTask PlanetSwitchOff;
	/** Tree generated from file ./config/interaction/events/found_tree1.xbt. */
	private static jbt.model.core.ModelTask FoundTree1;
	/** Tree generated from file ./config/interaction/events/found_tree2.xbt. */
	private static jbt.model.core.ModelTask FoundTree2;
	/** Tree generated from file ./config/interaction/events/found_tree3.xbt. */
	private static jbt.model.core.ModelTask FoundTree3;
	/** Tree generated from file ./config/interaction/events/again_tree.xbt. */
	private static jbt.model.core.ModelTask AgainTree;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/introPlanet/intro.xbt.
	 */
	private static jbt.model.core.ModelTask Intro;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/introPlanet/intro2.xbt.
	 */
	private static jbt.model.core.ModelTask Intro2;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/introPlanet/intro3.xbt.
	 */
	private static jbt.model.core.ModelTask Intro3;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/introPlanet/intro4.xbt.
	 */
	private static jbt.model.core.ModelTask Intro4;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/introPlanet/tuto1_switch_on.xbt.
	 */
	private static jbt.model.core.ModelTask Tuto1SwitchOn;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/introPlanet/tuto1_switch_off.xbt.
	 */
	private static jbt.model.core.ModelTask Tuto1SwitchOff;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/satelliteTuto1/found_rock_tuto.xbt.
	 */
	private static jbt.model.core.ModelTask FoundRockTuto;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/satelliteTuto1/tuto2_switch_on.xbt.
	 */
	private static jbt.model.core.ModelTask Tuto2SwitchOn;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/satelliteTuto1/tuto2_switch_off.xbt.
	 */
	private static jbt.model.core.ModelTask Tuto2SwitchOff;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/satelliteTuto1/first_switch_back_on.xbt.
	 */
	private static jbt.model.core.ModelTask FirstSwitchBackOn;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/satelliteTuto1/first_switch_back_off.xbt.
	 */
	private static jbt.model.core.ModelTask FirstSwitchBackOff;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/satelliteTuto2/intro_tuto2.xbt.
	 */
	private static jbt.model.core.ModelTask IntroTuto2;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/satelliteTuto2/found_vegetation_tuto.xbt.
	 */
	private static jbt.model.core.ModelTask FoundVegetationTuto;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/satelliteTuto2/big_planet_switch_on.xbt.
	 */
	private static jbt.model.core.ModelTask BigPlanetSwitchOn;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/satelliteTuto2/big_planet_switch_off.xbt.
	 */
	private static jbt.model.core.ModelTask BigPlanetSwitchOff;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/satelliteTuto2/tuto1_switch_back_on.xbt.
	 */
	private static jbt.model.core.ModelTask Tuto1SwitchBackOn;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/satelliteTuto2/tuto1_switch_back_off.xbt.
	 */
	private static jbt.model.core.ModelTask Tuto1SwitchBackOff;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/bigPlanet/intro_big_planet.xbt.
	 */
	private static jbt.model.core.ModelTask IntroBigPlanet;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/bushPlanet/bush1.xbt.
	 */
	private static jbt.model.core.ModelTask Bush1;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/bushPlanet/bush2.xbt.
	 */
	private static jbt.model.core.ModelTask Bush2;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/animalPlanet/animals1.xbt.
	 */
	private static jbt.model.core.ModelTask Animals1;
	/**
	 * Tree generated from file
	 * ./config/interaction/events/animalPlanet/animals2.xbt.
	 */
	private static jbt.model.core.ModelTask Animals2;

	/* Static initialization of all the trees. */
	static {
		Object = new jbt.model.task.composite.ModelSequence(null,
				new bts.actions.I_LookClose(null, (float) 0, null),
				new bts.actions.I_Use(null));

		Vegetation = new jbt.model.task.composite.ModelSequence(null,
				new bts.actions.I_LookClose(null, (float) 0.1, null),
				new bts.actions.I_ForbidCancel(null, (boolean) true, null),
				new bts.actions.I_Effect_AssetChanger(null,
						"planet/plant_eaten.png", null, (float) 1000, null),
				new jbt.model.task.leaf.ModelWait(null, 1000),
				new bts.actions.I_Destroy(null));

		VegetationTuto = new jbt.model.task.composite.ModelSequence(null,
				new bts.actions.I_LookClose(null, (float) 0.1, null),
				new bts.actions.I_ForbidCancel(null, (boolean) true, null),
				new bts.actions.I_Effect_AssetChanger(null,
						"planet/plant_eaten.png", null, (float) 1000, null),
				new bts.actions.I_Say(null, (int) 3020, null),
				new jbt.model.task.leaf.ModelWait(null, 1000),
				new bts.actions.I_Destroy(null),
				new jbt.model.task.leaf.ModelWait(null, 4000),
				new bts.actions.I_Say(null, (int) 3021, null),
				new jbt.model.task.leaf.ModelWait(null, 5000),
				new bts.actions.I_Say(null, (int) 3022, null));

		Seed = new jbt.model.task.composite.ModelSelector(null,
				new jbt.model.task.composite.ModelSequence(null,
						new bts.actions.I_IsType(null, "Animal", null),
						new bts.actions.I_LookClose(null, (float) 0.02, null),
						new jbt.model.task.decorator.ModelInverter(null,
								new bts.actions.I_IsTargetHolded(null)),
						new bts.actions.I_Hold(null)),
				new jbt.model.task.composite.ModelSequence(null,
						new bts.actions.I_IsType(null, "Human", null),
						new bts.actions.I_LookClose(null, (float) 0.02, null),
						new bts.actions.I_Destroy(null)));

		Rock = new jbt.model.task.composite.ModelSelector(null,
				new bts.actions.I_IsHolding(null),
				new bts.actions.I_IsTargetHolded(null),
				new jbt.model.task.composite.ModelSequence(null,
						new bts.actions.I_IsType(null, "Player", null),
						new bts.actions.I_LookClose(null, (float) 0.1f, null),
						new bts.actions.I_Hold(null)),
				new jbt.model.task.composite.ModelSequence(null,
						new bts.actions.I_IsType(null, "Human", null),
						new bts.actions.I_LookClose(null, (float) 0.02f, null),
						new bts.actions.I_Use(null)));

		RockTuto = new jbt.model.task.composite.ModelSelector(
				null,
				new bts.actions.I_IsHolding(null),
				new bts.actions.I_IsTargetHolded(null),
				new jbt.model.task.composite.ModelSequence(
						null,
						new bts.actions.I_IsType(null, "Player", null),
						new bts.actions.I_LookClose(null, (float) 0.1f, null),
						new bts.actions.I_Hold(null),
						new bts.actions.Counter_IncrAndGet(null,
								"rockHoldCounter", null, "rockHoldCount", null),
						new jbt.model.task.composite.ModelSelector(null,
								new jbt.model.task.composite.ModelSequence(
										null, new bts.actions.Integer_AreEqual(
												null, null, "rockHoldCount",
												(int) 1, null),
										new bts.actions.I_Say(null, (int) 2022,
												null)),
								new jbt.model.task.composite.ModelSequence(
										null, new bts.actions.Integer_AreEqual(
												null, null, "rockHoldCount",
												(int) 2, null),
										new bts.actions.I_Say(null, (int) 2023,
												null)),
								new jbt.model.task.composite.ModelSequence(
										null, new bts.actions.Integer_AreEqual(
												null, null, "rockHoldCount",
												(int) 3, null),
										new bts.actions.I_Say(null, (int) 2024,
												null)), new bts.actions.I_Say(
										null, (int) 2025, null))),
				new jbt.model.task.composite.ModelSequence(null,
						new bts.actions.I_IsType(null, "Human", null),
						new bts.actions.I_LookClose(null, (float) 0.1f, null),
						new bts.actions.I_Use(null)));

		PlanetSwitch = new jbt.model.task.composite.ModelSequence(null,
				new bts.actions.I_LookClose(null, (float) 0.02, null),
				new jbt.model.task.leaf.ModelWait(null, 200),
				new bts.actions.I_Use(null));

		Animal = new jbt.model.task.composite.ModelSequence(
				null,
				new jbt.model.task.composite.ModelSelector(
						null,
						new jbt.model.task.composite.ModelSequence(null,
								new bts.actions.I_IsType(null, "Player", null),
								new bts.actions.I_LookClose(null, (float) 0.05,
										null), new bts.actions.I_Hold(null)),
						new jbt.model.task.composite.ModelSequence(
								null,
								new bts.actions.I_IsType(null, "Human", null),
								new bts.actions.I_LookClose(null, (float) 0.05,
										null),
								new jbt.model.task.composite.ModelRandomSelector(
										null, new bts.actions.I_Destroy(null),
										new jbt.model.task.leaf.ModelSuccess(
												null)))));

		Human = new jbt.model.task.composite.ModelSequence(null,
				new bts.actions.I_IsType(null, "Animal", null),
				new bts.actions.I_LookClose(null, (float) 0.05, null),
				new jbt.model.task.composite.ModelRandomSelector(null,
						new bts.actions.I_Destroy(null),
						new jbt.model.task.leaf.ModelSuccess(null)));

		Shelter = new jbt.model.task.composite.ModelSelector(null,
				new jbt.model.task.composite.ModelSequence(null,
						new bts.actions.I_IsType(null, "Animal", null),
						new bts.actions.I_IsHolding(null),
						new bts.actions.I_LookClose(null, (float) 0.02, null),
						new bts.actions.I_Use(null)));

		SpawnSeeds = new bts.actions.I_Use(null);

		FallSeeds = new bts.actions.I_Use(null);

		SpawnVegetation = new jbt.model.task.composite.ModelSequence(null,
				new jbt.model.task.composite.ModelSelector(null,
						new jbt.model.task.composite.ModelSequence(null,
								new jbt.model.task.decorator.ModelInverter(
										null, new bts.actions.I_IsTargetHolded(
												null)), new bts.actions.I_Use(
										null)),
						new jbt.model.task.composite.ModelSequence(null,
								new jbt.model.task.decorator.ModelUntilFail(
										null, new bts.actions.I_IsTargetHolded(
												null)),
								new jbt.model.task.leaf.ModelFailure(null))));

		PlanetSwitchOn = new jbt.model.task.composite.ModelParallel(
				null,
				jbt.model.task.composite.ModelParallel.ParallelPolicy.SEQUENCE_POLICY,
				new bts.actions.I_Say(null, (int) 5, null),
				new bts.actions.I_Show(null));

		PlanetSwitchOff = new jbt.model.task.composite.ModelParallel(
				null,
				jbt.model.task.composite.ModelParallel.ParallelPolicy.SEQUENCE_POLICY,
				new bts.actions.I_Say(null, (int) 6, null),
				new bts.actions.I_Hide(null));

		FoundTree1 = new bts.actions.I_Say(null, (int) 1, null);

		FoundTree2 = new bts.actions.I_Say(null, (int) 2, null);

		FoundTree3 = new bts.actions.I_Say(null, (int) 3, null);

		AgainTree = new bts.actions.I_Say(null, (int) 4, null);

		Intro = new jbt.model.task.composite.ModelSequence(null,
				new jbt.model.task.leaf.ModelWait(null, 1000),
				new bts.actions.I_Say(null, (int) 1000, null),
				new jbt.model.task.leaf.ModelWait(null, 5000),
				new bts.actions.I_Say(null, (int) 1001, null),
				new jbt.model.task.leaf.ModelWait(null, 2000),
				new bts.actions.I_Say(null, (int) 1002, null),
				new jbt.model.task.leaf.ModelWait(null, 5000));

		Intro2 = new jbt.model.task.composite.ModelSequence(null,
				new bts.actions.I_Say(null, (int) 1003, null),
				new jbt.model.task.leaf.ModelWait(null, 5000),
				new bts.actions.I_Say(null, (int) 1004, null),
				new jbt.model.task.leaf.ModelWait(null, 5000),
				new bts.actions.I_Say(null, (int) 1005, null),
				new jbt.model.task.leaf.ModelWait(null, 5000));

		Intro3 = new jbt.model.task.composite.ModelSequence(null,
				new bts.actions.I_Say(null, (int) 1006, null),
				new jbt.model.task.leaf.ModelWait(null, 4000));

		Intro4 = new bts.actions.I_Say(null, (int) 1007, null);

		Tuto1SwitchOn = new jbt.model.task.composite.ModelSequence(
				null,
				new bts.actions.Counter_IncrAndGet(null,
						"Tuto1SwitchOnCounter", null, "Tuto1SwitchOnCount",
						null),
				new jbt.model.task.composite.ModelParallel(
						null,
						jbt.model.task.composite.ModelParallel.ParallelPolicy.SEQUENCE_POLICY,
						new bts.actions.I_Show(null),
						new jbt.model.task.composite.ModelSelector(null,
								new jbt.model.task.composite.ModelSequence(
										null, new bts.actions.Integer_AreEqual(
												null, null,
												"Tuto1SwitchOnCount", (int) 1,
												null), new bts.actions.I_Say(
												null, (int) 1010, null),
										new jbt.model.task.leaf.ModelWait(null,
												5000), new bts.actions.I_Say(
												null, (int) 1011, null),
										new jbt.model.task.leaf.ModelWait(null,
												5000), new bts.actions.I_Say(
												null, (int) 1012, null),
										new jbt.model.task.leaf.ModelWait(null,
												5000), new bts.actions.I_Say(
												null, (int) 1013, null)),
								new jbt.model.task.composite.ModelSequence(
										null, new bts.actions.Integer_AreEqual(
												null, null,
												"Tuto1SwitchOnCount", (int) 2,
												null), new bts.actions.I_Say(
												null, (int) 1030, null)),
								new bts.actions.I_Say(null, (int) 1031, null))));

		Tuto1SwitchOff = new jbt.model.task.composite.ModelParallel(
				null,
				jbt.model.task.composite.ModelParallel.ParallelPolicy.SEQUENCE_POLICY,
				new bts.actions.I_Hide(null), new bts.actions.I_Say(null,
						(int) 1020, null), new jbt.model.task.leaf.ModelWait(
						null, 5000), new bts.actions.I_Say(null, (int) 1021,
						null));

		FoundRockTuto = new jbt.model.task.composite.ModelSequence(null,
				new bts.actions.I_Say(null, (int) 2010, null),
				new jbt.model.task.leaf.ModelWait(null, 3000),
				new bts.actions.I_Say(null, (int) 2011, null),
				new jbt.model.task.leaf.ModelWait(null, 5000),
				new bts.actions.I_Say(null, (int) 2012, null),
				new jbt.model.task.leaf.ModelWait(null, 10000),
				new bts.actions.I_Say(null, (int) 2013, null),
				new jbt.model.task.leaf.ModelWait(null, 10000),
				new bts.actions.I_Say(null, (int) 2014, null),
				new jbt.model.task.leaf.ModelWait(null, 10000),
				new bts.actions.I_Say(null, (int) 2015, null));

		Tuto2SwitchOn = new jbt.model.task.composite.ModelSequence(
				null,
				new bts.actions.Counter_IncrAndGet(null,
						"Tuto2SwitchOnCounter", null, "Tuto2SwitchOnCount",
						null),
				new jbt.model.task.composite.ModelParallel(
						null,
						jbt.model.task.composite.ModelParallel.ParallelPolicy.SEQUENCE_POLICY,
						new bts.actions.I_Show(null),
						new jbt.model.task.composite.ModelSelector(null,
								new jbt.model.task.composite.ModelSequence(
										null, new bts.actions.Integer_AreEqual(
												null, null,
												"Tuto2SwitchOnCount", (int) 1,
												null), new bts.actions.I_Say(
												null, (int) 2030, null)),
								new bts.actions.I_Say(null, (int) 2050, null))));

		Tuto2SwitchOff = new jbt.model.task.composite.ModelParallel(
				null,
				jbt.model.task.composite.ModelParallel.ParallelPolicy.SEQUENCE_POLICY,
				new bts.actions.I_Hide(null), new bts.actions.I_Say(null,
						(int) 2040, null));

		FirstSwitchBackOn = new jbt.model.task.composite.ModelSequence(
				null,
				new bts.actions.Counter_IncrAndGet(null,
						"FirstSwitchBackOnCounter", null,
						"FirstSwitchBackOnCount", null),
				new jbt.model.task.composite.ModelParallel(
						null,
						jbt.model.task.composite.ModelParallel.ParallelPolicy.SEQUENCE_POLICY,
						new bts.actions.I_Show(null),
						new jbt.model.task.composite.ModelSelector(null,
								new jbt.model.task.composite.ModelSequence(
										null, new bts.actions.Integer_AreEqual(
												null, null,
												"FirstSwitchBackOnCount",
												(int) 1, null),
										new bts.actions.I_Say(null, (int) 2060,
												null)), new bts.actions.I_Say(
										null, (int) 2070, null))));

		FirstSwitchBackOff = new jbt.model.task.composite.ModelSequence(
				null,
				new bts.actions.Counter_IncrAndGet(null,
						"FirstSwitchBackOffCounter", null,
						"FirstSwitchBackOffCount", null),
				new jbt.model.task.composite.ModelParallel(
						null,
						jbt.model.task.composite.ModelParallel.ParallelPolicy.SEQUENCE_POLICY,
						new bts.actions.I_Hide(null),
						new jbt.model.task.composite.ModelSelector(null,
								new bts.actions.Integer_AreEqual(null, null,
										"FirstSwitchBackOffCount", (int) 1,
										null), new bts.actions.I_Say(null,
										(int) 2040, null))));

		IntroTuto2 = new jbt.model.task.composite.ModelSequence(null,
				new bts.actions.I_Say(null, (int) 3000, null),
				new jbt.model.task.leaf.ModelWait(null, 4000),
				new bts.actions.I_Say(null, (int) 3001, null));

		FoundVegetationTuto = new bts.actions.I_Say(null, (int) 3010, null);

		BigPlanetSwitchOn = new jbt.model.task.composite.ModelSequence(
				null,
				new bts.actions.Counter_IncrAndGet(null,
						"BigPlanetSwitchOnCounter", null,
						"BigPlanetSwitchOnCount", null),
				new jbt.model.task.composite.ModelParallel(
						null,
						jbt.model.task.composite.ModelParallel.ParallelPolicy.SEQUENCE_POLICY,
						new bts.actions.I_Show(null),
						new jbt.model.task.composite.ModelSelector(null,
								new jbt.model.task.composite.ModelSequence(
										null, new bts.actions.Integer_AreEqual(
												null, null,
												"BigPlanetSwitchOnCount",
												(int) 1, null),
										new bts.actions.I_Say(null, (int) 3030,
												null)), new bts.actions.I_Say(
										null, (int) 3032, null))));

		BigPlanetSwitchOff = new jbt.model.task.composite.ModelSequence(
				null,
				new bts.actions.Counter_IncrAndGet(null,
						"BigPlanetSwitchOffCounter", null,
						"BigPlanetSwitchOffCount", null),
				new jbt.model.task.composite.ModelParallel(
						null,
						jbt.model.task.composite.ModelParallel.ParallelPolicy.SEQUENCE_POLICY,
						new bts.actions.I_Hide(null),
						new jbt.model.task.composite.ModelSelector(null,
								new jbt.model.task.composite.ModelSequence(
										null, new bts.actions.Integer_AreEqual(
												null, null,
												"BigPlanetSwitchOffCount",
												(int) 1, null),
										new bts.actions.I_Say(null, (int) 3031,
												null)), new bts.actions.I_Say(
										null, (int) 3033, null))));

		Tuto1SwitchBackOn = new jbt.model.task.composite.ModelSequence(
				null,
				new bts.actions.Counter_IncrAndGet(null,
						"Tuto1SwitchBackOnCounter", null,
						"Tuto1SwitchBackOnCount", null),
				new jbt.model.task.composite.ModelParallel(
						null,
						jbt.model.task.composite.ModelParallel.ParallelPolicy.SEQUENCE_POLICY,
						new bts.actions.I_Show(null),
						new jbt.model.task.composite.ModelSelector(
								null,
								new bts.actions.Integer_AreEqual(null, null,
										"Tuto1SwitchBackOnCount", (int) 1, null),
								new bts.actions.I_Say(null, (int) 3050, null))));

		Tuto1SwitchBackOff = new bts.actions.I_Hide(null);

		IntroBigPlanet = new jbt.model.task.composite.ModelSequence(null,
				new bts.actions.I_Say(null, (int) 4000, null),
				new jbt.model.task.leaf.ModelWait(null, 4000),
				new bts.actions.I_Say(null, (int) 4001, null));

		Bush1 = new jbt.model.task.composite.ModelSequence(null,
				new bts.actions.I_Say(null, (int) 5000, null),
				new jbt.model.task.leaf.ModelWait(null, 10000));

		Bush2 = new jbt.model.task.composite.ModelSequence(null,
				new bts.actions.I_Say(null, (int) 5001, null),
				new jbt.model.task.leaf.ModelWait(null, 10000));

		Animals1 = new jbt.model.task.composite.ModelSequence(null,
				new bts.actions.I_Say(null, (int) 6000, null),
				new jbt.model.task.leaf.ModelWait(null, 10000));

		Animals2 = new jbt.model.task.composite.ModelSequence(null,
				new bts.actions.I_Say(null, (int) 6001, null),
				new jbt.model.task.leaf.ModelWait(null, 10000));

	}

	/**
	 * Returns a behaviour tree by its name, or null in case it cannot be found.
	 * It must be noted that the trees that are retrieved belong to the class,
	 * not to the instance (that is, the trees are static members of the class),
	 * so they are shared among all the instances of this class.
	 */
	public jbt.model.core.ModelTask getBT(java.lang.String name) {
		if (name.equals("Object")) {
			return Object;
		}
		if (name.equals("Vegetation")) {
			return Vegetation;
		}
		if (name.equals("VegetationTuto")) {
			return VegetationTuto;
		}
		if (name.equals("Seed")) {
			return Seed;
		}
		if (name.equals("Rock")) {
			return Rock;
		}
		if (name.equals("RockTuto")) {
			return RockTuto;
		}
		if (name.equals("PlanetSwitch")) {
			return PlanetSwitch;
		}
		if (name.equals("Animal")) {
			return Animal;
		}
		if (name.equals("Human")) {
			return Human;
		}
		if (name.equals("Shelter")) {
			return Shelter;
		}
		if (name.equals("SpawnSeeds")) {
			return SpawnSeeds;
		}
		if (name.equals("FallSeeds")) {
			return FallSeeds;
		}
		if (name.equals("SpawnVegetation")) {
			return SpawnVegetation;
		}
		if (name.equals("PlanetSwitchOn")) {
			return PlanetSwitchOn;
		}
		if (name.equals("PlanetSwitchOff")) {
			return PlanetSwitchOff;
		}
		if (name.equals("FoundTree1")) {
			return FoundTree1;
		}
		if (name.equals("FoundTree2")) {
			return FoundTree2;
		}
		if (name.equals("FoundTree3")) {
			return FoundTree3;
		}
		if (name.equals("AgainTree")) {
			return AgainTree;
		}
		if (name.equals("Intro")) {
			return Intro;
		}
		if (name.equals("Intro2")) {
			return Intro2;
		}
		if (name.equals("Intro3")) {
			return Intro3;
		}
		if (name.equals("Intro4")) {
			return Intro4;
		}
		if (name.equals("Tuto1SwitchOn")) {
			return Tuto1SwitchOn;
		}
		if (name.equals("Tuto1SwitchOff")) {
			return Tuto1SwitchOff;
		}
		if (name.equals("FoundRockTuto")) {
			return FoundRockTuto;
		}
		if (name.equals("Tuto2SwitchOn")) {
			return Tuto2SwitchOn;
		}
		if (name.equals("Tuto2SwitchOff")) {
			return Tuto2SwitchOff;
		}
		if (name.equals("FirstSwitchBackOn")) {
			return FirstSwitchBackOn;
		}
		if (name.equals("FirstSwitchBackOff")) {
			return FirstSwitchBackOff;
		}
		if (name.equals("IntroTuto2")) {
			return IntroTuto2;
		}
		if (name.equals("FoundVegetationTuto")) {
			return FoundVegetationTuto;
		}
		if (name.equals("BigPlanetSwitchOn")) {
			return BigPlanetSwitchOn;
		}
		if (name.equals("BigPlanetSwitchOff")) {
			return BigPlanetSwitchOff;
		}
		if (name.equals("Tuto1SwitchBackOn")) {
			return Tuto1SwitchBackOn;
		}
		if (name.equals("Tuto1SwitchBackOff")) {
			return Tuto1SwitchBackOff;
		}
		if (name.equals("IntroBigPlanet")) {
			return IntroBigPlanet;
		}
		if (name.equals("Bush1")) {
			return Bush1;
		}
		if (name.equals("Bush2")) {
			return Bush2;
		}
		if (name.equals("Animals1")) {
			return Animals1;
		}
		if (name.equals("Animals2")) {
			return Animals2;
		}
		return null;
	}

	/**
	 * Returns an Iterator that is able to iterate through all the elements in
	 * the library. It must be noted that the iterator does not support the
	 * "remove()" operation. It must be noted that the trees that are retrieved
	 * belong to the class, not to the instance (that is, the trees are static
	 * members of the class), so they are shared among all the instances of this
	 * class.
	 */
	public java.util.Iterator<jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>> iterator() {
		return new BTLibraryIterator();
	}

	private class BTLibraryIterator
			implements
			java.util.Iterator<jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>> {
		static final long numTrees = 41;
		long currentTree = 0;

		public boolean hasNext() {
			return this.currentTree < numTrees;
		}

		public jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask> next() {
			this.currentTree++;

			if ((this.currentTree - 1) == 0) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Object", Object);
			}

			if ((this.currentTree - 1) == 1) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Vegetation", Vegetation);
			}

			if ((this.currentTree - 1) == 2) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"VegetationTuto", VegetationTuto);
			}

			if ((this.currentTree - 1) == 3) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Seed", Seed);
			}

			if ((this.currentTree - 1) == 4) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Rock", Rock);
			}

			if ((this.currentTree - 1) == 5) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"RockTuto", RockTuto);
			}

			if ((this.currentTree - 1) == 6) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"PlanetSwitch", PlanetSwitch);
			}

			if ((this.currentTree - 1) == 7) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Animal", Animal);
			}

			if ((this.currentTree - 1) == 8) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Human", Human);
			}

			if ((this.currentTree - 1) == 9) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Shelter", Shelter);
			}

			if ((this.currentTree - 1) == 10) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"SpawnSeeds", SpawnSeeds);
			}

			if ((this.currentTree - 1) == 11) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"FallSeeds", FallSeeds);
			}

			if ((this.currentTree - 1) == 12) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"SpawnVegetation", SpawnVegetation);
			}

			if ((this.currentTree - 1) == 13) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"PlanetSwitchOn", PlanetSwitchOn);
			}

			if ((this.currentTree - 1) == 14) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"PlanetSwitchOff", PlanetSwitchOff);
			}

			if ((this.currentTree - 1) == 15) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"FoundTree1", FoundTree1);
			}

			if ((this.currentTree - 1) == 16) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"FoundTree2", FoundTree2);
			}

			if ((this.currentTree - 1) == 17) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"FoundTree3", FoundTree3);
			}

			if ((this.currentTree - 1) == 18) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"AgainTree", AgainTree);
			}

			if ((this.currentTree - 1) == 19) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Intro", Intro);
			}

			if ((this.currentTree - 1) == 20) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Intro2", Intro2);
			}

			if ((this.currentTree - 1) == 21) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Intro3", Intro3);
			}

			if ((this.currentTree - 1) == 22) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Intro4", Intro4);
			}

			if ((this.currentTree - 1) == 23) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Tuto1SwitchOn", Tuto1SwitchOn);
			}

			if ((this.currentTree - 1) == 24) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Tuto1SwitchOff", Tuto1SwitchOff);
			}

			if ((this.currentTree - 1) == 25) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"FoundRockTuto", FoundRockTuto);
			}

			if ((this.currentTree - 1) == 26) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Tuto2SwitchOn", Tuto2SwitchOn);
			}

			if ((this.currentTree - 1) == 27) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Tuto2SwitchOff", Tuto2SwitchOff);
			}

			if ((this.currentTree - 1) == 28) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"FirstSwitchBackOn", FirstSwitchBackOn);
			}

			if ((this.currentTree - 1) == 29) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"FirstSwitchBackOff", FirstSwitchBackOff);
			}

			if ((this.currentTree - 1) == 30) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"IntroTuto2", IntroTuto2);
			}

			if ((this.currentTree - 1) == 31) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"FoundVegetationTuto", FoundVegetationTuto);
			}

			if ((this.currentTree - 1) == 32) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"BigPlanetSwitchOn", BigPlanetSwitchOn);
			}

			if ((this.currentTree - 1) == 33) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"BigPlanetSwitchOff", BigPlanetSwitchOff);
			}

			if ((this.currentTree - 1) == 34) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Tuto1SwitchBackOn", Tuto1SwitchBackOn);
			}

			if ((this.currentTree - 1) == 35) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Tuto1SwitchBackOff", Tuto1SwitchBackOff);
			}

			if ((this.currentTree - 1) == 36) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"IntroBigPlanet", IntroBigPlanet);
			}

			if ((this.currentTree - 1) == 37) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Bush1", Bush1);
			}

			if ((this.currentTree - 1) == 38) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Bush2", Bush2);
			}

			if ((this.currentTree - 1) == 39) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Animals1", Animals1);
			}

			if ((this.currentTree - 1) == 40) {
				return new jbt.util.Pair<java.lang.String, jbt.model.core.ModelTask>(
						"Animals2", Animals2);
			}

			throw new java.util.NoSuchElementException();
		}

		public void remove() {
			throw new java.lang.UnsupportedOperationException();
		}
	}
}
