package com.chernobyl.lifeinspace;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import com.chernobyl.common.inputs.InputListener;
import com.chernobyl.common.inputs.InputManager;
import com.chernobyl.common.inputs.TouchGameRecord;
import com.chernobyl.common.inputs.TouchState;

import com.chernobyl.common.utils.Exceptions;
import com.chernobyl.common.utils.JBTHelpers.TriggerType;
import com.chernobyl.common.utils.JBTHelpers;
import com.chernobyl.common.utils.RenderHelpers;
import com.chernobyl.common.utils.Screenshot;
import com.chernobyl.common.utils.ShaderBank;
import com.chernobyl.common.utils.TextRenderer;

import com.chernobyl.common.render.Billboard;
import com.chernobyl.common.render.RenderOptions;
import com.chernobyl.common.render.manekin.Biped;
import com.chernobyl.common.render.manekin.eAppearance;
import com.chernobyl.common.render.manekin.eEmotion;
import com.chernobyl.common.render.manekin.eSimpleAppearance;


public class GdxGame implements ApplicationListener
{
	final static boolean playScript = false;
	final static boolean allowScreenCapture = false;
	
	boolean doCapture = false;
	
	boolean showFPS = false;
	
	boolean isDebug;

	SpriteBatch sprites;
	ShapeRenderer shapes;

	OrthographicCamera camera;
	Rectangle screen;
	int screenRealWidth, screenRealHeight;

	OrthographicCamera cameraUI;
	
	InputManager inputManager;

	boolean useTarget;
	TouchState touchState;
	Vector2 target;
	float currDeltaTime;

	Matrix4 renderMat;

	Player player;
	
	ArrayList<Planet> planets;
	
	Texture cursorTex;
	float cursorOffsetX;
	float cursorOffsetY;
	
	HashSet<Entity> haloEntities;
	HashSet<Class<? extends Entity>> interacted;
	
	public GdxGame(boolean isDebug)
	{
		this.isDebug = isDebug;
		
		if (isDebug)
		{
			showFPS = true;
		}
	}
	
	@Override
	public void create()
	{
		// render
		
		screenRealWidth = Gdx.graphics.getWidth();
		screenRealHeight = Gdx.graphics.getHeight();
		
		camera = RenderHelpers.GetCamera();
		
		screen = new Rectangle();
		screen.x = screen.y = 0.f;
		screen.width = camera.viewportWidth;
		screen.height = camera.viewportHeight;
		
		sprites = new SpriteBatch();
		shapes = new ShapeRenderer();
		
		renderMat = new Matrix4();
		
		TextRenderer.init(screenRealWidth,
			screenRealHeight);
		
		// inputs
		
		Gdx.input.setCursorCatched(true);
		
		useTarget = false;
		target = new Vector2();

		inputManager = new InputManager();
		inputManager.AddListener(new InputListener()
			{
				@Override
				public void OnInput(TouchState state, int x, int y)
				{
					//Gdx.app.log("Input", state.ToString() + " x=" + x + " y=" + y);

					if (state == TouchState.PRESS)
					{
						useTarget = true;
					}

					touchState = state;
					updateTargetPos(x, y);
				}
				
				@Override
				public void OnKeyDown(int keycode)
				{
					if ((keycode == Keys.ESCAPE) || (keycode == Keys.BACK))
					{
						Gdx.app.exit();
					}
					else if (keycode == Keys.F3)
					{
						if (allowScreenCapture)
						{
							doCapture = true;
						}
					}
					else if (keycode == Keys.F4)
					{
						if (allowScreenCapture)
						{
							doCapture = false;
						}
					}
				}

				@Override
				public void OnTick(float deltaTime)
				{
					currDeltaTime = deltaTime;

					// Force mouse cursor to be always considered on border, even when moved far away and back
					if (!IsTouchType())
					{
						float cursorX = (float)Gdx.input.getX() + cursorOffsetX;
						if (cursorX > (float)screenRealWidth)
						{
							cursorOffsetX -= cursorX - (float)screenRealWidth;
						}
						else if (cursorX < 0.f)
						{
							cursorOffsetX -= cursorX;
						}

						float cursorY = (float)Gdx.input.getY() + cursorOffsetY;
						if (cursorY > (float)screenRealHeight)
						{
							cursorOffsetY -= cursorY - (float)screenRealHeight;
						}
						else if (cursorY < 0.f)
						{
							cursorOffsetY -= cursorY;
						}
					}
				}

				private void updateTargetPos(int x, int y)
				{
					target.x = x + cursorOffsetX;

					target.x *= screen.width
						/ (float)screenRealWidth;

					target.y = screenRealHeight - y - cursorOffsetY;
					target.y *= screen.height
						/ (float)screenRealHeight;
				}
			});
		
		haloEntities = new HashSet<Entity>();
		interacted = new HashSet<Class<? extends Entity>>(); 

		planets = new ArrayList<Planet>();
		
		//OldInitScene();
		InitScene();
	}
	
	void InitScene()
	{		
		Planet introPlanet = CreateIntroPlanet();
		planets.add(introPlanet);
		
		Planet satelliteTuto1 = CreateSatelliteTuto1();
		planets.add(satelliteTuto1);

		Planet satelliteTuto2 = CreateSatelliteTuto2();
		planets.add(satelliteTuto2);

		Planet bigPlanet = CreateBigPlanet();
		planets.add(bigPlanet);

		Planet satelliteWithAnimals = CreateSatelliteWithAnimals();
		planets.add(satelliteWithAnimals);

		Planet bigSatelliteWithBushes = CreateBigSatelliteWithBushes();
		planets.add(bigSatelliteWithBushes);

		// test only
		/*introPlanet.AddPlanetSwitch(20.f, bigPlanet, TriggerType.PLANET_ZONE_TUTO1);
		introPlanet.AddPlanetSwitch(30.f, satelliteWithAnimals, TriggerType.PLANET_ZONE_TUTO1);
		introPlanet.AddPlanetSwitch(40.f, bigSatelliteWithBushes, TriggerType.PLANET_ZONE_TUTO1);
		bigPlanet.AddPlanetSwitch(30.f, satelliteWithAnimals, TriggerType.PLANET_ZONE_TUTO1);
		bigPlanet.AddPlanetSwitch(40.f, bigSatelliteWithBushes, TriggerType.PLANET_ZONE_TUTO1);
		satelliteWithAnimals.AddPlanetSwitch(30.f, bigPlanet, TriggerType.PLANET_ZONE_TUTO1);
		satelliteWithAnimals.AddPlanetSwitch(40.f, bigSatelliteWithBushes, TriggerType.PLANET_ZONE_TUTO1);
		bigSatelliteWithBushes.AddPlanetSwitch(30.f, bigPlanet, TriggerType.PLANET_ZONE_TUTO1);
		bigSatelliteWithBushes.AddPlanetSwitch(40.f, satelliteWithAnimals, TriggerType.PLANET_ZONE_TUTO1);*/
		////////
		
		introPlanet.AddPlanetSwitch(170.f, satelliteTuto1, TriggerType.PLANET_ZONE_TUTO1);
		
		satelliteTuto1.AddPlanetSwitch(-120.f, satelliteTuto2, TriggerType.PLANET_ZONE_TUTO2);
		satelliteTuto1.AddPlanetSwitch(0.f, introPlanet, TriggerType.PLANET_ZONE_FIRST_BACK);
		
		satelliteTuto2.AddPlanetSwitch(170.f, bigPlanet, TriggerType.PLANET_ZONE_BIG_PLANET);
		satelliteTuto2.AddPlanetSwitch(0.f, satelliteTuto1, TriggerType.PLANET_ZONE_TUTO1_BACK);
		
		//bigPlanet.AddPlanetSwitch(0.f, satelliteTuto2);	
		bigPlanet.AddPlanetSwitch(100.f, bigSatelliteWithBushes);
		bigPlanet.AddPlanetSwitch(-170.f, satelliteWithAnimals);
		
		satelliteWithAnimals.AddPlanetSwitch(0.f, bigPlanet);
		bigSatelliteWithBushes.AddPlanetSwitch(0.f, bigPlanet);
		
		player = new Player(eAppearance.DEFAULT);
		player.renderOrder = 20;
		player.isPlayer = true;
		player.EnterPlanet(introPlanet, 0.f);
	}
	
	Planet CreateIntroPlanet()
	{
		Planet planet = new Planet(2.f, "planet/planet05.png", "planet/background01.png", "planet/switch0501.png");
		planet.center.set(0.5f * screen.width, -1.5f * screen.height);

		//// Temp for test
		/*planet.AddRocket(-40.f, 0.2f);

		planet.AddModule(10.f, false, true);
		planet.AddModule(20.f, true, true);

		planet.AddModule(-5.f, false, false);
		planet.AddModule(-10.f, true, false);*/
		////////
		
		planet.AddTrigger(0.f, TriggerType.INTRO_MONOLOGUE);
		
		return planet;
	}
	
	Planet CreateSatelliteTuto1()
	{
		Planet planet = new Planet(0.75f, "planet/planet02.png", "planet/background02.png", "planet/switch02.png");
		planet.center.set(0.5f * screen.width, -0.25f * screen.height);
		
		planet.AddRockTuto(-70);
		planet.AddRockTuto(80);		
		planet.AddRockTuto(100);
		
		planet.AddRockTuto(160);
		planet.AddRockTuto(-140);
		
		return planet;
	}
	
	Planet CreateSatelliteTuto2()
	{
		Planet planet = new Planet(0.75f, "planet/planet03.png", "planet/background03.png", "planet/switch03.png");
		planet.center.set(0.5f * screen.width, -0.25f * screen.height);

		planet.AddTrigger(0.f, TriggerType.TUTO2_MONOLOGUE);
		
		planet.AddVegetationTuto(160.f);
		
		return planet;
	}
	
	Planet CreateBigPlanet()
	{
		Planet planet = new Planet(2.f, "planet/planet01.png", "planet/background01.png", "planet/switch01.png");
		planet.center.set(0.5f * screen.width, -1.5f * screen.height);

		planet.AddTrigger(0.f, TriggerType.BIG_PLANET_MONOLOGUE);

		planet.AddVegetation(20.f);
		planet.AddVegetation(-10.f);
		planet.AddVegetation(-60.f);

		planet.AddVegetation(50.f);
		planet.AddVegetation(60.f);
		planet.AddVegetation(65.f);

		planet.AddVegetation(110.f);
		planet.AddVegetation(120.f);

		planet.AddVegetation(-150.f);

		planet.AddRock(-80.f);
		planet.AddRock(-100.f);
		planet.AddRock(-110.f);

		planet.AddRock(180.f);

		planet.AddShelter(170.f);
		planet.AddShelter(-50.f);
		
		return planet;
	}
	
	Planet CreateSatelliteWithAnimals()
	{
		Planet planet = new Planet(0.75f, "planet/planet04.png", "planet/background04.png", "planet/switch04.png");
		planet.center.set(0.5f * screen.width, -0.25f * screen.height);
		
		planet.AddVegetation(30.f);
		planet.AddVegetation(50.f);
		planet.AddVegetation(-110.f);

		planet.AddTrigger(125.f, TriggerType.ANIMAL_PLANET_MONOLOGUE1);
		planet.AddTrigger(-155.f, TriggerType.ANIMAL_PLANET_MONOLOGUE2);

		planet.AddRock(135.f);
		planet.AddRock(-165.f);
		
		planet.AddAnimal(145.f);
		planet.AddAnimal(148.f);
		planet.AddAnimal(149.f);
		planet.AddAnimal(165.f);
		planet.AddAnimal(167.f);
		planet.AddAnimal(172.f);
		planet.AddAnimal(178.f);
		planet.AddAnimal(-178.f);
		planet.AddAnimal(-175.f);

		planet.AddShelter(-35.f);
				
		return planet;
	}
	
	Planet CreateBigSatelliteWithBushes()
	{
		Planet planet = new Planet(1.f, "planet/planet05.png", "planet/background05.png", "planet/switch05.png");
		planet.center.set(screen.width * 0.5f, -screen.height * 0.5f);

		planet.AddTrigger(55.f, TriggerType.BUSH_PLANET_MONOLOGUE1);
		planet.AddTrigger(-110.f, TriggerType.BUSH_PLANET_MONOLOGUE2);
		
		planet.AddBush(62.f);
		planet.AddBush(69.f);
		planet.AddBush(82.f);

		planet.AddBush(63.f, -0.08f);
		planet.AddBush(71.f, -0.08f);
		planet.AddBush(81.f, -0.08f);

		planet.AddBush(62.f, -0.18f);
		planet.AddBush(72.f, -0.18f);
		planet.AddBush(80.f, -0.18f);

		planet.AddBush(61.f, -0.28f);
		planet.AddBush(76.5f, -0.28f);
		planet.AddBush(81.f, -0.28f);

		planet.AddBush(62.f, -0.4f);
		planet.AddBush(75.f, -0.4f);
		planet.AddBush(82.f, -0.4f);

		planet.AddBush(63.f, -0.5f);
		planet.AddBush(72.f, -0.5f);
		planet.AddBush(83.f, -0.5f);

		planet.AddBush(-120.f);
		planet.AddBush(-132.f);
		planet.AddBush(-145.f);

		planet.AddBush(-121.f, -0.08f);
		planet.AddBush(-130.f, -0.08f);
		planet.AddBush(-142.f, -0.08f);

		planet.AddBush(-118.f, -0.18f);
		planet.AddBush(-129.f, -0.18f);
		planet.AddBush(-140.f, -0.18f);

		planet.AddBush(-124.f, -0.28f);
		planet.AddBush(-131.5f, -0.28f);
		planet.AddBush(-139.f, -0.28f);

		planet.AddBush(-122.f, -0.4f);
		planet.AddBush(-130.f, -0.4f);
		planet.AddBush(-144.f, -0.4f);

		planet.AddBush(-120.f, -0.5f);
		planet.AddBush(-129.f, -0.5f);
		planet.AddBush(-143.f, -0.5f);

		planet.AddVegetation(180.f);
		planet.AddVegetation(170.f);
		planet.AddVegetation(160.f);
		planet.AddVegetation(-170.f);
		planet.AddVegetation(-160.f);

		planet.ApplyToEntities(new Planet.EntityFunction()
		{
			@Override
			public void apply(Entity entity)
			{
				entity.EnableAI(TriggerType.DISCOVER_BUSHES);
			}
		});
				
		planet.AddTrigger(150, TriggerType.BUSHES_SECRET);
		
		return planet;
	}
	
	private boolean IsTouchType()
	{
		switch (Gdx.app.getType())
		{
			case Android:
			case iOS:
				return true;
				
			default:
				return false;
		}
	}
	
	@Override
	public void render()
	{    
		if (!IsTouchType()
			&& cursorTex == null)
		{
			cursorTex = new Texture(Gdx.files.internal("graphics/cursor.png"),
				Format.RGBA8888, RenderOptions.useMipmap);
			cursorTex.setFilter(
				RenderOptions.mipmapMethodMin, RenderOptions.mipmapMethodMag);
		}
		
		TextRenderer.get().preloadFontIfNeeded();
	
		float deltaTimeGdx = doCapture ? 0.02f : Gdx.graphics.getDeltaTime();
		if (deltaTimeGdx > 0.1f)
			deltaTimeGdx = 0.1f;

		if (!inputManager.Update(deltaTimeGdx))
		{
			if (inputManager.IsReplaying())
			{
				inputManager.StopReplay();
			}

			currDeltaTime = deltaTimeGdx;
		}

		RenderHelpers.UpdateCamera(deltaTimeGdx);
		
		TextRenderer.update();
		
		JBTHelpers.UpdateTimer(deltaTimeGdx);
		
		if (playScript)
		{
			UpdateScript(deltaTimeGdx);
		}
		
		if (interTitleText != null)
		{
			GL20 gl = Gdx.graphics.getGL20();
			gl.glClearColor(
				interTitleColorBg.r, interTitleColorBg.g,
				interTitleColorBg.b, interTitleColorBg.a);
			gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

			sprites.setProjectionMatrix(camera.combined);
			
			sprites.begin();
			sprites.setTransformMatrix(renderMat);
			
			TextRenderer.get().setColor(interTitleColorFg);
			TextRenderer.get().setScale(10.f);
			TextRenderer.get().drawText(
				sprites, interTitleText,
				0.5f * screen.width,
				0.5f * screen.height);
		
			sprites.end();
		}
		else
		{
			GL20 gl = Gdx.graphics.getGL20();
			gl.glClearColor(0, 0, 0, 1);
			gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
			
			UpdateGame(deltaTimeGdx);
			
			if (showFPS)
			{
				int fps = Gdx.graphics.getFramesPerSecond();
				
				OrthographicCamera cameraUI =
					RenderHelpers.GetCameraUI();
				sprites.setProjectionMatrix(cameraUI.combined);
				
				sprites.begin();
				sprites.setTransformMatrix(renderMat);
				
				TextRenderer.get().setScale(4.f);
				TextRenderer.get().drawText(
					sprites, String.format("%d fps", fps),
					0.f, screen.height,
					TextRenderer.HAnchor.LEFT,
					TextRenderer.VAnchor.TOP);
					 
				sprites.end();
			}
		}
		
		if (doCapture)
		{
			Screenshot.saveScreenshot();
		}
	}
	
	void UpdateGame(float deltatime)
	{
		// drawing

		sprites.setProjectionMatrix(camera.combined);
		if (isDebug)
		{
			shapes.setProjectionMatrix(camera.combined);
		}

		// draw scene

		renderMat.idt();

		Planet curPlanet = player.GetPlanet();

		curPlanet.Draw(renderMat, sprites);
		if (isDebug)
		{
			curPlanet.DebugDraw(renderMat, shapes);
		}

		if (cursorTex != null &&
			!player.disableInputs)
		{
			// draw cursor

			sprites.begin();
			sprites.setTransformMatrix(renderMat);

			final float cursorWidth = 0.05f;
			final float cursorHeight = 0.05f;
			final float hotSpotX = 0.f;
			final float hotSpotY = 1.f;

			float cursorX = Gdx.input.getX() + cursorOffsetX;
			cursorX *= screen.width / (float)screenRealWidth;
			cursorX = MathUtils.clamp(cursorX, 0.f, screen.width - cursorWidth * 0.2f);

			float cursorY = screenRealHeight - (float)Gdx.input.getY() - cursorOffsetY;
			cursorY *= screen.height / (float)screenRealHeight;
			cursorY = MathUtils.clamp(cursorY, cursorHeight * 0.2f, screen.height);

			sprites.draw(cursorTex, 
						 cursorX - hotSpotX * cursorWidth, cursorY - hotSpotY * cursorHeight, 
						 cursorWidth, cursorHeight);

			sprites.end();
		}

		// process inputs
		if (useTarget &&
			!player.disableInputs)
		{
			if (touchState == TouchState.RELEASE)
			{
				useTarget = false;
			}

			Entity targetEnt = curPlanet.Intersect(target);
			if (targetEnt == player)
			{
				player.Move(0.f);
			}
			else if (player.IsHolding() &&
					 targetEnt == player.holdedEntity)
			{
				if (touchState == TouchState.PRESS)
				{
					player.StopHolding();
					useTarget = false;
				}
			}
			else if (targetEnt != null &&
					 player.Interact(targetEnt, touchState))
			{
				useTarget = false;
				player.Move(0.f);

				interacted.add(targetEnt.getClass());
			}
			else
			{
				// cancel on-going interaction
				player.StopInteraction();

				// just move
				float targetAngle = curPlanet.ConvertToAngle(target);
				player.MoveTo(targetAngle);
			}
		}
		else if (!player.IsInteracting()
			&& !player.disableInputs)
		{
			player.Move(0.f);
		}

		boolean bPlayerMoving = player.IsMoving();
		UpdateCameraFollowing(curPlanet, bPlayerMoving);

		curPlanet.Update(deltatime);

		UpdateHalo(curPlanet);
	}
	
	void UpdateCameraFollowing(Planet planet, boolean bPlayerMoving)
	{
		if (player.disableCameraFollow)
			return;
		
		// rotate planet to follow player
		if (bPlayerMoving)
		{			
			float diffAngle = planet.DiffAngle(player);

			if (diffAngle > 0.15 * Math.PI / planet.radius
				&& player.angleSpeed > 0.f)
			{
				planet.rotate(-player.angleSpeed);
			}
			else if (diffAngle < -0.15 * Math.PI / planet.radius
					 && player.angleSpeed < 0.f)
			{
				planet.rotate(-player.angleSpeed);
			}
			else
			{
				planet.rotate(0.f);
			}			
		}
		else
		{
			planet.rotate(0.f);
		}
	}
	
	public void UpdateHalo(Planet planet)
	{		
		if (player.disableHalo)
			return;
		
		final float haloDistance = 0.4f;
		
		int dir = player.GetDir();
		
		Iterator<Entity> it = haloEntities.iterator();
		while (it.hasNext())
		{
			Entity haloEntity = it.next();
			if (haloEntity == player.holdedEntity)
				continue;
			
			float dist = planet.Distance(player, haloEntity);
			float distdir = dist*(float)dir;
			if (distdir < 0.f || distdir > haloDistance)
			{
				haloEntity.StopHalo();
				it.remove();
			}
		}

		Entity nextEntity = dir > 0 ?
			player.GetNextEntity() : player.GetPrevEntity();
		
		while (nextEntity != null && nextEntity != player &&
			Math.abs(planet.Distance(player, nextEntity)) < haloDistance)
		{				
			if (nextEntity.haloEnabled
				&& !haloEntities.contains(nextEntity)
				&& player.CanInteract(nextEntity))
			{
				boolean repeat = !interacted.contains(nextEntity.getClass());
				nextEntity.TriggerHalo(repeat);
				haloEntities.add(nextEntity);
			}
			
			nextEntity = dir > 0 ?
				nextEntity.GetNextEntity() : nextEntity.GetPrevEntity();
		}
	}
	
	String interTitleText;
	Color interTitleColorBg = Color.BLACK;
	Color interTitleColorFg = Color.WHITE;
	
	int scriptStartDelay = 3;
	float scriptTime = 0.f;
	float lastScriptTime = 0.f;
	void UpdateScript(float deltatime)
	{
		if (--scriptStartDelay > 0)
			return;
		
		scriptTime += deltatime;
		
		//if (UpdateScript_InfiniteLoop())
		//if (UpdateScript_Stones())
		//if (UpdateScript_TrapperSkill())
		if (UpdateScript_SwitchTest())
		{
			lastScriptTime = scriptTime;
		}
	}
	
	boolean UpdateScript_InfiniteLoop()
	{
		if (lastScriptTime <= 0.f
			&& scriptTime > 0.f)
		{
			Planet planet = CreatePlanet_InfiniteLoop();
			planets.add(planet);
			player.EnterPlanet(planet, 0.f, true);
			
			player.InstantMove(0.3f);
			player.MoveDir(1);
			
			player.disableCameraFollow = true;
			player.disableInputs = true;
			
			return true;
		}
		else if (lastScriptTime <= 3.f
				 && scriptTime > 3.f)
		{
			player.InstantMove(-2.4f);
			player.MoveDir(1);
			
			return true;
		}
		else if (lastScriptTime <= 6.5f
				 && scriptTime > 6.5f)
		{
			player.Say("Have I been here before?");
			
			return true;
		}
		else if (lastScriptTime <= 7.f
				 && scriptTime > 7.f)
		{
			player.Move(0.f);
			
			return true;
		}
		else if (lastScriptTime <= 8.f
				 && scriptTime > 8.f)
		{
			player.Say("Let's try a different direction.");
			
			return true;
		}
		else if (lastScriptTime <= 10.f
				 && scriptTime > 10.f)
		{
			player.MoveDir(-1);

			return true;
		}
		else if (lastScriptTime <= 14.f
				 && scriptTime > 14.f)
		{
			player.InstantMove(2.4f);
			player.MoveDir(-1);
			
			return true;
		}
		else if (lastScriptTime <= 18.f
				 && scriptTime > 18.f)
		{
			player.Move(0.f);
			
			return true;
		}
		else if (lastScriptTime <= 18.5f
				 && scriptTime > 18.5f)
		{
			player.SetDir(1);
			
			return true;
		}
		else if (lastScriptTime <= 19.f
				 && scriptTime > 19.f)
		{
			player.Say("Have I been here before?");
			
			return true;
		}
		else if (lastScriptTime <= 21.f
				 && scriptTime > 21.f)
		{
			player.Say("Let's try a different direction.");
			
			return true;
		}
		else if (lastScriptTime <= 23.f
				 && scriptTime > 23.f)
		{
			player.MoveDir(1);

			return true;
		}
		
		return false;
	}
	
	Planet CreatePlanet_InfiniteLoop()
	{
		Planet planet = new Planet(0.75f, "planet/planet01.png", "planet/background01.png", "planet/switch01.png");
		planet.center.set(0.5f * screen.width, -0.25f * screen.height);

		planet.AddVegetation(20.f);

		return planet;
	}

	Character scriptStonesWife;
	boolean UpdateScript_Stones()
	{
		if (lastScriptTime <= 0.f
			&& scriptTime > 0.f)
		{
			Planet planet = CreatePlanet_Stones();
			planets.add(planet);
			player.EnterPlanet(planet, 0.f, true);
			
			player.InstantMove(1.2f);
			
			planet.AddRock(player.angle * MathUtils.radDeg - 0.1f);
			Entity toHold = player.GetPrevEntity();
			player.HoldEntity(toHold);
			
			player.MoveDir(-1);

			player.disableCameraFollow = true;
			player.disableInputs = true;

			scriptStonesWife = new Character(
				new Biped(eAppearance.WOMAN));
			scriptStonesWife.EnterPlanet(planet, 0.f);
			scriptStonesWife.InstantMove(0.1f);
			scriptStonesWife.SetDir(-1);

			RenderHelpers.SetCameraZoom(1.3f,
				0.6f * RenderHelpers.GetCamera().viewportWidth,
				0.5f * RenderHelpers.GetCamera().viewportHeight);
			
			return true;
		}
		else if (lastScriptTime <= 2.2f
				 && scriptTime > 2.2f)
		{
			player.Move(0.f);

			return true;
		}
		else if (lastScriptTime <= 3.f
				 && scriptTime > 3.f)
		{
			player.Say("Darling, I'm home!");

			return true;
		}
		else if (lastScriptTime <= 3.5f
				 && scriptTime > 3.5f)
		{
			scriptStonesWife.SetDir(1);
			
			return true;
		}
		else if (lastScriptTime <= 5.0f
				 && scriptTime > 5.0f)
		{
			scriptStonesWife.Say("*sigh*                ");

			return true;			
		}
		else if (lastScriptTime <= 6.f
				 && scriptTime > 6.f)
		{
			player.MoveDir(-1);
			
			RenderHelpers.SetCameraZoom(1.f, 3.f);	

			return true;
		}
		else if (lastScriptTime <= 7.5f
				 && scriptTime > 7.5f)
		{
			scriptStonesWife.SetDir(-1);

			return true;
		}
		else if (lastScriptTime <= 8.f
				 && scriptTime > 8.f)
		{
			scriptStonesWife.Say("A stone? Again?!            ");

			return true;
		}
		else if (lastScriptTime <= 9.f
				 && scriptTime > 9.f)
		{
			player.Move(0.f);
			
			return true;
		}
		else if (lastScriptTime <= 9.3f
				 && scriptTime > 9.3f)
		{
			player.StopHolding();
			
			return true;
		}
		else if (lastScriptTime <= 9.8f
				 && scriptTime > 9.8f)
		{
			player.SetDir(1);
			
			return true;
		}
		else if (lastScriptTime <= 10.3f
				 && scriptTime > 10.3f)
		{
			player.SetDir(-1);
			
			return true;
		}
		else if (lastScriptTime <= 12.f
				&& scriptTime > 12.f)
		{
			player.Say("I thought you loved them...");

			return true;
		}
		else if (lastScriptTime <= 15.f
				&& scriptTime > 15.f)
		{
			// TODO: fix zoom to head + monologue
			/*Vector2 wifePos = new Vector2();
			player.GetPlanet().ConvertToWorldPos(scriptStonesWife, wifePos);
			wifePos.x += 0.15f;
			wifePos.y += 0.2f * scriptStonesWife.GetHeight();

			RenderHelpers.SetCameraZoom(4.f, wifePos.x, wifePos.y);		
			*/
			
			scriptStonesWife.Say("I do... It's just...            ");

			return true;
		}
		else if (lastScriptTime <= 17.5f
				&& scriptTime > 17.5f)
		{
			RenderHelpers.SetCameraZoom(1.f);
			
			return true;
		}
		else if (lastScriptTime <= 18.f
				&& scriptTime > 18.f)
		{			
			player.Say("What is it?");

			return true;
		}
		else if (lastScriptTime <= 20.f
				&& scriptTime > 20.f)
		{
			// TODO: rework text if needed
			scriptStonesWife.Say("I'm pregnant.            ");

			return true;
		}
		else if (lastScriptTime <= 21.f
				&& scriptTime > 21.f)
		{
			player.SetDir(1);

			return true;
		}
		else if (lastScriptTime <= 23.f
				&& scriptTime > 23.f)
		{
			RenderHelpers.SetCameraZoom(1.f);
			interTitleText = "A few months later...";
			
			player.InstantMove(0.3f);
			scriptStonesWife.InstantMove(-0.4f);

			return true;
		}
		else if (lastScriptTime <= 27.f
				&& scriptTime > 27.f)
		{
			interTitleText = null;

			RenderHelpers.SetCameraZoom(1.5f,
					0.675f * RenderHelpers.GetCamera().viewportWidth,
					0.55f * RenderHelpers.GetCamera().viewportHeight);
			
			player.MoveDir(1);
			
			return true;
		}

		else if (lastScriptTime <= 29.f
				&& scriptTime > 29.f)
		{
			player.MoveDir(-1);
			
			return true;
		}

		else if (lastScriptTime <= 30.5f
				&& scriptTime > 30.5f)
		{
			player.MoveDir(1);
			
			return true;
		}
		else if (lastScriptTime <= 30.8f
				&& scriptTime > 30.8f)
		{
			scriptStonesWife.Say("  Congratulations, it's a stone!");

			return true;
		}
		else if (lastScriptTime <= 31.3f
				&& scriptTime > 31.3f)
		{			
			player.Move(0.f);
			player.SetDir(-1);

			return true;
		}
		else if (lastScriptTime <= 34.f
				&& scriptTime > 34.f)
		{
			// cancel previous zoom
			RenderHelpers.ForceCameraZoomBlending(0.f);

			// progressive zoom to face
			RenderHelpers.SetCameraZoom(10.f,
					0.75f * RenderHelpers.GetCamera().viewportWidth,
					0.7f * RenderHelpers.GetCamera().viewportHeight, 2.5f);
			
			// but already zoom in little bit
			RenderHelpers.ForceCameraZoomBlending(0.8f);

			return true;
		}
		else if (lastScriptTime <= 36.f
				&& scriptTime > 36.f)
		{
			RenderHelpers.SetCameraZoom(1.f);
			interTitleText = "THE END.";

			return true;
		}

		return false;
	}
	
	Planet CreatePlanet_Stones()
	{
		Planet planet = new Planet(1.f, "planet/planet02.png", "planet/background02.png", "planet/switch02.png");
		planet.center.set(screen.width * 0.5f, -screen.height * 0.5f);
		
		planet.AddRock(-35.f);
		planet.AddRock(-39.f);
		planet.AddRock(-43.f);

		planet.AddRock(-50.f);
		planet.AddRock(-52.f);
		
		planet.AddRock(-58.f);

		return planet;
	}
	
	Character trapperSkillAnimal;
	Entity trapperSkillStone;
	boolean UpdateScript_TrapperSkill()
	{
		if (lastScriptTime <= 0.f
			&& scriptTime > 0.f)
		{
			Planet planet = CreatePlanet_TrapperSkill();
			planets.add(planet);
			player.EnterPlanet(planet, 0.f, true);

			planet.AddRock(0.1f);
			trapperSkillStone = player.GetNextEntity();
			player.HoldEntity(trapperSkillStone);

			player.disableCameraFollow = true;
			player.disableInputs = true;
			player.disableHalo = true;
			
			trapperSkillAnimal =
				(Character)player.GetPrevEntity();

			return true;
		}
		else if (lastScriptTime <= 2.f
				&& scriptTime > 2.f)
		{
			RenderHelpers.SetCameraZoom(20.f,
				0.5f * RenderHelpers.GetCamera().viewportWidth,
				0.8f * RenderHelpers.GetCamera().viewportHeight);
			
			return true;
		}
		else if (lastScriptTime <= 4.f
				&& scriptTime > 4.f)
		{
			Vector2 stonePos = new Vector2();
			player.GetPlanet().ConvertToWorldPos(trapperSkillStone, stonePos);
			RenderHelpers.SetCameraZoom(7.f, stonePos.x, stonePos.y + 0.2f);	
			
			return true;
		}
		else if (lastScriptTime <= 6.f
				&& scriptTime > 6.f)
		{
			RenderHelpers.SetCameraZoom(1.f);
			
			return true;
		}
		else if (scriptTime > 7.f
			&& scriptTime <= 8.5f)
		{
			Vector2 animalPos = new Vector2();
			player.GetPlanet().ConvertToWorldPos(trapperSkillAnimal, animalPos);
			RenderHelpers.SetCameraZoom(1.01f, animalPos.x, animalPos.y, 1.5f);
			
			return true;
		}
		else if (lastScriptTime <= 8.5f
				&& scriptTime > 8.5f)
		{			
			Vector2 stonePos = new Vector2();
			player.GetPlanet().ConvertToWorldPos(trapperSkillStone, stonePos);
			RenderHelpers.SetCameraZoom(2.f, stonePos.x, stonePos.y);
			
			return true;
		}
		else if (lastScriptTime <= 9.5f
			&& scriptTime > 9.5f)
		{
			player.StopHolding();
			
			return true;
		}
		else if (lastScriptTime <= 9.8f
				&& scriptTime > 9.8f)
		{			
			RenderHelpers.SetCameraZoom(1.2f);
			
			return true;
		}
		else if (lastScriptTime <= 10.f
				 && scriptTime > 10.f)
		{
			trapperSkillAnimal.Say("*lblulbll*");
			
			return true;
		}
		else if (lastScriptTime <= 10.5f
				 && scriptTime > 10.5f)
		{
			player.Say("Got it!        ");

			return true;
		}
		else if (lastScriptTime <= 14.f
				 && scriptTime > 14.f)
		{
			RenderHelpers.SetCameraZoom(1.f, 15.f);
			
			return true;
		}
		else if (lastScriptTime <= 14.5f
				 && scriptTime > 14.5f)
		{
			RenderHelpers.SetCameraZoom(1.f, 12.f);
			
			return true;
		}
		else if (lastScriptTime <= 15.f
				 && scriptTime > 15.f)
		{
			RenderHelpers.SetCameraZoom(1.f, 8.f);
			
			return true;
		}
		else if (lastScriptTime <= 18.f
				 && scriptTime > 18.f)
		{			
			player.Say("... Now what?                    ");

			return true;
		}
		else if (lastScriptTime <= 22.4f
				 && scriptTime > 22.4f)
		{
			RenderHelpers.SetCameraZoom(0.9f, 4.f);
			
			return true;
		}
		else if (lastScriptTime <= 25.f
				 && scriptTime > 25.f)
		{
			RenderHelpers.SetCameraZoom(1.f);
			interTitleText = "THE END.";
			
			return true;
		}
		
		return false;
	}

	Planet CreatePlanet_TrapperSkill()
	{
		Planet planet = new Planet(1.f, "planet/planet03.png", "planet/background03.png", "planet/switch03.png");
		planet.center.set(screen.width * 0.5f, -screen.height * 0.5f);

		planet.AddRock(30.f);
		
		planet.AddAnimal(40.f);

		return planet;
	}
	
	Entity switchTest1, switchTest2;
	boolean UpdateScript_SwitchTest()
	{
		if (lastScriptTime <= 0.f
			&& scriptTime > 0.f)
		{
			Planet planet = CreatePlanets_SwitchTest();
			player.EnterPlanet(planet, 0.f, true);
			
			player.disableInputs = true;
			
			player.MoveDir(1.f);
			
			return true;
		}
		else if (lastScriptTime <= 1.2f
			&& scriptTime > 1.2f)
		{
			player.Move(0.f);
			
			return true;
		}
		else if (lastScriptTime <= 3.f
			&& scriptTime > 3.f)
		{
			switchTest1 = player.GetNextEntity();
			player.Interact(switchTest1, touchState.PRESS);
			
			return true;
		}
		else if (lastScriptTime > 3.f
			&& player.IsSwitchingPlanet()
			&& player.IsTalking())
		{
			player.StopTalking();
			
			return true;
		}
		else if (lastScriptTime > 3.f
			&& !player.IsSwitchingPlanet()
			&& player.disableInputs == false)
		{
			player.disableInputs = true;
			
			return true;
		}
		else if (lastScriptTime <= 7.f
			&& scriptTime > 7.f)
		{
			player.MoveDir(1.f);
			
			return true;
		}
		else if (lastScriptTime <= 8.f
			&& scriptTime > 8.f)
		{
			player.Move(0.f);
			
			return true;
		}
		else if (lastScriptTime <= 8.5f
			&& scriptTime > 8.5f)
		{
			switchTest2 = player.GetPrevEntity();
			player.Interact(switchTest2, touchState.PRESS);
			
			return true;
		}
		else if (lastScriptTime > 8.5f
			&& !player.IsSwitchingPlanet()
			&& player.disableInputs == false)
		{
			player.disableInputs = true;

			return true;
		}
		
		return false;
	}
	
	Planet CreatePlanets_SwitchTest()
	{
		Planet planet1 = new Planet(2.f, "planet/planet05.png", "planet/background01.png", "planet/switch0501.png");
		planet1.center.set(0.5f * screen.width, -1.5f * screen.height);

		Planet planet2 = new Planet(0.75f, "planet/planet02.png", "planet/background02.png", "planet/switch02.png");
		planet2.center.set(0.5f * screen.width, -0.25f * screen.height);
		
		planet1.AddPlanetSwitch(15.f, planet2, TriggerType.PLANET_ZONE_TUTO1);
		planet2.AddPlanetSwitch(0.f, planet1, TriggerType.PLANET_ZONE_FIRST_BACK);
		
		planets.add(planet1);
		planets.add(planet2);
		
		return planet1;
	}

	@Override
	public void dispose()
	{
		player.Release();
		player = null;
		
		for (Planet planet : planets)
		{
			if (isDebug)
			{
				planet.CheckSort();
			}
			
			planet.Release();
		}
		planets.clear();
		
		if (inputManager.IsRecording())
		{
			TouchGameRecord record = inputManager.StopRecording();
			inputManager.SaveReplay(record, "replay.dat");
		}

		sprites.dispose();
		shapes.dispose();

		ShaderBank.disposeAll();
		
		TextRenderer.clean();
		
		
		if (cursorTex != null)
		{
			cursorTex.dispose();
			cursorTex = null;
		}
		final boolean fixAndDelay = true;
		eAppearance.CheckCleanMemory(fixAndDelay);
		eEmotion.CheckCleanMemory(fixAndDelay);
		Billboard.CheckCleanMemory(fixAndDelay);
		eSimpleAppearance.CheckCleanMemory(fixAndDelay);

		if (isDebug)
		{
			final boolean stacktrace = false;
			Exceptions.Flush(stacktrace);
		}
	}

	@Override
	public void resize(int width, int height)
	{
		screenRealWidth = Gdx.graphics.getWidth();
		screenRealHeight = Gdx.graphics.getHeight();
	}

	@Override
	public void pause()
	{
	}

	@Override
	public void resume()
	{
	}
	
	// old
	void OldInitScene()
	{
		Planet firstPlanet = CreatePlanet1();
		planets.add(firstPlanet);

		Planet secondPlanet = CreatePlanet2();
		planets.add(secondPlanet);
		 
		Planet thirdPlanet = CreatePlanet3();
		planets.add(thirdPlanet);
		 
		Planet fourthPlanet = CreatePlanet4();
		planets.add(fourthPlanet);

		Planet fifthPlanet = CreatePlanet5();
		planets.add(fifthPlanet);

		firstPlanet.AddPlanetSwitch(-170.f, secondPlanet);
		secondPlanet.AddPlanetSwitch(0.f, firstPlanet);
 
		firstPlanet.AddPlanetSwitch(-30.f, secondPlanet);
		secondPlanet.AddPlanetSwitch(-30.f, thirdPlanet);
		thirdPlanet.AddPlanetSwitch(-30.f, fourthPlanet);
		fourthPlanet.AddPlanetSwitch(-30.f, fifthPlanet);
		fifthPlanet.AddPlanetSwitch(-30.f, firstPlanet);
 		
 		player = new Player(eAppearance.DEFAULT);
		player.renderOrder = 20;
		player.isPlayer = true;
 		player.EnterPlanet(firstPlanet, 0.f);
	}
	
	Planet CreatePlanet1()
	{
		Planet planet = new Planet(1.f, "planet/planet01.png", "planet/background01.png", "planet/switch01.png");
		planet.center.set(
			screen.width * 0.5f,
			-screen.height * 0.5f);

		planet.AddVegetation(20.f);
		planet.AddVegetation(80.f);
		planet.AddVegetation(85.f);
		planet.AddVegetation(120.f);
		planet.AddVegetation(-130.f);
		planet.AddVegetation(-85.f);
		planet.AddVegetation(-75.f);
		planet.AddVegetation(-50.f);

		planet.ApplyToEntities(new Planet.EntityFunction()
			{
				@Override
				public void apply(Entity entity)
				{
					entity.EnableAI(TriggerType.DISCOVER_VEGETATION);
				}
			});
			
		planet.AddRock(40.f);

		// AI test
		for (int i = 0; i < 5; i++)
		{
			planet.AddHuman(0.f);
		}

		// AI test
		for (int i = 0; i < 5; i++)
		{
			planet.AddAnimal(0.f);
		}

		planet.AddShelter(10.f);

		planet.AddBush(20.f, -0.2f);

		planet.AddBush(-5.f);
		planet.AddBush(-8.f);
		planet.AddBush(-15.f);

		planet.AddBush(-6.f, -0.08f);
		planet.AddBush(-7.f, -0.08f);
		planet.AddBush(-12.f, -0.08f);

		planet.AddBush(-7.f, -0.18f);
		planet.AddBush(-9.f, -0.18f);
		planet.AddBush(-15.f, -0.18f);

		planet.AddBush(-4.f, -0.28f);
		planet.AddBush(-8.5f, -0.28f);
		planet.AddBush(-12.f, -0.28f);

		planet.AddBush(-5.f, -0.4f);
		planet.AddBush(-10.f, -0.4f);
		planet.AddBush(-15.f, -0.4f);

		planet.AddBush(-6.f, -0.5f);
		planet.AddBush(-9.f, -0.5f);
		planet.AddBush(-12.f, -0.5f);

		planet.AddAnimal(45.f);
		
		return planet;
	}

	Planet CreatePlanet2()
	{
		Planet planet = new Planet(1.f, "planet/planet02.png", "planet/background02.png", "planet/switch02.png");
		planet.center.set(
			screen.width * 0.5f,
			-screen.height * 0.5f);

		planet.AddVegetation(20.f);
		planet.AddVegetation(80.f);
		planet.AddVegetation(85.f);
		planet.AddVegetation(120.f);
		planet.AddVegetation(-130.f);
		planet.AddVegetation(-85.f);
		planet.AddVegetation(-75.f);
		planet.AddVegetation(-50.f);

		planet.ApplyToEntities(new Planet.EntityFunction()
			{
				@Override
				public void apply(Entity entity)
				{
					entity.EnableAI(TriggerType.AGAIN_VEGETATION);
				}
			});

		return planet;
	}

	Planet CreatePlanet3()
	{
		Planet planet = new Planet(1.f, "planet/planet03.png", "planet/background03.png", "planet/switch03.png");
		planet.center.set(
				screen.width * 0.5f,
				-screen.height * 0.5f);

		planet.AddVegetation(20.f);

		return planet;
	}

	Planet CreatePlanet4()
	{
		Planet planet = new Planet(1.f, "planet/planet04.png", "planet/background04.png", "planet/switch04.png");
		planet.center.set(
				screen.width * 0.5f,
				-screen.height * 0.5f);

		planet.AddVegetation(20.f);

		return planet;
	}

	Planet CreatePlanet5()
	{
		Planet planet = new Planet(1.f, "planet/planet05.png", "planet/background05.png", "planet/switch05.png");
		planet.center.set(
				screen.width * 0.5f,
				-screen.height * 0.5f);

		planet.AddVegetation(20.f);

		return planet;
	}
}
