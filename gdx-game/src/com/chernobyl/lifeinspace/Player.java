package com.chernobyl.lifeinspace;

import com.chernobyl.common.render.manekin.eAppearance;
import com.chernobyl.common.render.manekin.Biped;
import com.chernobyl.common.render.manekin.Manekin.AnimAction;
import com.chernobyl.common.utils.RenderHelpers;

import com.badlogic.gdx.math.Vector2;

public class Player extends Character
{
	final static float enterPlanetTime = 2.5f;
	final static float starTransitionTime = 0.3f;
	
	float enterPlanetTimeLeft;
	Planet enterPlanet;
	float enterAngle;
	
	Vector2 camPos;
	
	public boolean disableCameraFollow;
	public boolean disableInputs;
	public boolean disableHalo;

	Player()
	{
		super(new Biped(eAppearance.DEFAULT));
		
		camPos = new Vector2();
	}

	Player(eAppearance appearance)
	{
		super(new Biped(appearance));
		
		camPos = new Vector2();
	}

	@Override
	public String GetInteractionType()
	{
		return "Player";
	}

	@Override
	public void Update(float deltatime)
	{
		boolean wasPlayingAction =
			manekin.IsPlayingAction();
			
		super.Update(deltatime);

		if (enterPlanetTimeLeft > 0.f)
		{
			if (wasPlayingAction)
			{
				if (!manekin.IsPlayingAction())
				{
					float jumpTime = enterPlanetTimeLeft
						- 0.5f * AnimAction.JUMP_END.time;
					
					Jump(-0.5f * jumpTime * Entity.gravity);

					// rotate planet until player look straight
					float diffAngle = curPlanet.DiffAngle(this);
					curPlanet.rotate(diffAngle / (-0.5f * jumpTime));
				}
			}
			else if (enterPlanetTimeLeft <=
				0.5f * AnimAction.JUMP_END.time)
			{
				manekin.PlayAction(AnimAction.JUMP_END);
			}
			
			// camera follow
			curPlanet.ConvertToWorldPos(this, camPos);
			RenderHelpers.SetCameraZoom(1.5f,
				camPos.x, camPos.y,
				0.25f * enterPlanetTime);
										
			// jump transition
			enterPlanetTimeLeft -= deltatime;
			if (enterPlanetTimeLeft > 0.f)
			{
				// TODO: arbitrary offset/rotate target planet
				// and interpolate camera
				
				float blend = enterPlanetTimeLeft
					/ enterPlanetTime;
				if (blend <= 0.5f)
				{
					if (enterPlanet != null)
					{
						curPlanet.SetBackgroundAlpha(1.f);
						enterPlanet.SetBackgroundAlpha(0.f);
						
						curPlanet.rotate(0.f);
						
						EnterPlanet(
							enterPlanet, enterAngle, true);
						enterPlanet = null;
					}
					
					// manually blend out camera zoom
					RenderHelpers.ForceCameraZoomBlending(
						blend * 2.f);
						
					// show background
					float timeLeftbgFadeIn = enterPlanetTimeLeft
						- 0.5f * AnimAction.JUMP_END.time;
					if (timeLeftbgFadeIn < starTransitionTime)
					{
						float bgBlend = timeLeftbgFadeIn
							/ starTransitionTime;
						if (bgBlend < 0.f) bgBlend = 0.f;
						
						curPlanet.SetBackgroundAlpha(
							1.f - bgBlend);
					}
				}
				else if (curPlanet.GetBackgroundAlpha() > 0.f)
				{
					float diffTime = enterPlanetTime
						- enterPlanetTimeLeft
						- AnimAction.JUMP_START.time;
						
					float bgBlend = diffTime
						/ starTransitionTime;
					if (bgBlend > 1.f) bgBlend = 1.f;
					else if (bgBlend < 0.f) bgBlend = 0.f;
					
					curPlanet.SetBackgroundAlpha(
						1.f - bgBlend);
				}
			}
			else
			{
				// reset camera zoom
				RenderHelpers.SetCameraZoom(1.f);

				// put back inputs
				disableInputs = false;
				disableCameraFollow = false;
				disableHalo = false;
			}
		}
	}
	
	public void EnterPlanet(Planet planet, float angle,
		boolean instant)
	{
		if (!instant)
		{
			EnterPlanet(planet, angle);
			return;
		}
		
		super.EnterPlanet(planet, angle);
		
		planet.angle = (float)Math.PI * 2.f
			- this.angle;
	}

	@Override
	public void EnterPlanet(Planet planet, float angle)
	{
		if (curPlanet == null
			|| planet == null)
		{
			EnterPlanet(planet, angle, true);
			return;
		}

		enterPlanet = planet;
		enterAngle = angle;

		enterPlanetTimeLeft = enterPlanetTime;
		
		// prepare for jump
		manekin.PlayAction(AnimAction.JUMP_START);

		disableInputs = true;
		disableCameraFollow = true;
		disableHalo = true;
	}
	
	public boolean IsSwitchingPlanet()
	{
		return enterPlanetTimeLeft > 0.f;
	}
}
