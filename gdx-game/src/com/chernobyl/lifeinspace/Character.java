package com.chernobyl.lifeinspace;

import java.util.ArrayList;
import java.util.Iterator;

import com.chernobyl.common.inputs.TouchState;
import com.chernobyl.common.render.manekin.SimpleBiped;
import com.chernobyl.common.render.manekin.SimpleMultiped;
import com.chernobyl.common.render.manekin.eAppearance;
import com.chernobyl.common.render.manekin.eSimpleAppearance;
import com.chernobyl.common.render.manekin.Biped;
import com.chernobyl.common.render.manekin.Manekin;
import com.chernobyl.common.render.manekin.Talkable;
import com.chernobyl.common.utils.JBTHelpers;
import com.chernobyl.common.utils.JBTHelpers.Interaction;
import com.chernobyl.common.utils.JBTHelpers.AiType;
import com.badlogic.gdx.math.Vector2;

public class Character extends Entity
{
	final static float normalSpeed = 0.3f;
	
	Manekin manekin;
	
	Planet prevPlanet;
	float prevPlanetAngle;
	
	float moveTime;
	
	float speedScale;
	
	boolean blocked;
	
	Interaction interaction;
	ArrayList<Interaction> oldInteractions;
	
	Entity holdedEntity;
	
	static public class Human extends Character
	{
		Human()
		{
			super(new SimpleBiped(eSimpleAppearance.HUMAN));
			
			init();
		}
		
		Human(eSimpleAppearance appearance)
		{
			super(new SimpleBiped(appearance));
			
			init();
		}
		
		private void init()
		{
			SetScale(0.2f);
			ScaleSpeed(8.f);
			((SimpleBiped)manekin).ScaleCycleSpeed(0.2f);
		}
		
		@Override
		public String GetInteractionType()
		{
			return "Human";
		}
	}
	
	static public class Animal extends Character
	{
		Animal()
		{
			super(new SimpleMultiped(eSimpleAppearance.ANIMAL));
			
			init();
		}
		
		Animal(eSimpleAppearance appearance)
		{
			super(new SimpleMultiped(appearance));
			
			init();
		}
		
		private void init()
		{
			SetScale(0.2f);
			ScaleSpeed(10.f);
			((SimpleMultiped)manekin).ScaleCycleSpeed(0.2f);			
		}
		
		@Override
		public String GetInteractionType()
		{
			return "Animal";
		}
	}

	public Character(Manekin initManekin)
	{
		super(initManekin);
		manekin = initManekin;
		
		speedScale = 1.f;
		
		oldInteractions = new ArrayList<Interaction>();
	}
	
	void SetSpeed(float speed)
	{
		float newScale =
			speed / normalSpeed;
			
		ScaleSpeed(newScale / speedScale);
	}
	
	public void ScaleSpeed(float scale)
	{
		speedScale *= scale;
		moveTime /= scale;
	}
	
	@Override
	public String GetInteractionType()
	{
		return "Character";
	}
	
	@Override
	public void Update(float deltatime)
	{
		if (moveTime > 0.f)
		{
			moveTime -= deltatime;
			if (moveTime <= 0.f)
			{
				float moveDir = Math.signum(angleSpeed);
				InstantMove(-moveTime * moveDir
					* normalSpeed * scale * speedScale);
				moveTime = 0.f;
			}
		}
		
		super.Update(deltatime);
		
		UpdateInteraction();
		
		UpdateHolding();
	}
	
	@Override
	public void Release()
	{		
		super.Release();
	}
	
	public void EnterPlanet(Planet planet, float newAngle)
	{		
		StopTalking();
		
		// TODO: get proper position from switch
		float startAngle;
		if (prevPlanet == planet)
			startAngle = prevPlanetAngle;
		else
			startAngle = newAngle;

		prevPlanet = curPlanet;
		prevPlanetAngle = angle;
		
		super.EnterPlanet(planet, startAngle);
		
		// HACK: a little bit lower
		OffsetDist(-0.06f * scale);
		
		if (holdedEntity != null)
		{
			holdedEntity.EnterPlanet(planet, startAngle);
		}
	}
	
	public void EnableAI(AiType aiType)
	{
		if (agentTask == null)
		{
			agentTask = JBTHelpers.GetAgentTask(aiType, this);
		}
	}
	
	public boolean CanInteract(Entity entity)
	{
		String interactionType = entity.GetInteractionType();
		return JBTHelpers.HasInteraction(interactionType);
	}
	
	public boolean Interact(Entity entity, TouchState state)
	{
		if (state == TouchState.PRESS)
		{			
			if (interaction != null
				&& !interaction.IsCancellable())
			{
				if (interaction.GetTarget() == entity)
					return false;
			}
			
			StopInteraction();

			for (Interaction oldInteraction : oldInteractions)
			{
				if (oldInteraction.GetTarget() == entity)
				{
					return false;
				}
			}
			
			String interactionType = entity.GetInteractionType();
			interaction = JBTHelpers.GetInteraction(interactionType, entity, this);
			
			return interaction != null;
		}
		
		return false;
	}
	
	public void UpdateInteraction()
	{		
		Iterator<Interaction> it = oldInteractions.iterator();
		while (it.hasNext())
		{
			Interaction oldInteraction = it.next();
			if (!oldInteraction.run())
			{
				it.remove();
			}
		}
		
		if (interaction != null)
		{
			if (!interaction.run())
			{
				interaction = null;
			}
		}
	}
	
	public void StopInteraction()
	{
		if (interaction != null)
		{
			if (!interaction.IsCancellable())
			{
				oldInteractions.add(interaction);
			}
			else
			{
				interaction.forceFinish();
			}
			
			interaction = null;
		}
	}
	
	public boolean IsInteracting()
	{
		return interaction != null;
	}
	
	public void Say(String text)
	{
		Talkable talkable =
			manekin.GetTalkable();
				
		talkable.Say(text);
	}
	
	public void StopTalking()
	{
		Talkable talkable =
			manekin.GetTalkable();
			
		talkable.StopTalking();
	}
	
	public boolean IsTalking()
	{
		Talkable talkable =
			manekin.GetTalkable();
			
		return talkable.IsTalking();
	}
	
	public void SetDir(int dir)
	{
		manekin.SetDir(dir);
	}
	
	public int GetDir()
	{
		return manekin.GetDir();
	}
	
	@Override
	public void InstantMove(float diff)
	{
		if (blocked)
			return;		
		
		super.InstantMove(diff);
	}
	
	@Override
	public void Move(float speed)
	{
		if (blocked)
			return;
		
		manekin.SetMove(speed /
			(normalSpeed * scale));
		super.Move(speed);
		
		moveTime = 0.f; // infinite move
	}
	
	public void MoveTo(float targetAngle)
	{
		if (blocked)
			return;
		
		float diffAngle = targetAngle - angle;
		if (diffAngle > Math.PI)
			diffAngle -= Math.PI * 2.f;
		else if (diffAngle < -Math.PI)
			diffAngle += Math.PI * 2.f;
		
		MoveDiff(diffAngle * GetDist());
	}
	
	public void MoveDiff(float diff)
	{
		if (blocked)
			return;
		
		float absDiff = Math.abs(diff);
		if (absDiff < 0.001f)
			return;
		
		MoveDir(diff / absDiff);
		moveTime = absDiff /
			(normalSpeed * scale * speedScale);
	}
	
	public void MoveDir(float dir)
	{		
		if (blocked)
			return;
		
		Move(normalSpeed *
			scale * speedScale * dir);
	}
	
	public void BlockMovement(boolean blocked)
	{
		this.blocked = blocked;
		if (blocked)
		{
			Move(0.f);
		}
	}
	
	public void HoldEntity(Entity entity)
	{
		if (IsHolding())
			return;
		
		holdedEntity = entity;
		holdedEntity.SetHold(this);
		
		manekin.HoldObject(entity.renderData, entity.scale);
	}
	
	public void StopHolding()
	{
		if (!IsHolding())
			return;
				
		holdedEntity.SetHold(null);
		holdedEntity = null;
		
		manekin.StopHolding();
	}
	
	public void UpdateHolding()
	{
		if (!IsHolding())
			return;
		
		// TODO: entity position is wrong comparing draw position
		// because at holded entity angle, local rot and offset are different
		// result: halo have wrong offset, picking bounds are wrong
		
		// put at character position
		float diff = curPlanet.Distance(
			holdedEntity, this);
		// offset with holding local pos
		Vector2 offset = manekin.GetHoldingPos();
		holdedEntity.InstantMove(diff + offset.x);
		holdedEntity.InstantJump(offset.y);
	}
	
	public boolean IsHolding()
	{
		return holdedEntity != null;
	}
}
