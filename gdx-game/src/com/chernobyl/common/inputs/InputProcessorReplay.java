package com.chernobyl.common.inputs;

public class InputProcessorReplay extends InputProcessor
{
	TouchGameRecord gameRecord = null;
	
	InputProcessorReplay(TouchGameRecord gameRecord)
	{
		this.gameRecord = gameRecord;
	}
	
	@Override
	public boolean Update(float deltaTime)
	{
		TouchFrameRecord frameRecord = gameRecord.NextFrame();
		if (null == frameRecord)
		{
			return false;
		}
		
		NotifyListenersTick(frameRecord.deltaTime);
		
		for (TouchRecord rec : frameRecord.recordList)
		{
			NotifyListenersInput(rec.state, rec.x, rec.y);
		}
		
		return true;
	}
}
