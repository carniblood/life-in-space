package com.chernobyl.common.inputs;

import java.io.Serializable;

public class TouchRecord implements Serializable
{
    private static final long serialVersionUID = 1L;
    
	TouchState state;
	int x;
	int y;
	
	TouchRecord(TouchState state, int x, int y)
	{
		this.state = state;
		this.x = x;
		this.y = y;
	}
}
