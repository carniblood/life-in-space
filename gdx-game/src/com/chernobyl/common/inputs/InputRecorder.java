package com.chernobyl.common.inputs;

public class InputRecorder implements InputListener
{
	TouchGameRecord gameRecord = new TouchGameRecord();
	
	@Override
	public void OnTick(float deltaTime)
	{
		TouchFrameRecord frameRecord = new TouchFrameRecord(deltaTime);
		gameRecord.AddFrame(frameRecord);
	}

	@Override
	public void OnInput(TouchState state, int x, int y)
	{
		TouchRecord record = new TouchRecord(state, x, y);
		gameRecord.AddRecord(record);
	}

	@Override
	public void OnKeyDown(int keycode)
	{
	}
}
