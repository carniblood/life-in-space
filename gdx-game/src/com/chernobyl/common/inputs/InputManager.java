package com.chernobyl.common.inputs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import com.chernobyl.common.utils.Serializer;

public class InputManager extends InputProcessor implements InputListener
{
	private InputProcessor processor;
	private InputProcessorGdx defaultProcessor;
	private InputProcessorReplay replayer;
	
	private InputRecorder recorder = null;
	
	public InputManager()
	{
		defaultProcessor = new InputProcessorGdx();
		
		SetProcessor(defaultProcessor);
	}
	
	public void StartReplay(TouchGameRecord gameRecord)
	{
		replayer = new InputProcessorReplay(gameRecord);
		
		SetProcessor(replayer);
	}
	
	public void StopReplay()
	{
		if (replayer != null)
		{
			SetProcessor(defaultProcessor);
			
			replayer = null;
		}
	}
	
	public boolean IsReplaying()
	{
		return (replayer != null);
	}
	
	public void StartRecording()
	{
		StopRecording();
		
		recorder = new InputRecorder();
		AddListener(recorder);
	}
	
	public TouchGameRecord StopRecording()
	{
		TouchGameRecord gameRecord = null;
		
		if (null != recorder)
		{
			gameRecord = recorder.gameRecord;

			RemoveListener(recorder);
			recorder = null;
		}
		
		return gameRecord;
	}
	
	public boolean IsRecording()
	{
		return (recorder != null);
	}
	
	public void SaveReplay(TouchGameRecord gameRecord, String filename)
	{
		FileHandle file = Gdx.files.local(filename);
		try
		{
            file.writeBytes(Serializer.serialize(gameRecord), false);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
	}
	
	public TouchGameRecord LoadReplay(String filename)
	{
        FileHandle file = Gdx.files.local(filename);
        if (!file.exists())
        {
        	return null;
        }

		TouchGameRecord replay = null;
		try
		{
			replay = (TouchGameRecord)Serializer.deserialize(file.readBytes());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
		
        return replay;
	}
	
	public void SetProcessor(InputProcessor processor)
	{
		if (null != this.processor)
		{
			this.processor.RemoveListener(this);
		}
		
		this.processor = processor;
		processor.AddListener(this);
	}
	
	@Override
	public boolean Update(float deltaTime)
	{
		return processor.Update(deltaTime);
	}
	
	@Override
	public void OnTick(float deltaTime)
	{
		NotifyListenersTick(deltaTime);
	}
	
	@Override
	public void OnInput(TouchState state, int x, int y)
	{
		NotifyListenersInput(state, x, y);
	}
	
	@Override
	public void OnKeyDown(int keycode)
	{
		NotifyListenersKeyDown(keycode);
	}
}
