package com.chernobyl.common.inputs;

import java.util.ArrayList;

public abstract class InputProcessor
{
	private ArrayList<InputListener> listeners = new ArrayList<InputListener>();
	
	abstract public boolean Update(float deltaTime);
	
	public void AddListener(InputListener listener)
	{
		if (!listeners.contains(listener))
		{
			listeners.add(listener);
		}
	}
	
	public void RemoveListener(InputListener listener)
	{
		listeners.remove(listener);
	}
	
	public void NotifyListenersTick(float deltaTime)
	{
		for (InputListener listener : listeners)
		{
			listener.OnTick(deltaTime);
		}
	}
	
	public void NotifyListenersInput(TouchState state, int x, int y)
	{
		for (InputListener listener : listeners)
		{
			listener.OnInput(state, x, y);
		}
	}
	
	public void NotifyListenersKeyDown(int keycode)
	{
		for (InputListener listener : listeners)
		{
			listener.OnKeyDown(keycode);
		}
	}
}
