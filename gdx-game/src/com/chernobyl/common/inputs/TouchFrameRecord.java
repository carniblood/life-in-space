package com.chernobyl.common.inputs;

import java.io.Serializable;
import java.util.ArrayList;

public class TouchFrameRecord implements Serializable
{
    private static final long serialVersionUID = 1L;
    
	float deltaTime;
	ArrayList<TouchRecord> recordList = new ArrayList<TouchRecord>();
	
	TouchFrameRecord(float deltaTime)
	{
		this.deltaTime = deltaTime;
	}
	
	public void AddRecord(TouchRecord record)
	{
		recordList.add(record);
	}
}
