package com.chernobyl.common.inputs;

import java.util.ArrayList;
import java.io.Serializable;

public class TouchGameRecord implements Serializable
{
    private static final long serialVersionUID = 1L;
    
	private ArrayList<TouchFrameRecord> frameList = new ArrayList<TouchFrameRecord>();
	private int currFrame = 0;

	public void Reset()
	{
		currFrame = 0;
	}
	
	public void AddFrame(TouchFrameRecord frame)
	{
		frameList.add(frame);
	}
	
	public void AddRecord(TouchRecord record)
	{
		if (frameList.size() > 0)
		{
			frameList.get(frameList.size() - 1).AddRecord(record);
		}
	}
	
	public TouchFrameRecord NextFrame()
	{
		TouchFrameRecord nextFrame = null;
		if (frameList.size() > currFrame)
		{
			nextFrame = frameList.get(currFrame);
		}
		
		++currFrame;
		
		return nextFrame;
	}
}
