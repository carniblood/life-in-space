package com.chernobyl.common.inputs;

public enum TouchState
{
	PRESS(1.f),
	REPEAT(0.f),
	RELEASE(-1.f);
	
	float _value;
	
	TouchState(float value)
	{
		_value = value;
	}
	
	public float value()
	{
		return _value;
	}
	
	public String ToString()
	{
		if (this == PRESS)
		{
			return "PRESS";
		}
		else if (this == REPEAT)
		{
			return "REPEAT";
		}
		else if (this == RELEASE)
		{
			return "RELEASE";
		}
		
		return "UNKNOWN";
	}
}
