package com.chernobyl.common.render.manekin;

import com.chernobyl.common.utils.Exceptions;
import com.chernobyl.common.utils.RenderHelpers;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.Gdx;

import java.util.ArrayList;
import java.util.HashMap;

public enum eEmotion
{
	/*NEUTRAL   (	"head.png"			),
	HAPPY 	  (	"head_smile.png"	),
	UPSET 	  (	"head_upset.png"	),
	SURPRISED (	"head_surprised.png"),
	LOVE      (	"head_love.png"		),
	DEAD      (	"head_dead.png"		);*/

	NEUTRAL   (	"head.png"	);

	String texName;

	HashMap<String, AssetTexture> textureMap =
	new HashMap<String, AssetTexture>();

	private eEmotion(String texName)
	{
		this.texName = texName;
	}

	public static int total = 0;

	private static final ArrayList<eEmotion> list
		= new ArrayList<eEmotion>();
	static
	{
		for (eEmotion emo : eEmotion.values())
		{
			list.add(emo);
			total++;
		}
	}

	public static eEmotion from(int i)
	{
		return list.get(i);
	}

	private class AssetTexture
	{
		String assetName;
		Texture texture;
		int use;

		public AssetTexture(String assetName)
		{
			this.use = 0;
			this.assetName = assetName;
		}

		public void Load(String texName)
		{
			if (++use == 1)
			{
				FileHandle dir = Gdx.files.internal(
					"graphics/" + assetName + "/");
					
				texture = RenderHelpers.LoadTexture(
					dir.child(texName));
			}			
		}

		public void Release()
		{				
			if (--use == 0)
			{
				texture.dispose();
				texture = null;
			}			
		}
	}

	public void LoadTexture(String dirName)
	{
		AssetTexture texture = textureMap.get(dirName);
		if (texture == null)
		{
			texture = new AssetTexture(dirName);
			textureMap.put(dirName, texture);
		}

		texture.Load(texName);
	}

	public void ReleaseTexture(String dirName)
	{
		AssetTexture texture = textureMap.get(dirName);
		if (texture == null)
		{
			return;
		}

		texture.Release();
	}

	public Texture GetTexture(String dirName)
	{
		AssetTexture texture = textureMap.get(dirName);
		if (texture == null)
		{
			return null;
		}

		return texture.texture;
	}

	public static void LoadTextures(String dirName)
	{
		for (eEmotion emo : eEmotion.values())
			emo.LoadTexture(dirName);
	}

	public static void ReleaseTextures(String dirName)
	{
		for (eEmotion emo : eEmotion.values())
			emo.ReleaseTexture(dirName);
	}
	
	public static void CheckCleanMemory(boolean fixAndDelay)
	{
		for (eEmotion emo : eEmotion.values())
		{
			for (AssetTexture tex : emo.textureMap.values())
			{
				if (tex.use > 0)
				{
					String pathAndName =
						tex.assetName + "/" + emo.texName;
						
					if (fixAndDelay)
					{
						Exceptions.PushAssetNotReleased(
							pathAndName, tex.use);
							
						tex.Release();
						tex.use = 0;
					}
					else
					{
						Exceptions.ThrowAssetNotReleased(
							pathAndName, tex.use);
					}
				}
				else if (tex.use < 0)
				{
					String pathAndName =
						tex.assetName + "/" + emo.texName;
					
					if (fixAndDelay)
					{
						Exceptions.PushAssetReleasedTwice(
							pathAndName, tex.use);
					}
					else
					{
						Exceptions.ThrowAssetReleasedTwice(
							pathAndName, tex.use);
					}
				}
			}
		}
	}
}

