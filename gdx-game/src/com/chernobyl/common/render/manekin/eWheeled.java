package com.chernobyl.common.render.manekin;

import com.chernobyl.common.utils.Exceptions;
import com.chernobyl.common.utils.RenderHelpers;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.Gdx;

public enum eWheeled
{
	DEFAULT		( "car" );

	public String assetName;

	public Texture texBody;
	public Texture texWheel;

	int texUse = 0;

	private eWheeled(String assetName)
	{
		this.assetName = assetName;
	}
	
	public void LoadTextures()
	{
		if (++texUse == 1)
		{
			FileHandle assetDir = Gdx.files.internal(
				"graphics/" + assetName + "/");

			texBody = RenderHelpers.LoadTexture(
				assetDir.child("body.png"));
			texWheel = RenderHelpers.LoadTexture(
				assetDir.child("wheel.png"));
		}
	}

	public void ReleaseTextures(boolean force)
	{
		if (force)
		{
			texUse = 1;
		}

		ReleaseTextures();
	}

	public void ReleaseTextures()
	{
		if (--texUse == 0)
		{
			if (texBody != null)
			{
				texBody.dispose();
				texBody = null;
			}

			if (texWheel != null)
			{
				texWheel.dispose();
				texWheel = null;
			}
		}
	}

	public static void CheckCleanMemory(boolean fixAndDelay)
	{
		for (eWheeled wheeled : eWheeled.values())
		{
			if (wheeled.texUse > 0)
			{
				String name = "wheeled." + wheeled.assetName;

				if (fixAndDelay)
				{
					Exceptions.PushAssetNotReleased(
						name, wheeled.texUse);

					wheeled.ReleaseTextures(true);
				}
				else
				{
					Exceptions.ThrowAssetNotReleased(
						name, wheeled.texUse);
				}
			}
			else if (wheeled.texUse < 0)
			{
				String name = "wheeled." + wheeled.assetName;

				if (fixAndDelay)
				{
					Exceptions.PushAssetReleasedTwice(
						name, wheeled.texUse);
				}
				else
				{
					Exceptions.ThrowAssetReleasedTwice(
						name, wheeled.texUse);
				}
			}
		}
	}
}
