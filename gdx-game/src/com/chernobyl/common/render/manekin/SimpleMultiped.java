package com.chernobyl.common.render.manekin;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import com.chernobyl.common.render.RenderData;

public class SimpleMultiped extends Manekin
{
	static final float bodyWidth = 0.2f;
	static final float bodyHeight = 0.125f;
	
	static final float actionAnimTime = 1.f;

	static final float restStepLeft = 0.75f;
	static final float restStepRight = 0.25f;
	
	static final float walkLegAngle = 20.f;
	
	static final float cycleSpeed = 2.f;

	eSimpleAppearance appearance;

	Sprite body;
	Sprite head;
	Sprite legFL, legFR;
	Sprite legBL, legBR;
	
	Rectangle bounds;

	int dir = 0;

	int moveDir = 0;
	float moveScale = 0.f;

	float step = restStepRight;
	
	AnimAction playAction;
	float playActionCycle = 0.f;
	
	RenderData holdedObject;
	float holdedScale;
	Vector2 holdingPos;
	
	float cycleSpeedScale = 0.f;

	public SimpleMultiped(eSimpleAppearance appearance)
	{
		this.appearance = appearance;
		Init();
		
		cycleSpeedScale = 1.f;
	}

	private void Init()
	{
		appearance.LoadTextures();

		body = new Sprite(appearance.texBody);
		head = new Sprite(appearance.texHead);

		legFR = new Sprite(appearance.texLegFR);
		legFL = new Sprite(appearance.texLegFL);

		legBR = new Sprite(appearance.texLegBR);
		legBL = new Sprite(appearance.texLegBL);
		
		bounds = new Rectangle();

		initBody();
		
		SetDir(1);
	}

	@Override
	public int GetDir()
	{
		return dir;
	}

	@Override
	public void SetDir(int dir)
	{
		if (this.dir == dir)
			return;

		body.setFlip(dir < 0, false);
		head.setFlip(dir < 0, false);

		legFR.setFlip(dir < 0, false);
		legFL.setFlip(dir < 0, false);
		legBR.setFlip(dir < 0, false);
		legBL.setFlip(dir < 0, false);

		if (this.dir != 0)
		{
			float centerX = body.getWidth() * 0.5f;

			// Invert leg pivot points on x
			legFR.setOrigin(2.0f * centerX - legFR.getOriginX(), legFR.getOriginY());
			legFL.setOrigin(2.0f * centerX - legFL.getOriginX(), legFL.getOriginY());
			legBR.setOrigin(2.0f * centerX - legBR.getOriginX(), legBR.getOriginY());
			legBL.setOrigin(2.0f * centerX - legBL.getOriginX(), legBL.getOriginY());
		}

		this.dir = dir;
	}

	@Override
	public void SetMove(float move)
	{
		float absMove = Math.abs(move);
		if (absMove < 0.001f)
		{
			moveDir = 0;
		}
		else
		{
			moveScale = absMove;
			moveDir = (int)(move / absMove);
			SetDir(moveDir);
		}
	}
	
	public void ScaleCycleSpeed(float scale)
	{
		cycleSpeedScale *= scale;
	}
	
	private void initBody()
	{		
		setBasePose();	
		updateBounds();
	}

	private void setBasePose()
	{
		initSpriteFromBodySize(body);
		initSpriteFromBodySize(head);

		initSpriteFromBodySize(legFR);
		legFR.setOrigin(
				legFR.getWidth() * 0.6f,
				legFR.getHeight() * 0.25f);

		initSpriteFromBodySize(legFL);
		legFL.setOrigin(
				legFL.getWidth() * 0.6f,
				legFL.getHeight() * 0.25f);

		initSpriteFromBodySize(legBR);
		legBR.setOrigin(
				legBR.getWidth() * 0.3f,
				legBR.getHeight() * 0.25f);

		initSpriteFromBodySize(legBL);
		legBL.setOrigin(
				legBL.getWidth() * 0.3f,
				legBL.getHeight() * 0.25f);
	}

	private void initSpriteFromBodySize(Sprite sprite)
	{
		sprite.setSize(bodyWidth, bodyHeight);
		sprite.setCenterX(0.f);
		sprite.setOrigin(
			sprite.getWidth() * 0.5f,
			sprite.getHeight() * 0.5f);
	}
	
	private void updateBounds()
	{
		bounds.set(body.getBoundingRectangle());

		bounds.merge(head.getBoundingRectangle());

		bounds.merge(legFL.getBoundingRectangle());
		bounds.merge(legFR.getBoundingRectangle());

		bounds.merge(legBL.getBoundingRectangle());
		bounds.merge(legBR.getBoundingRectangle());
	}

	@Override
	public void Update(float deltatime)
	{
		super.Update(deltatime);

		if (moveDir == 0)
		{
			if (step != restStepLeft && step != restStepRight)
			{
				if (dir == 0)
				{
					step = restStepRight;
				}
				else
				{
					if (dir > 0)
					{
						float newStep = step +
								deltatime * cycleSpeed * cycleSpeedScale * moveScale;
						newStep -= MathUtils.floor(newStep);

						if (step < restStepLeft && newStep > restStepLeft)
							step = restStepLeft;
						else if (step < restStepRight && newStep > restStepRight)
							step = restStepRight;
						else
							step = newStep;
					}
					else if (dir < 0)
					{
						float newStep = step -
								deltatime * cycleSpeed * cycleSpeedScale * moveScale;
						newStep -= MathUtils.floor(newStep);

						if (step > restStepLeft && newStep < restStepLeft)
							step = restStepLeft;
						else if (step > restStepRight && newStep < restStepRight)
							step = restStepRight;
						else
							step = newStep;
					}
				}
			}
		}
		else
		{
			step += deltatime * moveDir * cycleSpeed
					* cycleSpeedScale * moveScale;
			step -= MathUtils.floor(step);
		}

		float legAngle;
		if (step <= 0.5f)
			legAngle = walkLegAngle * (4.f * step - 1.f);
		else
			legAngle = walkLegAngle * (4.f * (1.f - step) - 1.f);

		legFL.setRotation(legAngle);

		if (IsHolding())
			legFR.setRotation(dir * 90.f);
		else
			legFR.setRotation(-legAngle);

		legBR.setRotation(legAngle);
		legBL.setRotation(-legAngle);
	}

	@Override
	public void Draw(SpriteBatch batch)
	{
		super.Draw(batch);
		
		legBL.draw(batch);
		legFL.draw(batch);

		body.draw(batch);
			
		if (IsHolding())
		{
			DrawHolding(batch);
		}

		legBR.draw(batch);
		legFR.draw(batch);

		head.draw(batch);
	}
	
	private void DrawHolding(SpriteBatch batch)
	{
		if (!IsHolding())
			return;

		holdingPos.x = body.getX()
			+ body.getOriginX();
		holdingPos.y = body.getY()
			+ body.getOriginY();

		if (dir > 0)
			holdingPos.x += body.getWidth() * 0.5f;
		else
			holdingPos.x -= body.getWidth() * 0.5f;
			
		Matrix4 mat = batch.getTransformMatrix();
		float scale = mat.getScaleX() / holdedScale;
		float invScale = 1.f / scale;

		Rectangle objBounds =
			holdedObject.GetBounds();

		holdingPos.y -= 0.5f * invScale
			* objBounds.getHeight();
		
		mat.translate(holdingPos.x, holdingPos.y, 0.f);
		mat.scale(invScale, invScale, invScale);
		batch.setTransformMatrix(mat);
		
		holdedObject.Draw(batch);

		mat.scale(scale, scale, scale);
		mat.translate(-holdingPos.x, -holdingPos.y, 0.f);
		batch.setTransformMatrix(mat);
	}

	@Override
	public void DebugDraw(ShapeRenderer shapes)
	{		
		super.DebugDraw(shapes);
	
		shapes.setColor(1, 0, 0, 1);
		shapes.rect(
			bounds.x, bounds.y,
			bounds.width, bounds.height);

		if (IsHolding())
		{
			Matrix4 mat = shapes.getTransformMatrix();
			float scale = mat.getScaleX() / holdedScale;
			float invScale = 1.f / scale;

			mat.translate(holdingPos.x, holdingPos.y, 0.f);
			mat.scale(invScale, invScale, invScale);
			shapes.setTransformMatrix(mat);

			holdedObject.DebugDraw(shapes);

			mat.scale(scale, scale, scale);
			mat.translate(-holdingPos.x, -holdingPos.y, 0.f);
			shapes.setTransformMatrix(mat);
		}
	}

	@Override
	public Rectangle GetBounds()
	{
		return bounds;
	}
	
	@Override
	public boolean HasFace()
	{
		return false;
	}
	
	@Override
	public Face GetFace()
	{
		return null;
	}

	@Override
	public void PlayAction(AnimAction action)
	{
		playAction = action;
		playActionCycle = actionAnimTime;
	}

	@Override
	public void StopPlayAction()
	{		
		playAction = null;
		playActionCycle = 0.f;
	}
	
	@Override
	public boolean IsPlayingAction()
	{
		return playAction != null;
	}

	@Override
	public void Release()
	{
		super.Release();

		appearance.ReleaseTextures();
		appearance = null;
	}

	@Override
	public void HoldObject(RenderData renderData, float scale)
	{
		holdedObject = renderData;
		holdedScale = scale;
		
		if (holdingPos == null)
		{
			holdingPos = new Vector2(0, 0);
		}
		else
		{
			holdingPos.set(0, 0);
		}
	}

	@Override
	public void StopHolding()
	{
		holdedObject = null;
	}

	@Override
	public boolean IsHolding()
	{
		return holdedObject != null;
	}
	
	@Override
	public Vector2 GetHoldingPos()
	{
		return holdingPos;
	}
}
