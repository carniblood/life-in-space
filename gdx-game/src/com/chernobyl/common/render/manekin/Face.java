package com.chernobyl.common.render.manekin;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

public class Face
{
	eAppearance appearance;

	Sprite head;
	
	int dir = 0;
	
	eEmotion currFaceEmo = null;
	float faceTimer = 0.f;
	
	boolean showHeadBack = false;
	
	Rectangle bounds;
	
	public Face(eAppearance appearance,
		float headRadius)
	{
		this.appearance = appearance;
		this.bounds = new Rectangle();
		
		appearance.LoadTextures();
		eEmotion.LoadTextures(appearance.assetName);
		
		Init(headRadius);
	}
	
	private void Init(float headRadius)
	{
		head = new Sprite(dir > 0 ?
			appearance.texHeadR :
			appearance.texHeadL);
		head.setSize(headRadius * 2.f,
			headRadius * 2.f);
		head.setOrigin(headRadius, 0.f);

		bounds.set(head.getBoundingRectangle());
	}
	
	public void Update(float deltatime)
	{
		if (IsShowingFace())
		{
			faceTimer -= deltatime;
			if (faceTimer <= 0.f)
			{
				faceTimer = 0.f;
				
				currFaceEmo = null;
				RefreshHead();
			}
		}
	}
	
	private void RefreshHead()
	{
		if (showHeadBack)
			return;
		
		if (IsShowingFace())
		{
			Texture tex = currFaceEmo.GetTexture(
				appearance.assetName);
			head.setTexture(tex);

			if (appearance.texHeadL == appearance.texHeadR)
			{
				head.setFlip(dir < 0, false);
			}
		}
		else
		{
			if (appearance.texHeadL !=
				appearance.texHeadR)
			{
				head.setTexture(dir > 0 ?
								appearance.texHeadR :
								appearance.texHeadL);
			}
			else
			{
				head.setTexture(
					appearance.texHeadR);

				head.setFlip(dir < 0, false);
			}
		}
	}
	
	public void Draw(SpriteBatch batch)
	{
		head.draw(batch);
	}

	public void DebugDraw(ShapeRenderer shapes)
	{
		shapes.setColor(1, 0, 0, 1);
		shapes.rect(
			bounds.x, bounds.y,
			bounds.width, bounds.height);
	}
	
	public float GetPosX()
	{
		return head.getX() +
			head.getOriginX();
	}
	
	public float GetY()
	{
		return head.getY() +
			head.getOriginY();
	}

	public void SetPos(float x, float y)
	{
		head.setPosition(
			x - head.getOriginX(),
			y - head.getOriginY());
		
		bounds.x = head.getX();
		bounds.y = head.getY();
	}
	
	public int GetDir()
	{
		return dir;
	}

	public void SetDir(int dir)
	{
		if (this.dir == dir)
		{
			return;
		}

		this.dir = dir;

		if (!IsShowingFace())
		{
			RefreshHead();
		}
	}

	public void Show(eEmotion emo, float time)
	{
		faceTimer = time;

		if (currFaceEmo == emo)
		{
			return;
		}

		currFaceEmo = emo;
		RefreshHead();
	}

	public boolean IsShowingFace()
	{
		return (currFaceEmo != null);
	}

	public boolean IsShowing(eEmotion emo)
	{
		Texture tex = emo.GetTexture(
			appearance.assetName);
		return (head.getTexture() == tex);
	}
	
	public void ShowHeadBack(boolean show)
	{
		if (showHeadBack == show)
			return;
		
		showHeadBack = show;
		
		if (showHeadBack)
		{
			head.setTexture(
				appearance.texHeadBack);
		}
		else
		{
			RefreshHead();
		}
	}
	
	public float getWidth()
	{
		return bounds.width;
	}
	
	public float getHeight()
	{
		return bounds.height;
	}
	
	public Rectangle getBounds()
	{
		return bounds;
	}
	
	public void Release()
	{
		eEmotion.ReleaseTextures(
			appearance.assetName);
		
		appearance.ReleaseTextures();
		appearance = null;
	}
}
