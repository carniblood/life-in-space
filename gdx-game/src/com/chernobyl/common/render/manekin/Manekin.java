package com.chernobyl.common.render.manekin;

import com.chernobyl.common.render.RenderData;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;


public abstract class Manekin extends RenderData
{
	Talkable talkable;
	
	Manekin()
	{
		talkable = new Talkable(this);
	}

	@Override
	public void Update(float deltatime)
	{
		super.Update(deltatime);
		
		talkable.Update(deltatime);
	}

	@Override
	public void Draw(SpriteBatch batch)
	{		
		talkable.Draw(batch);
	}

	@Override
	public void DebugDraw(ShapeRenderer shapes)
	{
		talkable.DebugDraw(shapes);
	}

	public Talkable GetTalkable()
	{
		return talkable;
	}
	
	@Override
	public void Release()
	{
		super.Release();

		talkable.Release();
		talkable = null;
	}
	
	public abstract void SetMove(float move);
	
	public abstract void SetDir(int dir);
	public abstract int GetDir();
	
	public abstract boolean HasFace();
	public abstract Face GetFace();
	
	public enum AnimAction
	{
		USE(1.f),
		JUMP_START(0.4f),
		JUMP_END(0.3f);

		public final float time;

		AnimAction(float time)
		{
			this.time = time;
		}
	}
	
	public abstract void PlayAction(AnimAction action);
	public abstract void StopPlayAction();
	public abstract boolean IsPlayingAction();

	public abstract void HoldObject(RenderData renderData, float scale);
	public abstract void StopHolding();
	public abstract boolean IsHolding();
	public abstract Vector2 GetHoldingPos();
}
