package com.chernobyl.common.render.manekin;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import com.chernobyl.common.render.RenderData;

public class SimpleBiped extends Manekin
{
	static final float bodyWidth = 0.05f;
	static final float bodyHeight = 0.1f;
	
	static final float actionAnimTime = 1.f;

	static final float restStepLeft = 0.75f;
	static final float restStepRight = 0.25f;
	
	static final float walkBodyAngle = 10.f;
	
	static final float cycleSpeed = 2.f;

	eSimpleAppearance appearance;

	Sprite body;
	Sprite bodyRest;
	
	Rectangle bounds;

	int dir = 0;

	int moveDir = 0;
	float moveScale = 0.f;

	float step = restStepRight;
	
	AnimAction playAction;
	float playActionCycle = 0.f;
	
	RenderData holdedObject;
	float holdedScale;
	Vector2 holdingPos;
	
	float cycleSpeedScale = 0.f;

	public SimpleBiped(eSimpleAppearance appearance)
	{
		this.appearance = appearance;
		Init();
		
		cycleSpeedScale = 1.f;
	}

	private void Init()
	{
		appearance.LoadTextures();

		body = new Sprite(appearance.texBody);
		bodyRest = new Sprite(appearance.texBodyRest);
		
		bounds = new Rectangle();

		initBody();
		
		SetDir(1);
	}

	@Override
	public int GetDir()
	{
		return dir;
	}

	@Override
	public void SetDir(int dir)
	{
		if (this.dir == dir)
			return;

		body.setFlip(dir < 0, false);

		this.dir = dir;
	}

	@Override
	public void SetMove(float move)
	{
		float absMove = Math.abs(move);
		if (absMove < 0.001f)
		{
			moveDir = 0;
		}
		else
		{
			moveScale = absMove;
			moveDir = (int)(move / absMove);
			SetDir(moveDir);
		}
	}
	
	public void ScaleCycleSpeed(float scale)
	{
		cycleSpeedScale *= scale;
	}
	
	private void initBody()
	{		
		setBasePose();	
		updateBounds();
	}

	private void setBasePose()
	{
		initSpriteFromBodySize(body);
		initSpriteFromBodySize(bodyRest);
	}

	private void initSpriteFromBodySize(Sprite sprite)
	{
		sprite.setSize(bodyWidth, bodyHeight);
		sprite.setCenterX(0.f);
		sprite.setOrigin(
			sprite.getWidth() * 0.5f,
			sprite.getHeight() * 0.5f);
	}
	
	private void updateBounds()
	{
		bounds.set(body.getBoundingRectangle());
	}

	@Override
	public void Update(float deltatime)
	{
		super.Update(deltatime);

		if (moveDir == 0)
		{
			if (step != restStepLeft && step != restStepRight)
			{
				if (dir == 0)
				{
					step = restStepRight;
				}
				else
				{
					if (dir > 0)
					{
						float newStep = step +
								deltatime * cycleSpeed * cycleSpeedScale * moveScale;
						newStep -= MathUtils.floor(newStep);

						if (step < restStepLeft && newStep > restStepLeft)
							step = restStepLeft;
						else if (step < restStepRight && newStep > restStepRight)
							step = restStepRight;
						else
							step = newStep;
					}
					else if (dir < 0)
					{
						float newStep = step -
								deltatime * cycleSpeed * cycleSpeedScale * moveScale;
						newStep -= MathUtils.floor(newStep);

						if (step > restStepLeft && newStep < restStepLeft)
							step = restStepLeft;
						else if (step > restStepRight && newStep < restStepRight)
							step = restStepRight;
						else
							step = newStep;
					}
				}
			}
		}
		else
		{
			step += deltatime * moveDir * cycleSpeed
					* cycleSpeedScale * moveScale;
			step -= MathUtils.floor(step);
		}

		float bodyAngle;
		if (step <= 0.5f)
			bodyAngle = walkBodyAngle * (4.f * step - 1.f);
		else
			bodyAngle = walkBodyAngle * (4.f * (1.f - step) - 1.f);

		body.setRotation(bodyAngle);
	}

	@Override
	public void Draw(SpriteBatch batch)
	{
		super.Draw(batch);
		
		if (moveDir == 0)
		{
			bodyRest.draw(batch);
		}
		else
		{
			body.draw(batch);
		}
			
		if (IsHolding())
		{
			DrawHolding(batch);
		}
	}
	
	private void DrawHolding(SpriteBatch batch)
	{
		if (!IsHolding())
			return;

		holdingPos.x = body.getX()
			+ body.getOriginX();
		holdingPos.y = body.getY()
			+ body.getOriginY();

		if (dir > 0)
			holdingPos.x += body.getWidth() * 0.5f;
		else
			holdingPos.x -= body.getWidth() * 0.5f;
			
		Matrix4 mat = batch.getTransformMatrix();
		float scale = mat.getScaleX() / holdedScale;
		float invScale = 1.f / scale;

		Rectangle objBounds =
			holdedObject.GetBounds();

		holdingPos.y -= 0.5f * invScale
			* objBounds.getHeight();
		
		mat.translate(holdingPos.x, holdingPos.y, 0.f);
		mat.scale(invScale, invScale, invScale);
		batch.setTransformMatrix(mat);
		
		holdedObject.Draw(batch);

		mat.scale(scale, scale, scale);
		mat.translate(-holdingPos.x, -holdingPos.y, 0.f);
		batch.setTransformMatrix(mat);
	}

	@Override
	public void DebugDraw(ShapeRenderer shapes)
	{		
		super.DebugDraw(shapes);
	
		shapes.setColor(1, 0, 0, 1);
		shapes.rect(
			bounds.x, bounds.y,
			bounds.width, bounds.height);

		if (IsHolding())
		{
			Matrix4 mat = shapes.getTransformMatrix();
			float scale = mat.getScaleX() / holdedScale;
			float invScale = 1.f / scale;

			mat.translate(holdingPos.x, holdingPos.y, 0.f);
			mat.scale(invScale, invScale, invScale);
			shapes.setTransformMatrix(mat);

			holdedObject.DebugDraw(shapes);

			mat.scale(scale, scale, scale);
			mat.translate(-holdingPos.x, -holdingPos.y, 0.f);
			shapes.setTransformMatrix(mat);
		}
	}

	@Override
	public Rectangle GetBounds()
	{
		return bounds;
	}
	
	@Override
	public boolean HasFace()
	{
		return false;
	}
	
	@Override
	public Face GetFace()
	{
		return null;
	}
	
	@Override
	public void PlayAction(AnimAction action)
	{
		playAction = action;
		playActionCycle = action.time;
	}

	@Override
	public void StopPlayAction()
	{		
		playAction = null;
		playActionCycle = 0.f;
	}
	
	@Override
	public boolean IsPlayingAction()
	{
		return playAction != null;
	}

	@Override
	public void Release()
	{
		super.Release();

		appearance.ReleaseTextures();
		appearance = null;
	}

	@Override
	public void HoldObject(RenderData renderData, float scale)
	{
		holdedObject = renderData;
		holdedScale = scale;
		
		if (holdingPos == null)
		{
			holdingPos = new Vector2(0, 0);
		}
		else
		{
			holdingPos.set(0, 0);
		}
	}

	@Override
	public void StopHolding()
	{
		holdedObject = null;
	}

	@Override
	public boolean IsHolding()
	{
		return holdedObject != null;
	}
	
	@Override
	public Vector2 GetHoldingPos()
	{
		return holdingPos;
	}
}
