package com.chernobyl.common.render.manekin;

import com.chernobyl.common.utils.Exceptions;
import com.chernobyl.common.utils.RenderHelpers;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.Gdx;

import java.util.ArrayList;

public enum eSimpleAppearance
{
	ANIMAL		( "animal" ),
	HUMAN		( "human" );

	public String assetName;

	public Texture texBody;
	public Texture texBodyRest;
	public Texture texHead;
	public Texture texLegFR;
	public Texture texLegFL;
	public Texture texLegBR;
	public Texture texLegBL;

	int texUse = 0;

	private eSimpleAppearance(String assetName)
	{
		this.assetName = assetName;
	}

	private int value;
	public static int total = 0;

	private static final ArrayList<eSimpleAppearance> list
		= new ArrayList<eSimpleAppearance>();
	static
	{
		for (eSimpleAppearance appearance : eSimpleAppearance.values())
		{
			appearance.value = total++;
			list.add(appearance);
		}
	}

	public static eSimpleAppearance from(int i)
	{
		return list.get(i);
	}

	public int ToInt()
	{
		return value;
	}

	public void LoadTextures()
	{
		if (++texUse == 1)
		{
			FileHandle assetDir = Gdx.files.internal(
				"graphics/" + assetName + "/");

			texBody = TryLoadTexture(assetDir, "body.png");
			texBodyRest = TryLoadTexture(assetDir, "body_rest.png");
			texHead = TryLoadTexture(assetDir, "head.png");
			texLegFR = TryLoadTexture(assetDir, "legFR.png");
			texLegFL = TryLoadTexture(assetDir, "legFL.png");
			texLegBR = TryLoadTexture(assetDir, "legBR.png");
			texLegBL = TryLoadTexture(assetDir, "legBL.png");
		}
	}

	private Texture TryLoadTexture(FileHandle assetDir, String name)
	{
		FileHandle assetFile = Gdx.files.internal(assetDir.path() + "/" + name);
		if (assetFile.exists())
		{
			return RenderHelpers.LoadTexture(assetFile);
		}

		return null;
	}

	public void ReleaseTextures(boolean force)
	{
		if (force)
		{
			texUse = 1;
		}
		
		ReleaseTextures();
	}
	
	public void ReleaseTextures()
	{
		if (--texUse == 0)
		{
			if (texBody != null)
			{
				texBody.dispose();
				texBody = null;
			}
			if (texBodyRest != null)
			{
				texBodyRest.dispose();
				texBodyRest = null;
			}

			if (texHead != null)
			{
				texHead.dispose();
				texHead = null;
			}

			if (texLegFR != null)
			{
				texLegFR.dispose();
				texLegFR = null;
			}

			if (texLegFL != null)
			{
				texLegFL.dispose();
				texLegFL = null;
			}

			if (texLegBR != null)
			{
				texLegBR.dispose();
				texLegBR = null;
			}

			if (texLegBL != null)
			{
				texLegBL.dispose();
				texLegBL = null;
			}
		}
	}

	public static void CheckCleanMemory(boolean fixAndDelay)
	{
		for (eAppearance appearance : eAppearance.values())
		{
			if (appearance.texUse > 0)
			{
				String name = "simpleAppearance." +
					appearance.assetName;
					
				if (fixAndDelay)
				{
					Exceptions.PushAssetNotReleased(
						name, appearance.texUse);
						
					appearance.ReleaseTextures(true);
				}
				else
				{
					Exceptions.ThrowAssetNotReleased(
						name, appearance.texUse);
				}
			}
			else if (appearance.texUse < 0)
			{
				String name = "simpleAppearance." +
					appearance.assetName;
				
				if (fixAndDelay)
				{
					Exceptions.PushAssetReleasedTwice(
						name, appearance.texUse);
				}
				else
				{
					Exceptions.ThrowAssetReleasedTwice(
						name, appearance.texUse);
				}
			}
		}
	}
}
	
