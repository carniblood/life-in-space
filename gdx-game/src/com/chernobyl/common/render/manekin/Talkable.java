package com.chernobyl.common.render.manekin;

import com.chernobyl.common.render.TextBox;

import com.badlogic.gdx.math.Rectangle;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class Talkable
{
	final static float talkShowTime = 5.f / 60.f; // 5sec for 60 characters
	
	Manekin parent;
	
	TextBox talkMsg;
	float talkTime;
	
	Talkable(Manekin parent)
	{
		this.parent = parent;
	}
	
	public void Say(String text)
	{
		if (talkMsg != null)
		{
			talkMsg.Release();
			talkMsg = null;
		}
		
		if (text.length() == 0)
			return;
		
		talkMsg = new TextBox(text, 0.4f);
			
		Rectangle bounds = parent.GetBounds();
		Rectangle textBounds = talkMsg.GetBounds();
		
		talkMsg.SetOffset(
			0.5f * bounds.getWidth() + 0.05f,
			bounds.getHeight() - textBounds.getHeight(),
			false);
		
		talkTime = talkShowTime * (float)(text.length());
	}
	
	public void StopTalking()
	{
		if (talkMsg != null)
		{
			talkMsg.Release();
			talkMsg = null;
		}
	}

	public boolean IsTalking()
	{
		return talkMsg != null;
	}

	public void Update(float deltatime)
	{
		if (talkMsg != null)
		{
			talkMsg.Update(deltatime);

			talkTime -= deltatime;
			if (talkTime < 0.f)
			{
				talkMsg.Release();
				talkMsg = null;
			}
		}
	}
	
	public void Draw(SpriteBatch batch)
	{
		if (talkMsg != null)
		{
			talkMsg.Draw(batch);
		}
	}
	
	public void DebugDraw(ShapeRenderer shapes)
	{
		if (talkMsg != null)
		{
			talkMsg.DebugDraw(shapes);
		}
	}

	public void Release()
	{
		if (talkMsg != null)
		{
			talkMsg.Release();
			talkMsg = null;
		}
	}
}
