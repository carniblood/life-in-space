package com.chernobyl.common.render.manekin;

import com.chernobyl.common.utils.Exceptions;
import com.chernobyl.common.utils.RenderHelpers;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.Gdx;

import java.util.ArrayList;

public enum eAppearance
{
	//DEFAULT	( "basic" );
	DEFAULT		( "hero" ),
	WOMAN		( "woman" );

	public String assetName;

	public Texture texHeadL;
	public Texture texHeadR;
	public Texture texHeadBack;
	public Texture texBody;
	public Texture texPelvis;
	public Texture texArm;
	public Texture texLeg;
	public Texture texHipL;
	public Texture texHipR;
	public Texture texShoulderL;
	public Texture texShoulderR;

	// temp color test
	public Texture texColor;

	int texUse = 0;

	private eAppearance(String assetName)
	{
		this.assetName = assetName;
	}

	private int value;
	public static int total = 0;

	private static final ArrayList<eAppearance> list
		= new ArrayList<eAppearance>();
	static
	{
		for (eAppearance appearance : eAppearance.values())
		{
			appearance.value = total++;
			list.add(appearance);
		}
	}

	public static eAppearance from(int i)
	{
		return list.get(i);
	}

	public int ToInt()
	{
		return value;
	}

	public void LoadTextures()
	{
		if (++texUse == 1)
		{
			FileHandle assetDir = Gdx.files.internal(
				"graphics/" + assetName + "/");

			texColor = RenderHelpers.LoadTexture(assetDir.child("colors01.png"));

			FileHandle uniqueHeadFile = Gdx.files.internal(assetDir.path() + "/head_side.png");
			if (uniqueHeadFile.exists())
			{
				texHeadL = texHeadR =
					RenderHelpers.LoadTexture(uniqueHeadFile);
			}
			else
			{
				FileHandle headFileLeft = Gdx.files.internal(assetDir.path() + "/head_sideL.png");
				FileHandle headFileRight = Gdx.files.internal(assetDir.path() + "/head_sideR.png");
				if (headFileLeft.exists() && headFileRight.exists())
				{
					texHeadL = RenderHelpers.LoadTexture(headFileLeft);
					texHeadR = RenderHelpers.LoadTexture(headFileRight);
				}
				else
				{
					// Fallback to front head
					texHeadL = texHeadR =
						RenderHelpers.LoadTexture(assetDir.child("head.png"));
				}
			}
			
			// todo: head back asset
			texHeadBack = texHeadL;

			texBody = RenderHelpers.LoadTexture(assetDir.child("body.png"));
			texPelvis = RenderHelpers.LoadTexture(assetDir.child("pelvis.png"));
			texArm = RenderHelpers.LoadTexture(assetDir.child("arm.png"));
			texLeg = RenderHelpers.LoadTexture(assetDir.child("leg.png"));

			FileHandle uniqueHipFile = Gdx.files.internal(assetDir.path() + "/hip.png");
			if (uniqueHipFile.exists())
			{
				texHipL = texHipR =
					RenderHelpers.LoadTexture(uniqueHipFile);
			}
			else
			{
				FileHandle hipLFile = Gdx.files.internal(assetDir.path() + "/hipL.png");
				if (hipLFile.exists())
				{
					texHipL = RenderHelpers.LoadTexture(hipLFile);
				}

				FileHandle hipRFile = Gdx.files.internal(assetDir.path() + "/hipR.png");
				if (hipRFile.exists())
				{
					texHipR = RenderHelpers.LoadTexture(hipRFile);
				}
			}

			FileHandle uniqueShoulderFile = Gdx.files.internal(assetDir.path() + "/shoulder.png");
			if (uniqueShoulderFile.exists())
			{
				texShoulderL = texShoulderR =
					RenderHelpers.LoadTexture(uniqueShoulderFile);
			}
			else
			{
				FileHandle shoulderLFile = Gdx.files.internal(assetDir.path() + "/shoulderL.png");
				if (shoulderLFile.exists())
				{
					texShoulderL = RenderHelpers.LoadTexture(shoulderLFile);
				}

				FileHandle shoulderRFile = Gdx.files.internal(assetDir.path() + "/shoulderR.png");
				if (shoulderRFile.exists())
				{
					texShoulderR = RenderHelpers.LoadTexture(shoulderRFile);
				}
			}
		}
	}

	public void ReleaseTextures(boolean force)
	{
		if (force)
		{
			texUse = 1;
		}
		
		ReleaseTextures();
	}
	
	public void ReleaseTextures()
	{
		if (--texUse == 0)
		{
			if (texHeadBack != null &&
				texHeadBack != texHeadL)
			{
				texHeadBack.dispose();
				texHeadBack = null;
			}
			
			if (texHeadR != null &&
				texHeadR != texHeadL)
			{
				texHeadR.dispose();
				texHeadR = null;
			}

			if (texHeadL != null)
			{
				texHeadL.dispose();
				texHeadL = null;
			}

			if (texBody != null)
			{
				texBody.dispose();
				texBody = null;
			}

			if (texPelvis != null)
			{
				texPelvis.dispose();
				texPelvis = null;
			}

			if (texArm != null)
			{
				texArm.dispose();
				texArm = null;
			}

			if (texLeg != null)
			{
				texLeg.dispose();
				texLeg = null;
			}

			if (texHipL != null &&
				texHipL != texHipR)
			{
				texHipL.dispose();
				texHipL = null;
			}

			if (texHipR != null)
			{
				texHipR.dispose();
				texHipR = null;
			}

			if (texShoulderL != null &&
				texShoulderL != texShoulderR)
			{
				texShoulderL.dispose();
				texShoulderL = null;
			}

			if (texShoulderR != null)
			{
				texShoulderR.dispose();
				texShoulderR = null;
			}
		}
	}

	public static void CheckCleanMemory(boolean fixAndDelay)
	{
		for (eAppearance appearance : eAppearance.values())
		{
			if (appearance.texUse > 0)
			{
				String name = "appearance." +
					appearance.assetName;
					
				if (fixAndDelay)
				{
					Exceptions.PushAssetNotReleased(
						name, appearance.texUse);
						
					appearance.ReleaseTextures(true);
				}
				else
				{
					Exceptions.ThrowAssetNotReleased(
						name, appearance.texUse);
				}
			}
			else if (appearance.texUse < 0)
			{
				String name = "appearance." +
					appearance.assetName;
				
				if (fixAndDelay)
				{
					Exceptions.PushAssetReleasedTwice(
						name, appearance.texUse);
				}
				else
				{
					Exceptions.ThrowAssetReleasedTwice(
						name, appearance.texUse);
				}
			}
		}
	}
}
	
