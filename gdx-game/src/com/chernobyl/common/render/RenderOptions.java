package com.chernobyl.common.render;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;

public class RenderOptions
{
	public static boolean useMipmap = true;
	public static Texture.TextureFilter mipmapMethodMin =
		Texture.TextureFilter.MipMapLinearLinear;
	public static Texture.TextureFilter mipmapMethodMag =
		Texture.TextureFilter.Linear;

	public static Texture.TextureFilter textFilterMethodMin =
		Texture.TextureFilter.Linear;
	public static Texture.TextureFilter textFilterMethodMag =
		Texture.TextureFilter.Linear;	
		
	public static Pixmap.Filter texResizeFilterMethod =
		Pixmap.Filter.BiLinear;
}
