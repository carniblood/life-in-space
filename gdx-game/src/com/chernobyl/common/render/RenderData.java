package com.chernobyl.common.render;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

public abstract class RenderData
{
	public static RenderData CreateEmpty(final float size)
	{
		RenderData empty = new RenderData()
		{
			Rectangle bounds = new Rectangle(
				-0.5f * size, -0.5f * size, size, size);

			@Override
			public void Draw(SpriteBatch batch)
			{}

			@Override
			public void DebugDraw(ShapeRenderer shapes)
			{
				shapes.setColor(0, 0, 0.7f, 1);
				float radius = Math.max(0.1f, bounds.width) * 0.5f;
				shapes.circle(0, 0, radius, 12);

				shapes.setColor(0, 0, 0.3f, 1);
				shapes.rect(bounds.x, bounds.y, bounds.width, bounds.height);
			}

			@Override
			public Rectangle GetBounds()
			{
				return bounds;
			}
		};
		
		return empty;
	}
	
	public abstract void Draw(SpriteBatch batch);
	public abstract void DebugDraw(ShapeRenderer shapes);
	
	public abstract Rectangle GetBounds();
	
	public boolean Intersect(float x, float y)
	{
		return GetBounds().contains(x, y);
	}
	
	public boolean Overlap(Rectangle rect)
	{
		return GetBounds().overlaps(rect);
	}
	
	public void Update(float deltatime)
	{
		UpdateEffects(deltatime);
	}
	
	public boolean HasPart(String name) { return false;}
	
	public void HidePart(String name, boolean hide) {}
	
	public boolean IsPartHidden(String name) { return false;}

	public void Release()
	{
		ReleaseEffects();		
	}
	
	static public class Effect
	{
		float start;
		float duration;
		
		public Effect(float start, float duration)
		{
			this.start = start;
			this.duration = duration;
		}

		public void Init(RenderData renderData) {}
		public void Start(RenderData renderData) {}
		public void Finish(RenderData renderData) {}
		public void Release(RenderData renderData) {}
	}
	
	
	ArrayList<Effect> effects;
	
	public void AddEffect(Effect effect)
	{
		if (effects == null)
		{
			effects = new ArrayList<Effect>();
		}
		
		effects.add(effect);
		effect.Init(this);
	}
	
	private void UpdateEffects(float deltatime)
	{
		if (effects == null)
			return;
		
		boolean effectLeft = false;
		for (Effect effect : effects)
		{
			if (effect.start > 0.f)
			{
				effectLeft = true;
				
				effect.start -= deltatime;
				if (effect.start > 0.f)
					continue;
				
				effect.duration += effect.start;
				effect.start = 0.f;
				effect.Start(this);
			}
			
			if (effect.duration > 0.f)
			{
				effectLeft = true;
				
				effect.duration -= deltatime;
				if (effect.duration <= 0.f)
				{
					effect.Finish(this);
				}
			}
		}
		
		if (!effectLeft)
		{
			ReleaseEffects();
		}
	}
	
	private void ReleaseEffects()
	{
		if (effects == null)
			return;
		
		for (Effect effect : effects)
		{
			effect.Release(this);
		}
		effects.clear();	
		effects = null;
	}
}
