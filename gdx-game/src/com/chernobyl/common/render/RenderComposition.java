package com.chernobyl.common.render;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

import java.util.ArrayList;

public class RenderComposition
	extends RenderData
{
	private class Part
	{
		RenderData renderData;
		int hashName;
		boolean hide;

		public Part(String name, RenderData data)
		{
			hashName = name.hashCode();
			renderData = data;
		}
		
		public Part(String name, RenderData data, boolean hidden)
		{
			hashName = name.hashCode();
			renderData = data;
			hide = hidden;
		}
	}

	ArrayList<Part> parts;
	
	Rectangle bounds;
	
	public RenderComposition()
	{
		parts = new ArrayList<Part>();
		bounds = new Rectangle();
	}

	public void AddPart(String name, RenderData renderData)
	{
		AddPart(name, renderData, false);
	}
	
	public void AddPart(String name, RenderData renderData, boolean hidden)
	{
		if (parts.isEmpty())
		{
			bounds.set(renderData.GetBounds());
		}
		else
		{
			bounds.merge(renderData.GetBounds());
		}

		parts.add(new Part(name, renderData, hidden));
	}
	
	public void RemovePart(String name)
	{
		Part part = GetPartByName(name);
		if (null != part)
		{
			parts.remove(part);
			RefreshBounds();
		}
	}
	
	@Override
	public boolean HasPart(String name)
	{
		return (null != GetPartByName(name));
	}

	@Override
	public void HidePart(String name, boolean hide)
	{
		Part part = GetPartByName(name);
		if (null != part)
		{
			GetPartByName(name).hide = hide;
			RefreshBounds();
		}
	}

	@Override
	public boolean IsPartHidden(String name)
	{
		return GetPartByName(name).hide;
	}

	private Part GetPartByName(String name)
	{
		int hashName = name.hashCode();

		int partCount = parts.size();
		for (int index = 0; index < partCount; ++index)
		{
			Part part = parts.get(index);
			if (hashName == part.hashName)
			{
				return part;
			}
		}

		return null;
	}
	
	private void RefreshBounds()
	{
		if (parts.isEmpty())
			return;
			
		boolean invalid = true;

		for (Part part : parts)
		{
			if (part.hide)
				continue;
				
			RenderData renderData = part.renderData;
			
			if (invalid)
			{
				invalid = false;
				bounds.set(renderData.GetBounds());
			}
			else
			{
				bounds.merge(renderData.GetBounds());
			}
		}
	}

	@Override
	public void Draw(SpriteBatch batch)
	{
		for (Part part : parts)
		{
			if (part.hide)
				continue;
				
			part.renderData.Draw(batch);
		}
	}

	@Override
	public void DebugDraw(ShapeRenderer shapes)
	{
		for (Part part : parts)
		{
			part.renderData.DebugDraw(shapes);
		}
	}

	@Override
	public Rectangle GetBounds()
	{
		return bounds;
	}

	public void Update(float deltatime)
	{
		super.Update(deltatime);
		
		for (Part part : parts)
		{
			part.renderData.Update(deltatime);
		}
	}

	@Override
	public void Release()
	{
		for (Part part : parts)
		{
			part.renderData.Release();
		}
		parts.clear();
		
		super.Release();
	}
}
