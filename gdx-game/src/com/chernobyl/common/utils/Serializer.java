package com.chernobyl.common.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.chernobyl.common.render.manekin.eAppearance;

import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class Serializer
{
	boolean read;
	Preferences prefs;

	public Serializer(String name, boolean read)
	{
		this.read = read;
		prefs = Gdx.app.getPreferences(name);
	}

	public void finish()
	{
		if (!read)
			prefs.flush();
	}

	public boolean version(int version)
	{
		if (read)
		{
			return (prefs.getInteger("version", -1)
				== version);
		}
		else
		{
			prefs.putInteger("version", version);
			return true;
		}
	}

	public boolean ser(String var, boolean value)
	{
		if (read)
		{
			return prefs.getBoolean(var);
		}
		else
		{
			prefs.putBoolean(var, value);
			return value;
		}
	}

	public int ser(String var, int value)
	{
		if (read)
		{
			return prefs.getInteger(var);
		}
		else
		{
			prefs.putInteger(var, value);
			return value;
		}
	}

	public float ser(String var, float value)
	{
		if (read)
		{
			return prefs.getFloat(var);
		}
		else
		{
			prefs.putFloat(var, value);
			return value;
		}
	}
	
	public String ser(String var, String value)
	{
		if (read)
		{
			return prefs.getString(var);
		}
		else
		{
			prefs.putString(var, value);
			return value;
		}
	}
	
	public void ser(String var, Vector2 vec)
	{
		if (read)
		{
			vec.x = prefs.getFloat(var+".x");
			vec.y = prefs.getFloat(var+".y");
		}
		else
		{
			prefs.putFloat(var+".x", vec.x);
			prefs.putFloat(var+".y", vec.y);
		}
	}

	public void ser(String var, Rectangle rect)
	{
		ser(var, rect, false);
	}
	
	public void ser(String var, Rectangle rect, boolean posOnly)
	{
		if (read)
		{
			rect.x = prefs.getFloat(var+".x");
			rect.y = prefs.getFloat(var+".y");

			if (!posOnly)
			{
				rect.width = prefs.getFloat(var+".w");
				rect.height = prefs.getFloat(var+".h");
			}
		}
		else
		{
			prefs.putFloat(var+".x", rect.x);
			prefs.putFloat(var+".y", rect.y);

			if (!posOnly)
			{
				prefs.putFloat(var+".w", rect.width);
				prefs.putFloat(var+".h", rect.height);
			}
		}
	}
	
	public void ser(String var, Sprite sprite)
	{
		ser(var, sprite, false);
	}

	public void ser(String var, Sprite sprite, boolean posOnly)
	{
		if (read)
		{
			sprite.setPosition(
				prefs.getFloat(var+".x"),
				prefs.getFloat(var+".y"));

			sprite.setRotation(
				prefs.getFloat(var+".rot"));
				
			if (!posOnly)
			{
				sprite.setSize(
					prefs.getFloat(var+".w"),
					prefs.getFloat(var+".h"));
			}
		}
		else
		{
			prefs.putFloat(var+".x", sprite.getX());
			prefs.putFloat(var+".y", sprite.getY());

			prefs.putFloat(var+".rot", sprite.getRotation());
			
			if (!posOnly)
			{
				prefs.putFloat(var+".w", sprite.getWidth());
				prefs.putFloat(var+".h", sprite.getHeight());
			}
		}
	}
	
	public eAppearance ser(String var, eAppearance appearance)
	{
		if (read)
		{
			int idx = prefs.getInteger(var);
			return eAppearance.from(idx);
		}
		else
		{
			int idx = appearance.ToInt();
			prefs.putInteger(var, idx);
			return appearance;
		}
	}
	
    public static byte[] serialize(Object obj) throws IOException
    {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        ObjectOutputStream o = new ObjectOutputStream(b);
        o.writeObject(obj);
        return b.toByteArray();
    }

    public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException
    {
        ByteArrayInputStream b = new ByteArrayInputStream(bytes);
        ObjectInputStream o = new ObjectInputStream(b);
        return o.readObject();
    }
}

