package com.chernobyl.common.utils;

import com.badlogic.gdx.Gdx;

import java.util.ArrayList;

public class Exceptions
{
	private static ArrayList<Exception> delayList;
	
	public static void Flush(boolean fullCallTrace)
	{
		if (delayList == null)
			return;
		
		if (delayList.isEmpty())
			return;
		
		for (Exception e : delayList)
		{
			if (fullCallTrace)
				e.printStackTrace();
			else
				Gdx.app.error("Exceptions", e.getMessage());
		}
		
		delayList = null;
		
		throw new RuntimeException("delayed exceptions. check log.");
	}
	
	public static void ThrowAssetNotReleased(String name, int use)
		throws AssetNotReleased
	{
		throw new AssetNotReleased(name, use);
	}
	
	public static void PushAssetNotReleased(String name, int use)
	throws AssetNotReleased
	{
		if (delayList == null)
		{
			delayList = new ArrayList<Exception>();
		}
		
		delayList.add(new AssetNotReleased(name, use));
	}
	
	private static class AssetNotReleased
		extends RuntimeException
	{
		private static final long serialVersionUID = -5732180845712731316L;

		public AssetNotReleased(String name, int use)
		{
			super("asset '" + name +
				"' not released (use="
				+ use + ").");
		}
	}
	
	public static void ThrowAssetReleasedTwice(String name, int use)
	throws AssetReleasedTwice
	{
		throw new AssetReleasedTwice(name, use);
	}
	
	public static void PushAssetReleasedTwice(String name, int use)
	throws AssetReleasedTwice
	{
		if (delayList == null)
		{
			delayList = new ArrayList<Exception>();
		}
		
		delayList.add(new AssetReleasedTwice(name, use));
	}
	
	private static class AssetReleasedTwice
		extends RuntimeException
	{
		private static final long serialVersionUID = 908259162061266130L;

		public AssetReleasedTwice(String name, int use)
		{
			super("asset '" + name +
				"' released multiple times (num="
				+ (1-use) + ").");
		}
	}

	public static void ThrowSortWrongCount(int idx, int count, int entSize)
	throws SortWrongCount
	{
		throw new SortWrongCount(idx, count, entSize);
	}

	public static void PushSortWrongCount(int idx, int count, int entSize)
	throws SortWrongCount
	{
		if (delayList == null)
		{
			delayList = new ArrayList<Exception>();
		}
		
		delayList.add(new SortWrongCount(idx, count, entSize));
	}

	private static class SortWrongCount
	extends RuntimeException
	{
		private static final long serialVersionUID = 908259162061266130L;

		public SortWrongCount(int idx, int count, int entSize)
		{
			super("sort #" + idx +
				  ": wrong count=" + count +
				  " (numEnt=" + entSize + ").");
		}
	}

	public static void ThrowSortWrong(int idx,
		String ent1, float val1, String ent2, float val2)
	throws SortWrong
	{
		throw new SortWrong(idx, ent1, val1, ent2, val2);
	}

	public static void PushSortWrong(int idx, 
		String ent1, float val1, String ent2, float val2)
	throws SortWrong
	{
		if (delayList == null)
		{
			delayList = new ArrayList<Exception>();
		}
		
		delayList.add(new SortWrong(idx, ent1, val1, ent2, val2));
	}

	private static class SortWrong
	extends RuntimeException
	{
		private static final long serialVersionUID = 908259162061266130L;

		public SortWrong(int idx,
			String ent1, float val1, String ent2, float val2)
		{
			super("sort #" + idx +
				  ": wrong val=" + val1 + "(ent=" + ent1 +
				  ") and val=" + val2 + "(ent=" + ent2 + ")");
		}
	}
}
