package com.chernobyl.common.utils;

public class IntrusiveLinkedList<T extends IntrusiveLinkedObject<T>>
{
	T.IntrusiveLink rootLink;
	
	public T Prev(T node)
	{
		T prevNode = node.GetLink().Prev();
		return prevNode;
	}

	public T Next(T node)
	{
		T nextNode = node.GetLink().Next();
		return nextNode;
	}

	public T GetHead()
	{
		if (rootLink == null)
			return null;
			
		return (T)rootLink.GetNode();
	}

	public T GetTail()
	{
		if (rootLink == null)
			return null;
			
		return (T)rootLink.Prev();
	}

	public boolean IsEmpty()
	{
		return rootLink == null;
	}

	public void UnlinkAll()
	{
		while (rootLink.IsLinked())
		{
			rootLink.PrevLink().Unlink();
		}
	}

	public void InsertBefore(T node, T.IntrusiveLink prevLink)
	{
		if (node == null)
			return;

		if (prevLink != null)
			node.GetLink().InsertBefore(prevLink);
		else if (rootLink != null)
			node.GetLink().InsertBefore(rootLink);
		else
		    rootLink = (T.IntrusiveLink)node.GetLink();
	}

	public void InsertAfter(T node, T.IntrusiveLink prevLink)
	{
		if (node == null)
			return;

		if (prevLink != null)
			node.GetLink().InsertAfter(prevLink);
		else if (rootLink != null)
		    node.GetLink().InsertAfter(rootLink);
		else
			rootLink = (T.IntrusiveLink)node.GetLink();
	}

	public void InsertTail(T node)
	{
		InsertBefore(node, null);
	}

	public void InsertHead(T node)
	{
		InsertAfter(node, null);
	}
	
	public void Remove(T node)
	{
		T.IntrusiveLink link = (T.IntrusiveLink)node.GetLink();
		
		if (link == rootLink)
		{
			rootLink = (T.IntrusiveLink)link.NextLink();
		}
		
		link.Unlink();
	}
}
