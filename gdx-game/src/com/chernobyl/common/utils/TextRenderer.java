package com.chernobyl.common.utils;

import com.chernobyl.common.render.RenderOptions;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.BitmapFontData;
import com.badlogic.gdx.graphics.g2d.BitmapFont.Glyph;
import com.badlogic.gdx.graphics.g2d.BitmapFontCache;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.GdxRuntimeException;

import java.util.ArrayList;
import java.util.HashMap;

public class TextRenderer
{
	static final FontType defaultFont = FontType.JAAPOKKI;
	
	private static float screenWidth;
	private static float screenHeight;
	
	public static void init(float screenWidth, 
		float screenHeight)
	{
		TextRenderer.screenWidth = screenWidth;
		TextRenderer.screenHeight = screenHeight;
	}
	
	protected TextRenderer()
	{
	}
	
	// multiton: one singleton per id
	// but use multi-threaded get() instead
	private static HashMap<Long, TextRenderer> instanceMap;
	private static TextRenderer get(long id)
	{
		if (instanceMap == null)
		{
			instanceMap = 
				new HashMap<Long, TextRenderer>();
		}
		
		TextRenderer instance = instanceMap.get(id);
		if (instance == null)
		{
			instance = new TextRenderer();
			instanceMap.put(id, instance);
		}
		
		return instance;
	}
	
	public static synchronized TextRenderer get()
	{
		// separate instances for each thread
		// only main thread can draw though
		long threadId = 
			Thread.currentThread().getId();
		
		return get(threadId);
	}
	
	static ArrayList<Long> cachedFontRequests;
	public static void update()
	{
		if (cachedFontRequests == null)
			return;

		synchronized(cachedFontRequests)
		{
			for (Long threadId : cachedFontRequests)
			{
				TextRenderer other = get(threadId);
				// TODO: just copy TextureRegion
				// from main thread's one?
				other.cachedFont = defaultFont.CreateBitmapFont();
			}
			
			cachedFontRequests.clear();
			cachedFontRequests.notifyAll();
		}
	}
	
	private static boolean requestNewCachedFont()
	{
		if (cachedFontRequests == null)
		{
			cachedFontRequests = new ArrayList<Long>();
		}

		Long threadId =
			Thread.currentThread().getId(); 
		
		synchronized(cachedFontRequests)
		{
			cachedFontRequests.add(threadId);

			try
			{
				cachedFontRequests.wait();
			}
			catch (InterruptedException ie)
			{
				ie.printStackTrace();
				return false;
			}
		}
		
		return true;
	}

	public static void clean()
	{
		if (instanceMap != null)
		{
			for (TextRenderer instance : instanceMap.values())
			{
				instance.dispose();
			}
			
			instanceMap.clear();
		}
	}
	
	public enum FontType
	{
		BUILTIN				( "", 						1.f,	Color.BLACK ),
		
		BILLY				( "billy",					0.65f,	Color.BLACK ),
		GIDOLE				( "gidole",					0.5f,	Color.BLACK ),
		HALLO_SANS			( "hallo-sans",				0.4f,	Color.BLACK ),
		JAAPOKKI			( "jaapokki",				0.4f,	Color.BLACK ),
		JAAPOKKI_ENCHANCE	( "jaapokki-enchance",		0.4f,	Color.BLACK ),
		JAAPOKKI_SUBTRACT	( "jaapokki-subtract",		0.4f,	Color.BLACK );
		
		String name;
		
		public float scaleAdjustment; 
		public Color defaultColor;
		
		FontType(String name, float scaleAdjustment, Color defaultColor)
		{
			this.name = name;
			this.scaleAdjustment = scaleAdjustment;
			this.defaultColor = defaultColor;
		}
		
		public BitmapFont CreateBitmapFont()
		{
			if (name.length() == 0)
				return new BitmapFont();
			
			String path = "fonts/" + name;
			return new BitmapFont(
				Gdx.files.internal(path + ".fnt"),
				Gdx.files.internal(path + ".png"), false);
		}
	}
	
	public enum HAnchor
	{
        LEFT, CENTER, RIGHT;
    }

    public enum VAnchor
	{
        BOTTOM, CENTER, TOP;
    }
	
	public void preloadFontIfNeeded()
	{
		getFont();
	}
	
	private BitmapFont cachedFont;
	private float defaultScale;
	private BitmapFont getFont()
	{		
		if (cachedFont == null)
		{			
			try
			{
				cachedFont = defaultFont.CreateBitmapFont();
			}
			catch (GdxRuntimeException rt)
			{
				rt.printStackTrace();
				return null;
			}
			catch (IndexOutOfBoundsException rt)
			{
				rt.printStackTrace();
				return null;				
			}
			catch (RuntimeException rt)
			{		
				// failed if not main thread
				// try on main thread
				// (main thread should always success!)
				requestNewCachedFont();
			}

			defaultScale = 1.f;
			
			cachedFont.setColor(defaultFont.defaultColor);
			cachedFont.getData().scale(defaultScale);
			cachedFont.getData().markupEnabled = true;
			
			BitmapFontCache cache = cachedFont.getCache();
			cache.setUseIntegerPositions(false);
			
			cachedFont.getRegion().getTexture().setFilter(
				RenderOptions.textFilterMethodMin,
				RenderOptions.textFilterMethodMag);
		}
		return cachedFont;
	}
	
	public void setColor(Color color)
	{
		BitmapFont font = getFont();
		font.setColor(color);
	}
	
	public void setDefaultColor()
	{
		BitmapFont font = getFont();
		font.setColor(defaultFont.defaultColor);
	}

	public void setScale(float scale)
	{
		float newScale = (scale / screenWidth)
			* defaultFont.scaleAdjustment;
		
		BitmapFont font = getFont();
		font.getData().setScale(
			newScale, newScale);
	}

	public void setDefaultScale()
	{
		setScale(defaultScale);
	}
	
	private float warpWidth = 0;
	public void setWarpWidth(float width)
	{
		warpWidth = width;
	}
	
	public void setDefaultWarpWidth()
	{
		warpWidth = 0;
	}

	public void resetFont()
	{
		setDefaultColor();
		setDefaultScale();
		setDefaultWarpWidth();
	}
	
	public Rectangle drawText(SpriteBatch batch,
		String text, float xpos, float ypos)
	{
		return drawText(batch, text, xpos, ypos,
			HAnchor.CENTER, VAnchor.CENTER);
	}
	
	private Rectangle cachedBounds;
	public Rectangle drawText(SpriteBatch batch,
		String text, float xpos, float ypos,
		HAnchor hAnchor, VAnchor vAnchor)
    {
		BitmapFont font = getFont();
		BitmapFontCache cache = font.getCache();

		if (cachedBounds == null)
		{
			cachedBounds = new Rectangle();
		}
		
		GlyphLayout layout =
			prepare(cache, text, xpos, ypos);
			
		cachedBounds.width = layout.width;
		cachedBounds.height = layout.height;
		
		boolean changed = false;
        switch (hAnchor)
        {
        	case LEFT:
        		break;

            case CENTER:
                xpos -= cachedBounds.width * 0.5f;
				changed = true;
                break;
				
            case RIGHT:
                xpos -= cachedBounds.width;
				changed = true;
                break;
        }

        switch (vAnchor)
        {
            case BOTTOM:
                ypos += cachedBounds.height;
				changed = true;
                break;

            case CENTER:
				ypos += cachedBounds.height * 0.5f;
				changed = true;
                break;
				
        	case TOP:
        		break;
        }
		
		if (changed)
		{
			layout = prepare(cache,
				text, xpos, ypos);
				
			cachedBounds.width = layout.width;
			cachedBounds.height = layout.height;
		}

		if (batch != null)
		{
			cache.draw(batch);
		}
        
		resetFont();
		
		cachedBounds.x = xpos;
		cachedBounds.y = ypos - cachedBounds.height;
		return cachedBounds;
    }
	
	private GlyphLayout prepare(
		BitmapFontCache cache, String text, float xpos, float ypos)
	{
		try
		{
			return cache.setText(
				text, xpos, ypos,
				warpWidth/* * screenWidth*/,
				Align.left, warpWidth > 0);
		}
		catch (IllegalStateException e)
		{
			throw new IllegalStateException(
				"failed setText with '"
				+ text + "'");
		}
	}
	
	public Rectangle findSubBounds(String fullText, String str)
	{
		int start = fullText.indexOf(str);
		if (start < 0)
			return null;

		int end = start + str.length();
		return findSubBounds(fullText, start, end);
	}
	
	private Rectangle cachedSubBounds;
	public Rectangle findSubBounds(String fullText, int start, int end)
	{
		if (cachedSubBounds == null)
		{
			cachedSubBounds = new Rectangle();
		}
		
		int startGlyph = countGlyphs(fullText, start);
		int endGlyph = countGlyphs(fullText, end);
		int numGlyphs = endGlyph - startGlyph;
		
		BitmapFont font = getFont();
		BitmapFontCache cache = font.getCache();
		float[] vertices = cache.getVertices();
		
		int idx = startGlyph * 20;
		cachedSubBounds.set(vertices[idx],
			vertices[idx + 1], 0, 0);
		for (int i = 0; i < numGlyphs; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				cachedSubBounds.merge(
					vertices[idx],
					vertices[idx + 1]);
				idx += 5;
			}
		}
		
		return cachedSubBounds;
	}

	// from BitmapFontCache
	// + check if glyph has been pushed
	private int countGlyphs(CharSequence seq, int end)
	{
		BitmapFont font = getFont();
		BitmapFontData data = font.getData();
		BitmapFontCache cache = font.getCache();
		float[] vertices = cache.getVertices();
		
		int cur = 0;
		int count = end;
		while (cur < end) {
			char ch = seq.charAt(cur++);
			if (ch == '[') {
				count--;
				if (!(cur < end && seq.charAt(cur) == '[')) { // non escaped '['
					while (cur < end && seq.charAt(cur) != ']') {
						cur++;
						count--;
					}
					//count--; // bug?
				}
				cur++;
			}
			
			// check if it is a character
			Glyph glyph = data.getGlyph(ch);
			if (glyph == null || glyph.page != 0)
			{
				count--;
			}
			else
			{
				// check if it has been skipped (warped + whitespace)
				int numSkips = end - count;
				int numGlyph = (cur - 1) - numSkips;
				int idx = numGlyph * 20;
				
				// check u,v matching
				if (vertices[idx + 3] != glyph.u ||
					vertices[idx + 4] != glyph.v ||
					vertices[idx + 13] != glyph.u2 ||
					vertices[idx + 14] != glyph.v2)
				{
					count--;
				}
			}
		}
		return count;
	}
	
	public void dispose()
	{
		if (cachedFont != null)
		{
			cachedFont.dispose();
			cachedFont = null;
		}
	}
}
