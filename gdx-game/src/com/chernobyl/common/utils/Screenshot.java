package com.chernobyl.common.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.utils.ScreenUtils;

import java.nio.ByteBuffer;

public class Screenshot
{
	final static boolean saveToLocalFolder = true;
	
    private static int counter = 1;

    public static void saveScreenshot()
    {
        FileHandle file;
        do
        {
            String screenshotFile = String.format("screenshot%03d.png", counter++);

            if (saveToLocalFolder)
            {
            	file = Gdx.files.local("capture/" + screenshotFile);
            }
            else
            {
            	file = Gdx.files.external("Ullich/" + screenshotFile);            	
            }
        }
        while (file.exists());

        Pixmap pixmap = getScreenshot(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
        PixmapIO.writePNG(file, pixmap);
        pixmap.dispose();
    }

    private static Pixmap getScreenshot(int x, int y, int w, int h, boolean yDown)
    {
        final Pixmap pixmap = ScreenUtils.getFrameBufferPixmap(x, y, w, h);

        if (yDown)
        {
            // Flip the pixmap upside down
            ByteBuffer pixels = pixmap.getPixels();
            int numBytes = w * h * 4;
            byte[] lines = new byte[numBytes];
            int numBytesPerLine = w * 4;
            for (int i = 0; i < h; i++)
            {
                pixels.position((h - i - 1) * numBytesPerLine);
                pixels.get(lines, i * numBytesPerLine, numBytesPerLine);
            }
            pixels.clear();
            pixels.put(lines);
            pixels.clear();
        }

        return pixmap;
    }
}
