package com.chernobyl.common.utils;

public class IntrusiveLinkedObject<T extends IntrusiveLinkedObject<T>>
{
	public class IntrusiveLink
	{
		private IntrusiveLinkedObject<T> nextNode;
		private IntrusiveLink prevLink;

		public IntrusiveLink(IntrusiveLinkedObject<T> node)
		{
			nextNode = node;
			prevLink = this;
		}

		public IntrusiveLink NextLink()
		{
			return nextNode.GetLink();
		}

		public IntrusiveLink PrevLink()
		{
			return prevLink;
		}

		public T Next()
		{
			if (nextNode.GetLink() == this)
				return null;

			return (T)nextNode;
		}

		public T Prev()
		{
			IntrusiveLinkedObject<T> prevNode = prevLink.prevLink.nextNode;

			if (prevNode.GetLink() == this)
				return null;

			return (T)prevNode;
		}

		public T GetNode()
		{
			return (T)prevLink.nextNode;
		}

		// not safe to call from outside
		// since members become dirty
		// call Unlink() instead
		private void RemoveFromList()
		{
			NextLink().prevLink = prevLink;
			prevLink.nextNode = nextNode;
		}

		public void Unlink()
		{
			T node = GetNode();

			RemoveFromList();

			nextNode = node;
			prevLink = this;
		}

		public boolean IsLinked()
		{
			return prevLink != this;
		}

		public void InsertBefore(IntrusiveLink link)
		{
			T node = GetNode();

			RemoveFromList();

			prevLink = link.prevLink;
			nextNode = prevLink.nextNode;

			prevLink.nextNode = node;
			link.prevLink = this;
		}

		public void InsertAfter(IntrusiveLink link)
		{
			T node = GetNode();

			RemoveFromList();

			prevLink = link;
			nextNode = link.nextNode;

			link.NextLink().prevLink = this;
			link.nextNode = node;
		}
	}
	
	private final IntrusiveLink link = new IntrusiveLink(this);

	public IntrusiveLink GetLink()
	{
		return link;
	}
}

